webpackJsonp([0],{

/***/ 1209:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return FoodBlog; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(10);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__index__ = __webpack_require__(31);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_Rx__ = __webpack_require__(229);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_Rx___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_rxjs_Rx__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_angularfire2_database__ = __webpack_require__(17);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__shared_index__ = __webpack_require__(15);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__ionic_native_social_sharing__ = __webpack_require__(74);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7_lodash__ = __webpack_require__(73);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7_lodash___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_7_lodash__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};








var FoodBlog = /** @class */ (function () {
    function FoodBlog(navCtrl, navParams, alertCtrl, modalCtrl, socialshare, data$, auth$, ionic$, error$) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.alertCtrl = alertCtrl;
        this.modalCtrl = modalCtrl;
        this.socialshare = socialshare;
        this.data$ = data$;
        this.auth$ = auth$;
        this.ionic$ = ionic$;
        this.error$ = error$;
        this.blogs = [];
    }
    FoodBlog.prototype.ngOnInit = function () {
        this.mi = this.auth$.currentUser;
    };
    FoodBlog.prototype.open = function (blog) {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_2__index__["c" /* Blog */], blog);
    };
    FoodBlog.prototype.likedByUser = function (blog) {
        blog.likes_count = __WEBPACK_IMPORTED_MODULE_7_lodash__["size"](blog.likes);
        blog.comments_count = __WEBPACK_IMPORTED_MODULE_7_lodash__["size"](blog.comments);
        return blog.likes && this.mi ? blog.likes[this.mi.uid] : undefined;
    };
    FoodBlog.prototype.bookmarked = function (blog) {
        return blog.bookmarks && this.mi ? blog.bookmarks[this.mi.uid] : undefined;
    };
    FoodBlog.prototype.like = function (blog) {
        var _this = this;
        var likedbyUser = blog.likes && this.mi ? blog.likes[this.mi.uid] : undefined;
        if (!likedbyUser) {
            this.data$.object('/blogs/' + blog.$key + '/likes/' + this.mi.uid)
                .set({
                name: this.mi.fullname ? this.mi.fullname : '',
                picture: this.mi.picture ? this.mi.picture : '',
                date: new Date()
            })
                .then(function () { return _this.ionic$.presentToast('You Liked this post'); }, function (error) { return _this.error$.handleError(error); });
        }
        else {
            this.data$.object('/blogs/' + blog.$key + '/likes/' + this.mi.uid)
                .remove().then(function () { return _this.ionic$.presentToast('You Unliked this post'); }, function (error) { return _this.error$.handleError(error); });
        }
    };
    FoodBlog.prototype.comment = function (blog) {
        var modal = this.modalCtrl.create(__WEBPACK_IMPORTED_MODULE_2__index__["d" /* BlogComments */], blog);
        modal.present();
    };
    FoodBlog.prototype.share = function (blog) {
        this.socialshare.share();
    };
    FoodBlog.prototype.delete = function (blog) {
        var _this = this;
        var alert = this.alertCtrl.create({
            title: 'Confirm Delete',
            message: 'Are you sure you want to delete this post?',
            buttons: [
                {
                    text: 'Cancel',
                    role: 'cancel',
                    handler: function () {
                    }
                },
                {
                    text: 'Delete',
                    cssClass: 'primary-button',
                    handler: function () {
                        _this.data$.object('/blogs/' + blog.$key).remove()
                            .then(function () { return _this.ionic$.presentToast('You have deleted this post'); }, function (error) { return _this.error$.handleError(error); });
                    }
                }
            ]
        });
        alert.present();
    };
    FoodBlog.prototype.bookmark = function (blog) {
        var _this = this;
        var bookmarked = blog.bookmarks ? blog.bookmarks[this.mi.uid] : undefined;
        if (!bookmarked) {
            this.data$.object('/blogs/' + blog.$key + '/bookmarks/' + this.mi.uid)
                .set(this.mi.uid)
                .then(function () { return _this.ionic$.presentToast('You have saved this post'); }, function (error) { return _this.error$.handleError(error); });
        }
        else {
            this.data$.list('/blogs/' + blog.$key + '/bookmarks/' + this.mi.uid)
                .remove()
                .then(function () { return _this.ionic$.presentToast('You have saved this post'); }, function (error) { return _this.error$.handleError(error); });
        }
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])(),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_3_rxjs_Rx__["Observable"])
    ], FoodBlog.prototype, "data", void 0);
    FoodBlog = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'food-blog',template:/*ion-inline-start:"D:\Development\Current_Projects\2019_5_16_Ionic\personal-coach-app\src\pages\blogs\food-blog\food-blog.html"*/'<ion-card class="blog-card" *ngFor="let blog of data | orderBy: \'-created_at\'">\n  <div class="image" *ngIf="blog?.images?.length > 0" (click)="open(blog)" [style.background-image]="\'url(\' + blog?.images[0] + \')\' | image: \'style: placeholder\'"></div>\n  <div class="content" (click)="open(blog)">\n    <p>{{blog.content| slice: 0: 140}}</p>\n    {{blog.type}}\n  </div>\n  <ion-row>\n        <ion-col col-4 class="date">{{blog.created_at | amTimeAgo}}</ion-col>\n        <ion-col text-right>\n            <span (click)="like(blog)"><ion-icon [name]="likedByUser(blog) ? \'heart\' : \'heart-outline\'"></ion-icon>{{blog?.likes_count > 0 ? blog?.likes_count : \'\'}}</span>\n            <span (click)="comment(blog)"><ion-icon name="quote-outline"></ion-icon>{{blog?.comments_count > 0 ? blog?.comments_count : \'\'}}</span>\n            <span (click)="bookmark(blog)"><ion-icon [name]="bookmarked(blog) ? \'bookmark\' : \'bookmark-outline\'"></ion-icon></span>\n            <span (click)="share(blog)"><ion-icon name="md-share"></ion-icon></span>\n            <span (click)="delete(blog)" *ngIf="blog?.user?.uid === mi?.uid"><ion-icon name="trash" color="danger"></ion-icon></span>\n        </ion-col>\n    </ion-row>\n</ion-card>\n'/*ion-inline-end:"D:\Development\Current_Projects\2019_5_16_Ionic\personal-coach-app\src\pages\blogs\food-blog\food-blog.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["b" /* AlertController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* ModalController */],
            __WEBPACK_IMPORTED_MODULE_6__ionic_native_social_sharing__["a" /* SocialSharing */],
            __WEBPACK_IMPORTED_MODULE_4_angularfire2_database__["a" /* AngularFireDatabase */],
            __WEBPACK_IMPORTED_MODULE_5__shared_index__["a" /* AuthProvider */],
            __WEBPACK_IMPORTED_MODULE_5__shared_index__["f" /* IonicProvider */],
            __WEBPACK_IMPORTED_MODULE_5__shared_index__["d" /* ErrorHandlerProvider */]])
    ], FoodBlog);
    return FoodBlog;
}());

//# sourceMappingURL=food-blog.js.map

/***/ }),

/***/ 1210:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return LifestyleBlog; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(10);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__index__ = __webpack_require__(31);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_Rx__ = __webpack_require__(229);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_Rx___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_rxjs_Rx__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_angularfire2_database__ = __webpack_require__(17);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__shared_index__ = __webpack_require__(15);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__ionic_native_social_sharing__ = __webpack_require__(74);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7_lodash__ = __webpack_require__(73);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7_lodash___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_7_lodash__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};








var LifestyleBlog = /** @class */ (function () {
    function LifestyleBlog(navCtrl, navParams, modalCtrl, alertCtrl, socialshare, data$, auth$, ionic$, error$) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.modalCtrl = modalCtrl;
        this.alertCtrl = alertCtrl;
        this.socialshare = socialshare;
        this.data$ = data$;
        this.auth$ = auth$;
        this.ionic$ = ionic$;
        this.error$ = error$;
    }
    LifestyleBlog.prototype.ngOnInit = function () {
        this.mi = this.auth$.currentUser;
    };
    LifestyleBlog.prototype.open = function (blog) {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_2__index__["c" /* Blog */], blog);
    };
    LifestyleBlog.prototype.likedByUser = function (blog) {
        blog.likes_count = __WEBPACK_IMPORTED_MODULE_7_lodash__["size"](blog.likes);
        blog.comments_count = __WEBPACK_IMPORTED_MODULE_7_lodash__["size"](blog.comments);
        return blog.likes ? blog.likes[this.mi.uid] : undefined;
    };
    LifestyleBlog.prototype.bookmarked = function (blog) {
        return blog.bookmarks ? blog.bookmarks[this.mi.uid] : undefined;
    };
    LifestyleBlog.prototype.like = function (blog) {
        var _this = this;
        var likedbyUser;
        likedbyUser = blog.likes ? blog.likes[this.mi.uid] : undefined;
        if (!likedbyUser) {
            this.data$.object('/blogs/' + blog.$key + '/likes/' + this.mi.uid)
                .set({
                name: this.mi.fullname ? this.mi.fullname : '',
                picture: this.mi.picture ? this.mi.picture : '',
                date: new Date()
            })
                .then(function () { return _this.ionic$.presentToast('You Liked this post'); }, function (error) { return _this.error$.handleError(error); });
        }
        else {
            this.data$.object('/blogs/' + blog.$key + '/likes/' + this.mi.uid)
                .remove().then(function () { return _this.ionic$.presentToast('You Unliked this post'); }, function (error) { return _this.error$.handleError(error); });
        }
    };
    LifestyleBlog.prototype.comment = function (blog) {
        var modal = this.modalCtrl.create(__WEBPACK_IMPORTED_MODULE_2__index__["d" /* BlogComments */], blog);
        modal.present();
    };
    LifestyleBlog.prototype.share = function (blog) {
        this.socialshare.share();
    };
    LifestyleBlog.prototype.delete = function (blog) {
        var _this = this;
        var alert = this.alertCtrl.create({
            title: 'Confirm Delete',
            message: 'Are you sure you want to delete this post?',
            buttons: [
                {
                    text: 'Cancel',
                    role: 'cancel',
                    handler: function () {
                    }
                },
                {
                    text: 'Delete',
                    cssClass: 'primary-button',
                    handler: function () {
                        _this.data$.object('/blogs/' + blog.$key).remove()
                            .then(function () { return _this.ionic$.presentToast('You have deleted this post'); }, function (error) { return _this.error$.handleError(error); });
                    }
                }
            ]
        });
        alert.present();
    };
    LifestyleBlog.prototype.bookmark = function (blog) {
        var _this = this;
        var bookmarked = blog.bookmarks ? blog.bookmarks[this.mi.uid] : undefined;
        if (!bookmarked) {
            this.data$.object('/blogs/' + blog.$key + '/bookmarks/' + this.mi.uid)
                .set(this.mi.uid)
                .then(function () { return _this.ionic$.presentToast('You have saved this post'); }, function (error) { return _this.error$.handleError(error); });
        }
        else {
            this.data$.list('/blogs/' + blog.$key + '/bookmarks/' + this.mi.uid)
                .remove()
                .then(function () { return _this.ionic$.presentToast('You have saved this post'); }, function (error) { return _this.error$.handleError(error); });
        }
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])(),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_3_rxjs_Rx__["Observable"])
    ], LifestyleBlog.prototype, "data", void 0);
    LifestyleBlog = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'lifestyle-blog',template:/*ion-inline-start:"D:\Development\Current_Projects\2019_5_16_Ionic\personal-coach-app\src\pages\blogs\lifestyle-blog\lifestyle-blog.html"*/'<ng-container *ngIf="mi">\n\n  <div class="block" *ngFor="let blog of data | orderBy: \'-created_at\'">\n    <div class="profile" [style.background-image]="\'url(\' + blog?.user?.picture + \')\' | image: \'style: profile\'">\n    </div>\n    <div class="description" (click)="open(blog)">\n      <ion-row class="header">\n        <ion-col col-7>{{blog.user.fullname}}</ion-col>\n        <ion-col col-5 text-right>{{blog.created_at | amTimeAgo}}</ion-col>\n      </ion-row>\n      <div class="content">\n        {{blog.content}}\n        <div *ngIf="blog?.images?.length > 0" class="image" [style.background-image]="\'url(\' + blog?.images[0] + \')\' | image: \'style: placeholder\'">\n        </div>\n      </div>\n    </div>\n    <ion-row>\n      <ion-col text-right>\n        <span (click)="like(blog)">\n          <ion-icon [name]="likedByUser(blog) ? \'heart\' : \'heart-outline\'"></ion-icon>{{blog?.likes_count > 0 ?\n          blog?.likes_count : \'\'}}\n        </span>\n        <span (click)="comment(blog)">\n          <ion-icon name="quote-outline"></ion-icon>{{blog?.comments_count > 0 ? blog?.comments_count : \'\'}}\n        </span>\n        <span (click)="bookmark(blog)">\n          <ion-icon [name]="bookmarked(blog) ? \'bookmark\' : \'bookmark-outline\'"></ion-icon>\n        </span>\n        <span (click)="share(blog)">\n          <ion-icon name="md-share"></ion-icon>\n        </span>\n        <span (click)="delete(blog)" *ngIf="blog?.user?.uid === mi.uid">\n          <ion-icon name="trash" color="danger"></ion-icon>\n        </span>\n      </ion-col>\n    </ion-row>\n\n  </div>\n</ng-container>'/*ion-inline-end:"D:\Development\Current_Projects\2019_5_16_Ionic\personal-coach-app\src\pages\blogs\lifestyle-blog\lifestyle-blog.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* ModalController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["b" /* AlertController */],
            __WEBPACK_IMPORTED_MODULE_6__ionic_native_social_sharing__["a" /* SocialSharing */],
            __WEBPACK_IMPORTED_MODULE_4_angularfire2_database__["a" /* AngularFireDatabase */],
            __WEBPACK_IMPORTED_MODULE_5__shared_index__["a" /* AuthProvider */],
            __WEBPACK_IMPORTED_MODULE_5__shared_index__["f" /* IonicProvider */],
            __WEBPACK_IMPORTED_MODULE_5__shared_index__["d" /* ErrorHandlerProvider */]])
    ], LifestyleBlog);
    return LifestyleBlog;
}());

//# sourceMappingURL=lifestyle-blog.js.map

/***/ }),

/***/ 1211:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AddBlog; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(10);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_angularfire2_database__ = __webpack_require__(17);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__shared_index__ = __webpack_require__(15);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__angular_forms__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__ionic_native_camera__ = __webpack_require__(138);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






var AddBlog = /** @class */ (function () {
    function AddBlog(viewCtrl, fb, camera, navParams, actionSheetCtrl, data$, auth$, storage$, ionic$, error$) {
        this.viewCtrl = viewCtrl;
        this.fb = fb;
        this.camera = camera;
        this.navParams = navParams;
        this.actionSheetCtrl = actionSheetCtrl;
        this.data$ = data$;
        this.auth$ = auth$;
        this.storage$ = storage$;
        this.ionic$ = ionic$;
        this.error$ = error$;
        this.images = [];
        this.types = ['food', 'lifestyle', 'fitness'];
        this.imageURLS = [];
        this.cameraOptions = {
            quality: 100,
            destinationType: this.camera.DestinationType.DATA_URL,
            sourceType: this.camera.PictureSourceType.CAMERA
        };
    }
    AddBlog.prototype.ngOnInit = function () {
        this.mi = this.auth$.currentUser;
        this.blog = this.navParams.get('blog');
        this.key = this.blog ? this.blog.$key : null;
        this.blog_form = this.fb.group({
            type: [this.blog ? this.blog.type : '', __WEBPACK_IMPORTED_MODULE_4__angular_forms__["Validators"].required],
            title: [this.blog ? this.blog.title : ''],
            images: [[]],
            content: [this.blog ? this.blog.content : '', __WEBPACK_IMPORTED_MODULE_4__angular_forms__["Validators"].required],
            created_at: this.blog ? this.blog.created_at : new Date().toISOString(),
            user: this.fb.group({
                uid: this.blog ? this.blog.user ? this.blog.user.uid : this.mi.uid : this.mi.uid,
                fullname: this.blog ? this.blog.user.fullname : this.mi.fullname,
                picture: this.blog ? this.blog.user.picture : this.mi.picture,
            })
        });
        this.images = this.blog ? this.blog.images : [];
    };
    AddBlog.prototype.blogType = function (value) {
        this.blog_form.patchValue({ type: value });
    };
    AddBlog.prototype.addImages = function () {
        var _this = this;
        var actionSheet = this.actionSheetCtrl.create({
            title: 'Send Image from',
            buttons: [
                {
                    text: 'Camera',
                    handler: function () {
                        _this.cameraOptions.sourceType = _this.camera.PictureSourceType.CAMERA;
                        _this.getPicture(_this.cameraOptions);
                    }
                },
                {
                    text: 'Photo Library',
                    handler: function () {
                        _this.cameraOptions.sourceType = _this.camera.PictureSourceType.PHOTOLIBRARY;
                        _this.getPicture(_this.cameraOptions);
                    }
                },
                {
                    text: 'Cancel',
                    role: 'cancel',
                    handler: function () {
                    }
                }
            ]
        });
        actionSheet.present();
    };
    AddBlog.prototype.getPicture = function (cameraOptions) {
        var _this = this;
        this.camera.getPicture(cameraOptions).then(function (imageData) {
            _this.images.push('data:image/jpeg;base64,' + imageData);
            _this.blog_form.patchValue({ images: ['data:image/jpeg;base64,' + imageData] });
        }, function (error) { return _this.ionic$.presentToast(error); });
    };
    AddBlog.prototype.save = function (_a) {
        var _this = this;
        var value = _a.value, valid = _a.valid;
        if (valid) {
            if (value.images.length > 0) {
                this.storage$.uploadFile(value.title.replace(' ', '_'), value.images[0]).then(function (res) { return _this.blog_form.patchValue({ images: [res] }); }, function (error) { return _this.error$.handleError(error); });
            }
            this.data$.list('/blogs').push(value).then(function (appt) { return _this.ionic$.presentToast('Blog Created'); }, function (error) { return _this.error$.handleError(error); });
            this.viewCtrl.dismiss();
        }
        else {
            this.ionic$.presentToast('Form Validation Error');
        }
    };
    AddBlog = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'add-blog',template:/*ion-inline-start:"D:\Development\Current_Projects\2019_5_16_Ionic\personal-coach-app\src\pages\blogs\add-blog\add-blog.html"*/'<ion-content [formGroup]="blog_form" *ngIf="blog_form">\n  <div class="top">\n    <div class="close">\n      <ion-row>\n        <ion-col col-2>\n          <ion-icon name="md-arrow-back" (click)="viewCtrl.dismiss()"></ion-icon>\n        </ion-col>\n        <ion-col col-7>\n          <div class="profile">\n            <img [src]="mi.picture | image: \'url: profile\'">\n            <span>{{mi.fullname}}</span>\n          </div>\n        </ion-col>\n        <ion-col col-3 text-right>\n          <ion-icon name="camera" style="padding-right:10px;" (click)="addImages()"></ion-icon>\n        </ion-col>\n      </ion-row>\n\n    </div>\n\n    <div class="content">\n\n      <ion-item>\n        <ion-label floating>Title your post..?</ion-label>\n        <ion-textarea elastic-textarea formControlName="title"></ion-textarea>\n      </ion-item>\n\n    </div>\n  </div>\n  <div class="blog">\n    <div class="blog-type">\n      <p>What is your post about?</p>\n      <div *ngFor="let type of types" (click)="blogType(type)" [ngClass]="{\'active\': blog?.type === type}">{{type}}</div>\n    </div>\n    <ion-item>\n      <ion-label floating>Enter blog content..?</ion-label>\n      <ion-textarea elastic-textarea formControlName="content"></ion-textarea>\n    </ion-item>\n\n  </div>\n\n  <div class="image" *ngIf="images.length > 0">\n    <ion-slides>\n      <ion-slide *ngFor="let image of images" [style.background-image]="\'url(\' + image + \')\' | image: \'style: placeholder\'">\n      </ion-slide>\n    </ion-slides>\n  </div>\n</ion-content>\n\n\n<div class="floating">\n  <button ion-button block gradient-left box-shadow (click)="save(blog_form)">Add Post</button>\n</div>\n'/*ion-inline-end:"D:\Development\Current_Projects\2019_5_16_Ionic\personal-coach-app\src\pages\blogs\add-blog\add-blog.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["o" /* ViewController */],
            __WEBPACK_IMPORTED_MODULE_4__angular_forms__["FormBuilder"],
            __WEBPACK_IMPORTED_MODULE_5__ionic_native_camera__["a" /* Camera */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* ActionSheetController */],
            __WEBPACK_IMPORTED_MODULE_2_angularfire2_database__["a" /* AngularFireDatabase */],
            __WEBPACK_IMPORTED_MODULE_3__shared_index__["a" /* AuthProvider */],
            __WEBPACK_IMPORTED_MODULE_3__shared_index__["n" /* StorageProvider */],
            __WEBPACK_IMPORTED_MODULE_3__shared_index__["f" /* IonicProvider */],
            __WEBPACK_IMPORTED_MODULE_3__shared_index__["d" /* ErrorHandlerProvider */]])
    ], AddBlog);
    return AddBlog;
}());

//# sourceMappingURL=add-blog.js.map

/***/ }),

/***/ 1212:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return BlogComments; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(10);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_forms__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_angularfire2_database__ = __webpack_require__(17);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__shared_index__ = __webpack_require__(15);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var BlogComments = /** @class */ (function () {
    function BlogComments(viewCtrl, navParams, data$, auth$, ionic$, error$) {
        this.viewCtrl = viewCtrl;
        this.navParams = navParams;
        this.data$ = data$;
        this.auth$ = auth$;
        this.ionic$ = ionic$;
        this.error$ = error$;
        this.message = new __WEBPACK_IMPORTED_MODULE_2__angular_forms__["FormControl"]();
    }
    BlogComments.prototype.ngOnInit = function () {
        var _this = this;
        this.mi = this.auth$.currentUser;
        this.blog = this.navParams.data;
        this.data$.list('/blogs/' + this.blog.$key + '/comments/').valueChanges()
            .subscribe(function (comments) {
            return _this.comments = comments;
        });
    };
    BlogComments.prototype.ionViewDidEnter = function () {
        this.content.scrollToBottom(0);
    };
    BlogComments.prototype.delete = function (comment) {
        var _this = this;
        this.data$.list('/blogs/' + this.blog.$key + '/comments/')
            .remove(comment.$key).then(function () { return _this.content.resize(); }, function (error) { return _this.error$.handleError(error); });
    };
    BlogComments.prototype.send = function (message) {
        var _this = this;
        this.data$.list('/blogs/' + this.blog.$key + '/comments')
            .push({
            uid: this.mi.uid,
            fullname: this.mi.fullname ? this.mi.fullname : '',
            picture: this.mi.picture ? this.mi.picture : '',
            text: message,
            date: new Date().toISOString()
        }).then(function () {
            _this.message.reset();
            _this.content.scrollToBottom();
        }, function (error) { return _this.error$.handleError(error); });
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])(__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["c" /* Content */]),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["c" /* Content */])
    ], BlogComments.prototype, "content", void 0);
    BlogComments = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'blog-comments',template:/*ion-inline-start:"D:\Development\Current_Projects\2019_5_16_Ionic\personal-coach-app\src\pages\blogs\blog-comments\blog-comments.html"*/'<ion-header>\n\n  <ion-navbar>\n    <button ion-button (click)="search()">\n          <ion-icon name="search-outline"></ion-icon>\n      </button>\n    <ion-title>Comments</ion-title>\n    <ion-buttons end>\n      <button ion-button (click)="viewCtrl.dismiss()">\n          <ion-icon name="close"></ion-icon>\n      </button>\n    </ion-buttons>\n  </ion-navbar>\n</ion-header>\n\n\n<ion-content>\n  <loading *ngIf="isLoading"></loading>\n\n  <div class="container" *ngIf="!isLoading">\n\n    <div *ngIf="comments?.length > 0; then dataTemplate else noDataTemplate"></div>\n\n    <ng-template #noDataTemplate>\n      <no-data></no-data>\n    </ng-template>\n\n    <ng-template #dataTemplate>\n      <!--Filter by closest trainer-->\n      <div class="user-card" *ngFor="let comment of comments | orderBy: \'+date\'">\n        <div class="image">\n          <img [src]="comment.picture | image: \'url: profile\'">\n        </div>\n        <div class="desc">\n          <div class="info">\n            <h2>{{comment.fullname}}</h2>\n            <p>{{comment.text}}</p>\n            <p class="date">{{comment.date | amTimeAgo}}</p>\n          </div>\n        </div>\n        <div class="actions" *ngIf="comment.uid === mi.uid" (click)="delete(comment)">\n          <ion-icon name="close"></ion-icon>\n        </div>\n      </div>\n    </ng-template>\n  </div>\n</ion-content>\n<ion-footer>\n  <ion-toolbar class="custom-form">\n    <ion-item>\n      <ion-input name="message" placeholder="Type comment here..." [formControl]="message"></ion-input>\n    </ion-item>\n    <ion-buttons end>\n      <button ion-button color="primary" (click)="send(message.value)">\n         <ion-icon name="send" color="primary" item-left></ion-icon> Send\n      </button>\n    </ion-buttons>\n  </ion-toolbar>\n</ion-footer>\n'/*ion-inline-end:"D:\Development\Current_Projects\2019_5_16_Ionic\personal-coach-app\src\pages\blogs\blog-comments\blog-comments.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["o" /* ViewController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_3_angularfire2_database__["a" /* AngularFireDatabase */],
            __WEBPACK_IMPORTED_MODULE_4__shared_index__["a" /* AuthProvider */],
            __WEBPACK_IMPORTED_MODULE_4__shared_index__["f" /* IonicProvider */],
            __WEBPACK_IMPORTED_MODULE_4__shared_index__["d" /* ErrorHandlerProvider */]])
    ], BlogComments);
    return BlogComments;
}());

//# sourceMappingURL=blog-comments.js.map

/***/ }),

/***/ 1213:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return Blog; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(10);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__index__ = __webpack_require__(31);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_angularfire2_database__ = __webpack_require__(17);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__shared_index__ = __webpack_require__(15);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__ionic_native_social_sharing__ = __webpack_require__(74);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6_lodash__ = __webpack_require__(73);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6_lodash___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_6_lodash__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







var Blog = /** @class */ (function () {
    function Blog(navCtrl, navParams, modalCtrl, socialshare, data$, auth$, ionic$, error$) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.modalCtrl = modalCtrl;
        this.socialshare = socialshare;
        this.data$ = data$;
        this.auth$ = auth$;
        this.ionic$ = ionic$;
        this.error$ = error$;
    }
    Blog.prototype.ngOnInit = function () {
        this.mi = this.auth$.currentUser;
        this.blog = this.navParams.data;
    };
    Blog.prototype.likedByUser = function (blog) {
        blog.likes_count = __WEBPACK_IMPORTED_MODULE_6_lodash__["size"](blog.likes);
        blog.comments_count = __WEBPACK_IMPORTED_MODULE_6_lodash__["size"](blog.comments);
        return blog.likes && this.mi ? blog.likes[this.mi.uid] : undefined;
    };
    Blog.prototype.like = function (blog) {
        var _this = this;
        var likedbyUser = blog.likes && this.mi ? blog.likes[this.mi.uid] : undefined;
        if (!likedbyUser) {
            this.data$.object('/blogs/' + blog.$key + '/likes/' + this.mi.uid)
                .set({
                name: this.mi.fullname,
                picture: this.mi.picture,
                date: new Date()
            })
                .then(function () { return _this.ionic$.presentToast('You Liked this post'); }, function (error) { return _this.error$.handleError(error); });
        }
        else {
            this.data$.object('/blogs/' + blog.$key + '/likes/' + this.mi.uid)
                .remove().then(function () { return _this.ionic$.presentToast('You Unliked this post'); }, function (error) { return _this.error$.handleError(error); });
        }
    };
    Blog.prototype.comment = function (blog) {
        var modal = this.modalCtrl.create(__WEBPACK_IMPORTED_MODULE_2__index__["d" /* BlogComments */], blog);
        modal.present();
    };
    Blog.prototype.share = function (blog) {
        this.socialshare.share();
    };
    Blog.prototype.bookmark = function (blog) {
        var _this = this;
        var bookmarked = blog.bookmarks && this.mi ? blog.bookmarks[this.mi.uid] : undefined;
        if (!bookmarked) {
            this.data$.object('/blogs/' + blog.$key + '/bookmarks/' + this.mi.uid)
                .set(this.mi.uid)
                .then(function () { return _this.ionic$.presentToast('You have saved this post'); }, function (error) { return _this.error$.handleError(error); });
        }
        else {
            this.data$.list('/blogs/' + blog.$key + '/bookmarks/' + this.mi.uid)
                .remove()
                .then(function () { return _this.ionic$.presentToast('You have saved this post'); }, function (error) { return _this.error$.handleError(error); });
        }
    };
    Blog = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'blog',template:/*ion-inline-start:"D:\Development\Current_Projects\2019_5_16_Ionic\personal-coach-app\src\pages\blogs\blog\blog.html"*/'<ion-header>\n  <ion-navbar [ngClass]="{\'dark\': blog?.images?.length < 1}">\n    <button ion-button menuToggle>\n      <ion-icon name="menu-outline"></ion-icon>\n    </button>\n    <ion-title>blog</ion-title>\n  </ion-navbar>\n</ion-header>\n\n<!--Styles available based on if the blog has an image or not.\n\'dark\' class is applied to toolbar, giving it a gradient color \nwhen no image is available for the post and \'no-image\' class is \napplied to the \'blog-card\' class to allow extra padding for the \ncontent section of the page-->\n<ion-content>\n  <div class="blog-card" [ngClass]="{\'no-image\': blog?.images?.length < 1}">\n    <ion-slides slidesPerView="1" *ngIf="blog?.images?.length > 0" box-shadow>\n      <ion-slide *ngFor="let image of blog.images" [style.background-image]="\'url(\' + image + \')\' | image: \'style: placeholder\'">\n      </ion-slide>\n    </ion-slides>\n    <div class="blog-type" *ngIf="blog?.images?.length > 0" gradient-left box-shadow>\n      {{blog.type}}\n    </div>\n    <div class="content" (click)="open(blog)">\n      <h2>{{blog.title}}</h2>\n      <p>{{blog.content}}</p>\n      <div class="blog-type" *ngIf="blog?.images?.length < 0" gradient-left box-shadow>\n      {{blog.type}}\n    </div>\n    </div>\n    <ion-row>\n      <ion-col class="date">{{blog.created_at | amTimeAgo}}</ion-col>\n      <ion-col text-right>\n        <span (click)="like(blog)"><ion-icon [name]="likedByUser(blog) ? \'heart\' : \'heart-outline\'"></ion-icon>{{blog?.likes_count > 0 ? blog?.likes_count : \'\'}}</span>\n        <span (click)="comment(blog)"><ion-icon name="quote-outline"></ion-icon>{{blog?.comments_count > 0 ? blog?.comments_count : \'\'}}</span>\n        <span (click)="bookmark(blog)"><ion-icon name="bookmark-outline"></ion-icon></span>\n        <span (click)="share(blog)"><ion-icon name="md-share"></ion-icon></span>\n      </ion-col>\n    </ion-row>\n  </div>\n</ion-content>\n'/*ion-inline-end:"D:\Development\Current_Projects\2019_5_16_Ionic\personal-coach-app\src\pages\blogs\blog\blog.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* ModalController */],
            __WEBPACK_IMPORTED_MODULE_5__ionic_native_social_sharing__["a" /* SocialSharing */],
            __WEBPACK_IMPORTED_MODULE_3_angularfire2_database__["a" /* AngularFireDatabase */],
            __WEBPACK_IMPORTED_MODULE_4__shared_index__["a" /* AuthProvider */],
            __WEBPACK_IMPORTED_MODULE_4__shared_index__["f" /* IonicProvider */],
            __WEBPACK_IMPORTED_MODULE_4__shared_index__["d" /* ErrorHandlerProvider */]])
    ], Blog);
    return Blog;
}());

//# sourceMappingURL=blog.js.map

/***/ }),

/***/ 1214:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SharedModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(10);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__index__ = __webpack_require__(15);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_native_keyboard__ = __webpack_require__(220);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};




var SharedModule = /** @class */ (function () {
    function SharedModule() {
    }
    SharedModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* IonicModule */]
            ],
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__index__["m" /* SanitizePipe */],
                __WEBPACK_IMPORTED_MODULE_2__index__["l" /* SanitizeImagePipe */],
                __WEBPACK_IMPORTED_MODULE_2__index__["h" /* Loading */],
                __WEBPACK_IMPORTED_MODULE_2__index__["k" /* Rating */],
                __WEBPACK_IMPORTED_MODULE_2__index__["i" /* NoData */],
                __WEBPACK_IMPORTED_MODULE_2__index__["c" /* ElasticTextareaDirective */],
                __WEBPACK_IMPORTED_MODULE_2__index__["g" /* KeyboardAttachDirective */],
                __WEBPACK_IMPORTED_MODULE_2__index__["j" /* OrderByPipe */]
            ],
            exports: [
                __WEBPACK_IMPORTED_MODULE_2__index__["m" /* SanitizePipe */],
                __WEBPACK_IMPORTED_MODULE_2__index__["l" /* SanitizeImagePipe */],
                __WEBPACK_IMPORTED_MODULE_2__index__["h" /* Loading */],
                __WEBPACK_IMPORTED_MODULE_2__index__["k" /* Rating */],
                __WEBPACK_IMPORTED_MODULE_2__index__["i" /* NoData */],
                __WEBPACK_IMPORTED_MODULE_2__index__["c" /* ElasticTextareaDirective */],
                __WEBPACK_IMPORTED_MODULE_2__index__["g" /* KeyboardAttachDirective */],
                __WEBPACK_IMPORTED_MODULE_2__index__["j" /* OrderByPipe */]
            ],
            providers: [
                __WEBPACK_IMPORTED_MODULE_3__ionic_native_keyboard__["a" /* Keyboard */]
            ]
        })
    ], SharedModule);
    return SharedModule;
}());

//# sourceMappingURL=shared.module.js.map

/***/ }),

/***/ 15:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__providers_auth_provider__ = __webpack_require__(793);
/* harmony namespace reexport (by used) */ __webpack_require__.d(__webpack_exports__, "a", function() { return __WEBPACK_IMPORTED_MODULE_0__providers_auth_provider__["a"]; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__providers_ionic_provider__ = __webpack_require__(540);
/* harmony namespace reexport (by used) */ __webpack_require__.d(__webpack_exports__, "b", function() { return __WEBPACK_IMPORTED_MODULE_1__providers_ionic_provider__["a"]; });
/* harmony namespace reexport (by used) */ __webpack_require__.d(__webpack_exports__, "f", function() { return __WEBPACK_IMPORTED_MODULE_1__providers_ionic_provider__["b"]; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_places_provider__ = __webpack_require__(829);
/* harmony namespace reexport (by used) */ __webpack_require__.d(__webpack_exports__, "e", function() { return __WEBPACK_IMPORTED_MODULE_2__providers_places_provider__["a"]; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_storage_provider__ = __webpack_require__(213);
/* harmony namespace reexport (by used) */ __webpack_require__.d(__webpack_exports__, "n", function() { return __WEBPACK_IMPORTED_MODULE_3__providers_storage_provider__["a"]; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__providers_error_handler_provider__ = __webpack_require__(830);
/* harmony namespace reexport (by used) */ __webpack_require__.d(__webpack_exports__, "d", function() { return __WEBPACK_IMPORTED_MODULE_4__providers_error_handler_provider__["a"]; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__widgets_loading_loading__ = __webpack_require__(831);
/* harmony namespace reexport (by used) */ __webpack_require__.d(__webpack_exports__, "h", function() { return __WEBPACK_IMPORTED_MODULE_5__widgets_loading_loading__["a"]; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__widgets_no_data_no_data__ = __webpack_require__(832);
/* harmony namespace reexport (by used) */ __webpack_require__.d(__webpack_exports__, "i", function() { return __WEBPACK_IMPORTED_MODULE_6__widgets_no_data_no_data__["a"]; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__widgets_rating_rating__ = __webpack_require__(833);
/* harmony namespace reexport (by used) */ __webpack_require__.d(__webpack_exports__, "k", function() { return __WEBPACK_IMPORTED_MODULE_7__widgets_rating_rating__["a"]; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__directives_keyboard_attach_directive__ = __webpack_require__(834);
/* harmony namespace reexport (by used) */ __webpack_require__.d(__webpack_exports__, "g", function() { return __WEBPACK_IMPORTED_MODULE_8__directives_keyboard_attach_directive__["a"]; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__directives_elastic_directive__ = __webpack_require__(835);
/* harmony namespace reexport (by used) */ __webpack_require__.d(__webpack_exports__, "c", function() { return __WEBPACK_IMPORTED_MODULE_9__directives_elastic_directive__["a"]; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__pipes_sanitize_pipe__ = __webpack_require__(836);
/* harmony namespace reexport (by used) */ __webpack_require__.d(__webpack_exports__, "m", function() { return __WEBPACK_IMPORTED_MODULE_10__pipes_sanitize_pipe__["a"]; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__pipes_sanitize_image_pipe__ = __webpack_require__(837);
/* harmony namespace reexport (by used) */ __webpack_require__.d(__webpack_exports__, "l", function() { return __WEBPACK_IMPORTED_MODULE_11__pipes_sanitize_image_pipe__["a"]; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12__pipes_order_by_pipe__ = __webpack_require__(838);
/* harmony namespace reexport (by used) */ __webpack_require__.d(__webpack_exports__, "j", function() { return __WEBPACK_IMPORTED_MODULE_12__pipes_order_by_pipe__["a"]; });













//# sourceMappingURL=index.js.map

/***/ }),

/***/ 213:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return StorageProvider; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_angularfire2__ = __webpack_require__(60);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_firebase__ = __webpack_require__(800);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_firebase___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_firebase__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};



var StorageProvider = /** @class */ (function () {
    function StorageProvider(firebaseApp) {
        this.storage = __WEBPACK_IMPORTED_MODULE_2_firebase___default.a.storage();
    }
    StorageProvider.prototype.uploadFile = function (filename, data) {
        var storageRef = __WEBPACK_IMPORTED_MODULE_2_firebase___default.a.storage().ref().child(filename);
        return storageRef
            .putString(data, 'data_url')
            .then(function (snapshot) { return snapshot.downloadURL; })
            .catch(function (error) { return error; });
    };
    StorageProvider = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(),
        __param(0, Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Inject"])(__WEBPACK_IMPORTED_MODULE_1_angularfire2__["b" /* FirebaseApp */])),
        __metadata("design:paramtypes", [Object])
    ], StorageProvider);
    return StorageProvider;
}());

//# sourceMappingURL=storage.provider.js.map

/***/ }),

/***/ 249:
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncatched exception popping up in devtools
	return Promise.resolve().then(function() {
		throw new Error("Cannot find module '" + req + "'.");
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = 249;

/***/ }),

/***/ 29:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__signup_signup__ = __webpack_require__(840);
/* harmony namespace reexport (by used) */ __webpack_require__.d(__webpack_exports__, "h", function() { return __WEBPACK_IMPORTED_MODULE_0__signup_signup__["a"]; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__login_login__ = __webpack_require__(930);
/* harmony namespace reexport (by used) */ __webpack_require__.d(__webpack_exports__, "c", function() { return __WEBPACK_IMPORTED_MODULE_1__login_login__["a"]; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__calendar_calendar__ = __webpack_require__(931);
/* harmony namespace reexport (by used) */ __webpack_require__.d(__webpack_exports__, "a", function() { return __WEBPACK_IMPORTED_MODULE_2__calendar_calendar__["a"]; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__chat_chat__ = __webpack_require__(938);
/* harmony namespace reexport (by used) */ __webpack_require__.d(__webpack_exports__, "b", function() { return __WEBPACK_IMPORTED_MODULE_3__chat_chat__["a"]; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__map_map__ = __webpack_require__(939);
/* harmony namespace reexport (by used) */ __webpack_require__.d(__webpack_exports__, "d", function() { return __WEBPACK_IMPORTED_MODULE_4__map_map__["a"]; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__search_search__ = __webpack_require__(940);
/* harmony namespace reexport (by used) */ __webpack_require__.d(__webpack_exports__, "g", function() { return __WEBPACK_IMPORTED_MODULE_5__search_search__["a"]; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__notifications_notifications__ = __webpack_require__(941);
/* harmony namespace reexport (by used) */ __webpack_require__.d(__webpack_exports__, "f", function() { return __WEBPACK_IMPORTED_MODULE_6__notifications_notifications__["a"]; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__notification_form_notification_form__ = __webpack_require__(942);
/* harmony namespace reexport (by used) */ __webpack_require__.d(__webpack_exports__, "e", function() { return __WEBPACK_IMPORTED_MODULE_7__notification_form_notification_form__["a"]; });








//# sourceMappingURL=index.js.map

/***/ }),

/***/ 295:
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncatched exception popping up in devtools
	return Promise.resolve().then(function() {
		throw new Error("Cannot find module '" + req + "'.");
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = 295;

/***/ }),

/***/ 31:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__walkthrough_walkthrough__ = __webpack_require__(839);
/* harmony namespace reexport (by used) */ __webpack_require__.d(__webpack_exports__, "u", function() { return __WEBPACK_IMPORTED_MODULE_0__walkthrough_walkthrough__["a"]; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__trainers_trainers__ = __webpack_require__(943);
/* harmony namespace reexport (by used) */ __webpack_require__.d(__webpack_exports__, "t", function() { return __WEBPACK_IMPORTED_MODULE_1__trainers_trainers__["a"]; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__trainers_trainer_trainer__ = __webpack_require__(944);
/* harmony namespace reexport (by used) */ __webpack_require__.d(__webpack_exports__, "s", function() { return __WEBPACK_IMPORTED_MODULE_2__trainers_trainer_trainer__["a"]; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__trainers_trainer_appointment_appointment__ = __webpack_require__(945);
/* harmony namespace reexport (by used) */ __webpack_require__.d(__webpack_exports__, "b", function() { return __WEBPACK_IMPORTED_MODULE_3__trainers_trainer_appointment_appointment__["a"]; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__intro_intro__ = __webpack_require__(946);
/* harmony namespace reexport (by used) */ __webpack_require__.d(__webpack_exports__, "j", function() { return __WEBPACK_IMPORTED_MODULE_4__intro_intro__["a"]; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__gyms_gyms__ = __webpack_require__(947);
/* harmony namespace reexport (by used) */ __webpack_require__.d(__webpack_exports__, "i", function() { return __WEBPACK_IMPORTED_MODULE_5__gyms_gyms__["a"]; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__gyms_gym_gym__ = __webpack_require__(948);
/* harmony namespace reexport (by used) */ __webpack_require__.d(__webpack_exports__, "h", function() { return __WEBPACK_IMPORTED_MODULE_6__gyms_gym_gym__["a"]; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__me_me__ = __webpack_require__(949);
/* harmony namespace reexport (by used) */ __webpack_require__.d(__webpack_exports__, "l", function() { return __WEBPACK_IMPORTED_MODULE_7__me_me__["a"]; });
/* harmony namespace reexport (by used) */ __webpack_require__.d(__webpack_exports__, "m", function() { return __WEBPACK_IMPORTED_MODULE_7__me_me__["b"]; });
/* harmony namespace reexport (by used) */ __webpack_require__.d(__webpack_exports__, "n", function() { return __WEBPACK_IMPORTED_MODULE_7__me_me__["c"]; });
/* harmony namespace reexport (by used) */ __webpack_require__.d(__webpack_exports__, "o", function() { return __WEBPACK_IMPORTED_MODULE_7__me_me__["d"]; });
/* harmony namespace reexport (by used) */ __webpack_require__.d(__webpack_exports__, "p", function() { return __WEBPACK_IMPORTED_MODULE_7__me_me__["e"]; });
/* harmony namespace reexport (by used) */ __webpack_require__.d(__webpack_exports__, "q", function() { return __WEBPACK_IMPORTED_MODULE_7__me_me__["f"]; });
/* harmony namespace reexport (by used) */ __webpack_require__.d(__webpack_exports__, "r", function() { return __WEBPACK_IMPORTED_MODULE_7__me_me__["g"]; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__blogs_blogs__ = __webpack_require__(952);
/* harmony namespace reexport (by used) */ __webpack_require__.d(__webpack_exports__, "e", function() { return __WEBPACK_IMPORTED_MODULE_8__blogs_blogs__["a"]; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__blogs_fitness_blog_fitness_blog__ = __webpack_require__(953);
/* harmony namespace reexport (by used) */ __webpack_require__.d(__webpack_exports__, "f", function() { return __WEBPACK_IMPORTED_MODULE_9__blogs_fitness_blog_fitness_blog__["a"]; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__blogs_food_blog_food_blog__ = __webpack_require__(1209);
/* harmony namespace reexport (by used) */ __webpack_require__.d(__webpack_exports__, "g", function() { return __WEBPACK_IMPORTED_MODULE_10__blogs_food_blog_food_blog__["a"]; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__blogs_lifestyle_blog_lifestyle_blog__ = __webpack_require__(1210);
/* harmony namespace reexport (by used) */ __webpack_require__.d(__webpack_exports__, "k", function() { return __WEBPACK_IMPORTED_MODULE_11__blogs_lifestyle_blog_lifestyle_blog__["a"]; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12__blogs_add_blog_add_blog__ = __webpack_require__(1211);
/* harmony namespace reexport (by used) */ __webpack_require__.d(__webpack_exports__, "a", function() { return __WEBPACK_IMPORTED_MODULE_12__blogs_add_blog_add_blog__["a"]; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_13__blogs_blog_comments_blog_comments__ = __webpack_require__(1212);
/* harmony namespace reexport (by used) */ __webpack_require__.d(__webpack_exports__, "d", function() { return __WEBPACK_IMPORTED_MODULE_13__blogs_blog_comments_blog_comments__["a"]; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_14__blogs_blog_blog__ = __webpack_require__(1213);
/* harmony namespace reexport (by used) */ __webpack_require__.d(__webpack_exports__, "c", function() { return __WEBPACK_IMPORTED_MODULE_14__blogs_blog_blog__["a"]; });















//# sourceMappingURL=index.js.map

/***/ }),

/***/ 523:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return APP_CONFIG; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "b", function() { return FIREBASE_CONFIG; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "c", function() { return GOOGLE_MAP_API_KEY; });
// Ionic Config settings
var APP_CONFIG = {
    iconMode: 'ios',
    mode: 'ios',
    menuType: 'reveal',
    tabsHideOnSubPages: true,
    tabsPlacement: 'bottom',
    dayShortNames: ['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat'],
    backButtonText: '',
    backButtonIcon: 'arrow-back-outline'
};
// Firebase Settings
var FIREBASE_CONFIG = {
    apiKey: "AIzaSyAzFBJ8LdR3deA7nwJcKEDr0V10nw4aUL4",
    authDomain: "sweltering-heat-1376.firebaseapp.com",
    databaseURL: "https://sweltering-heat-1376.firebaseio.com",
    storageBucket: "sweltering-heat-1376.appspot.com",
    messagingSenderId: "304758784392"
};
// https://developers.google.com/maps/documentation/javascript/get-api-key
var GOOGLE_MAP_API_KEY = 'AIzaSyCBEPGtBVChpQpQgJzsbpJNrZpQomn-Nec';
//# sourceMappingURL=base.url.js.map

/***/ }),

/***/ 540:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "b", function() { return IonicProvider; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return Connectivity; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(10);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_native_network__ = __webpack_require__(541);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var IonicProvider = /** @class */ (function () {
    function IonicProvider(alertCtrl, toastCtrl) {
        this.alertCtrl = alertCtrl;
        this.toastCtrl = toastCtrl;
    }
    IonicProvider.prototype.presentToast = function (message, duration) {
        var durationTime = 0;
        if (duration === 'long') {
            durationTime = 5000;
        }
        else if (duration === 'short') {
            durationTime = 2500;
        }
        else {
            durationTime = 3500;
        }
        var toast = this.toastCtrl.create({
            message: message,
            duration: durationTime
        });
        toast.present();
    };
    IonicProvider.prototype.showAlert = function (title, subTitle, buttons) {
        !buttons ? buttons = "OK" : null;
        var alert = this.alertCtrl.create({
            title: title,
            subTitle: subTitle,
            buttons: [buttons]
        });
        alert.present();
    };
    // formular extracted from http://www.movable-type.co.uk/scripts/latlong.html
    IonicProvider.prototype.calculateDistance = function (currentCoords, destinationCoords) {
        var R = 6371; // kilometres
        var φ1 = this.toRadians(currentCoords[0]);
        var φ2 = this.toRadians(destinationCoords[0]);
        var Δφ = this.toRadians((destinationCoords[0] - currentCoords[0]));
        var Δλ = this.toRadians((destinationCoords[1] - currentCoords[1]));
        var a = Math.sin(Δφ / 2) * Math.sin(Δφ / 2) +
            Math.cos(φ1) * Math.cos(φ2) *
                Math.sin(Δλ / 2) * Math.sin(Δλ / 2);
        var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
        return R * c;
    };
    IonicProvider.prototype.toRadians = function (angle) {
        return angle * (Math.PI / 180);
    };
    IonicProvider = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["b" /* AlertController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["n" /* ToastController */]])
    ], IonicProvider);
    return IonicProvider;
}());



var Connectivity = /** @class */ (function () {
    function Connectivity(platform, network) {
        this.platform = platform;
        this.network = network;
        this.onDevice = this.platform.is('cordova');
    }
    Connectivity.prototype.isOnline = function () {
        if (this.onDevice && this.network.type) {
            return this.network.type != 'none';
        }
        else {
            return navigator.onLine;
        }
    };
    Connectivity.prototype.isOffline = function () {
        if (this.onDevice && this.network.type) {
            return this.network.type == 'none';
        }
        else {
            return !navigator.onLine;
        }
    };
    Connectivity.prototype.watchOnline = function () {
        return this.network.onConnect();
    };
    Connectivity.prototype.watchOffline = function () {
        return this.network.onDisconnect();
    };
    Connectivity = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* Platform */], __WEBPACK_IMPORTED_MODULE_2__ionic_native_network__["a" /* Network */]])
    ], Connectivity);
    return Connectivity;
}());

//# sourceMappingURL=ionic.provider.js.map

/***/ }),

/***/ 585:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MiProfile; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(10);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_forms__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__shared_index__ = __webpack_require__(15);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__ionic_native_camera__ = __webpack_require__(138);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_angularfire2_database__ = __webpack_require__(17);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






var MiProfile = /** @class */ (function () {
    function MiProfile(fb, actionSheetCtrl, camera, data$, auth$, storage$, ionic$, error$) {
        this.fb = fb;
        this.actionSheetCtrl = actionSheetCtrl;
        this.camera = camera;
        this.data$ = data$;
        this.auth$ = auth$;
        this.storage$ = storage$;
        this.ionic$ = ionic$;
        this.error$ = error$;
        this.cameraOptions = {};
    }
    MiProfile.prototype.ngOnInit = function () {
        this.mi = this.auth$.currentUser;
        var ageDifMs = Date.now() - new Date(this.mi.dob).getTime();
        var ageDate = new Date(ageDifMs);
        this.age = Math.abs(ageDate.getUTCFullYear() - 1970);
    };
    MiProfile = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'mi-profile',template:/*ion-inline-start:"D:\Development\Current_Projects\2019_5_16_Ionic\personal-coach-app\src\pages\me\mi-profile\mi-profile.html"*/'<ion-header>\n\n  <ion-navbar>\n    <button ion-button menuToggle>\n      <ion-icon name="menu-outline"></ion-icon>\n    </button>\n    <ion-title>Profile</ion-title>\n  </ion-navbar>\n\n</ion-header>\n\n<ion-content padding>\n  <loading *ngIf="isLoading"></loading>\n  <div class="container custom-floating-form" *ngIf="!isLoading">\n      <div class="user-image">\n        <img [src]="mi?.picture | image: \'url:profile\'">\n      </div>\n       <h2>{{mi?.fullname}}</h2>\n\n      <div class="scroll">\n        <ion-card class="profile">\n          <ion-row>\n            <ion-col>gender  <p>{{mi.gender}}</p></ion-col>\n            <ion-col>age <p>{{age}}</p></ion-col>\n            <ion-col>weight <p>{{mi.weight}}kg</p></ion-col>\n          </ion-row>\n          <ion-row>\n            <ion-col>height <p>{{mi.height}}cm</p></ion-col>\n            <ion-col>BMI<p>{{mi.bmi}}%</p></ion-col>\n            <ion-col>Body Fat <p>{{mi.bfp}}%</p></ion-col>\n          </ion-row>\n        </ion-card>\n        <ion-card class="about">\n          <h2>About Me</h2>\n          <p>{{mi.about ? mi.about : \'No content added yet\'}}</p>\n        </ion-card>\n      </div>\n  </div>\n</ion-content>\n\n'/*ion-inline-end:"D:\Development\Current_Projects\2019_5_16_Ionic\personal-coach-app\src\pages\me\mi-profile\mi-profile.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_2__angular_forms__["FormBuilder"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* ActionSheetController */],
            __WEBPACK_IMPORTED_MODULE_4__ionic_native_camera__["a" /* Camera */],
            __WEBPACK_IMPORTED_MODULE_5_angularfire2_database__["a" /* AngularFireDatabase */],
            __WEBPACK_IMPORTED_MODULE_3__shared_index__["a" /* AuthProvider */],
            __WEBPACK_IMPORTED_MODULE_3__shared_index__["n" /* StorageProvider */],
            __WEBPACK_IMPORTED_MODULE_3__shared_index__["f" /* IonicProvider */],
            __WEBPACK_IMPORTED_MODULE_3__shared_index__["d" /* ErrorHandlerProvider */]])
    ], MiProfile);
    return MiProfile;
}());

//# sourceMappingURL=mi-profile.js.map

/***/ }),

/***/ 586:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MiReads; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(10);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_angularfire2_database__ = __webpack_require__(17);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__shared_index__ = __webpack_require__(15);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__components_index__ = __webpack_require__(29);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__index__ = __webpack_require__(31);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__ionic_native_social_sharing__ = __webpack_require__(74);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7_lodash__ = __webpack_require__(73);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7_lodash___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_7_lodash__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};








var MiReads = /** @class */ (function () {
    function MiReads(navCtrl, navParams, modalCtrl, alertCtrl, socialshare, data$, auth$, ionic$, error$) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.modalCtrl = modalCtrl;
        this.alertCtrl = alertCtrl;
        this.socialshare = socialshare;
        this.data$ = data$;
        this.auth$ = auth$;
        this.ionic$ = ionic$;
        this.error$ = error$;
        this.blog_type = 'food';
    }
    MiReads.prototype.ngOnInit = function () {
        this.mi = this.auth$.currentUser;
    };
    MiReads.prototype.ionViewDidEnter = function () {
        this.getData();
    };
    MiReads.prototype.getData = function () {
        var _this = this;
        this.isLoading = true;
        this.data$.list('/blogs/').valueChanges()
            .subscribe(function (blogs) {
            _this.isLoading = false;
            _this.blogs = blogs.filter(function (blog) { return blog.bookmarks ? blog.bookmarks[_this.mi.uid] : null; });
        });
    };
    MiReads.prototype.search = function () {
        var _this = this;
        var modal = this.modalCtrl.create(__WEBPACK_IMPORTED_MODULE_4__components_index__["g" /* Search */], { searchable: ['title', 'type', 'content'] });
        modal.onDidDismiss(function (search) {
            if (search) {
                _this.blogs.filter(function (blog) {
                    return search.title ? blog.title.toLowerCase().indexOf(search.title.toLowerCase()) > -1 : null
                        || search.type ? blog.type.toLowerCase().indexOf(search.type.toLowerCase()) > -1 : null
                        || search.content ? blog.content.toLowerCase().indexOf(search.content.toLowerCase()) > -1 : null;
                });
            }
        });
        modal.present();
    };
    MiReads.prototype.open = function (blog) {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_5__index__["c" /* Blog */], blog);
    };
    MiReads.prototype.likedByUser = function (blog) {
        blog.likes_count = __WEBPACK_IMPORTED_MODULE_7_lodash__["size"](blog.likes);
        blog.comments_count = __WEBPACK_IMPORTED_MODULE_7_lodash__["size"](blog.comments);
        return blog.likes ? blog.likes[this.mi.uid] : undefined;
    };
    MiReads.prototype.bookmarked = function (blog) {
        return blog.bookmarks ? blog.bookmarks[this.mi.uid] : undefined;
    };
    MiReads.prototype.like = function (blog) {
        var _this = this;
        var likedbyUser = blog.likes ? blog.likes[this.mi.uid] : undefined;
        if (!likedbyUser) {
            this.data$.object('/blogs/' + blog.$key + '/likes/' + this.mi.uid)
                .set({
                name: this.mi.fullname ? this.mi.fullname : '',
                picture: this.mi.picture ? this.mi.picture : '',
                date: new Date()
            })
                .then(function () { return _this.ionic$.presentToast('You Liked this post'); }, function (error) { return _this.error$.handleError(error); });
        }
        else {
            this.data$.object('/blogs/' + blog.$key + '/likes/' + this.mi.uid)
                .remove().then(function () { return _this.ionic$.presentToast('You Unliked this post'); }, function (error) { return _this.error$.handleError(error); });
        }
    };
    MiReads.prototype.comment = function (blog) {
        var modal = this.modalCtrl.create(__WEBPACK_IMPORTED_MODULE_5__index__["d" /* BlogComments */], blog);
        modal.present();
    };
    MiReads.prototype.share = function (blog) {
        this.socialshare.share();
    };
    MiReads.prototype.delete = function (blog) {
        var _this = this;
        var alert = this.alertCtrl.create({
            title: 'Confirm Delete',
            message: 'Are you sure you want to delete this post?',
            buttons: [
                {
                    text: 'Cancel',
                    role: 'cancel',
                    handler: function () {
                    }
                },
                {
                    text: 'Delete',
                    cssClass: 'primary-button',
                    handler: function () {
                        _this.data$.object('/blogs/' + blog.$key).remove()
                            .then(function () { return _this.ionic$.presentToast('You have deleted this post'); }, function (error) { return _this.error$.handleError(error); });
                    }
                }
            ]
        });
        alert.present();
    };
    MiReads.prototype.bookmark = function (blog) {
        var _this = this;
        var bookmarked = blog.bookmarks ? blog.bookmarks[this.mi.uid] : undefined;
        if (!bookmarked) {
            this.data$.object('/blogs/' + blog.$key + '/bookmarks/' + this.mi.uid)
                .set(this.mi.uid)
                .then(function () { return _this.ionic$.presentToast('You have saved this post'); }, function (error) { return _this.error$.handleError(error); });
        }
        else {
            this.data$.list('/blogs/' + blog.$key + '/bookmarks/' + this.mi.uid)
                .remove()
                .then(function () { return _this.ionic$.presentToast('You have saved this post'); }, function (error) { return _this.error$.handleError(error); });
        }
    };
    MiReads = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'mi-reads',template:/*ion-inline-start:"D:\Development\Current_Projects\2019_5_16_Ionic\personal-coach-app\src\pages\me\mi-reads\mi-reads.html"*/'<ion-header>\n\n  <ion-navbar>\n    <button ion-button menuToggle>\n      <ion-icon name="menu-outline"></ion-icon>\n    </button>\n    <ion-title>Reads</ion-title>\n    <ion-buttons end>\n      <button ion-button>\n          <ion-icon name="search-outline"></ion-icon>\n        </button>\n      <button ion-button>\n          <ion-icon name="settings-outline"></ion-icon>\n        </button>\n    </ion-buttons>\n  </ion-navbar>\n\n</ion-header>\n\n<ion-content>\n  <loading *ngIf="isLoading"></loading>\n\n  <div *ngIf="!isLoading">\n    <div *ngIf="blogs?.length > 0; then dataTemplate else noDataTemplate"></div>\n\n    <ng-template #noDataTemplate>\n      <no-data></no-data>\n    </ng-template>\n\n    <ng-template #dataTemplate>\n      <ion-card class="blog-card" *ngFor="let blog of blogs">\n        <div class="image" *ngIf="blog?.images?.length > 0" (click)="open(blog)" [style.background-image]="\'url(\' + blog?.images[0] + \')\' | image: \'style: placeholder\'"></div>\n        <div class="content" (click)="open(blog)">\n          <p>{{blog.content| slice: 0: 140}}</p>\n          {{blog.type}}\n        </div>\n        <ion-row>\n          <ion-col col-4 class="date">{{blog.created_at | amTimeAgo}}</ion-col>\n          <ion-col text-right>\n            <span (click)="like(blog)"><ion-icon [name]="likedByUser(blog) ? \'heart\' : \'heart-outline\'"></ion-icon>{{blog?.likes_count > 0 ? blog?.likes_count : \'\'}}</span>\n            <span (click)="comment(blog)"><ion-icon name="quote-outline"></ion-icon>{{blog?.comments_count > 0 ? blog?.comments_count : \'\'}}</span>\n            <span (click)="bookmark(blog)"><ion-icon name="bookmark-outline"></ion-icon></span>\n            <span (click)="share(blog)"><ion-icon name="md-share"></ion-icon></span>\n            <span (click)="delete(blog)" *ngIf="blog?.user?.uid === mi.uid"><ion-icon name="trash" color="danger"></ion-icon></span>\n          </ion-col>\n        </ion-row>\n      </ion-card>\n    </ng-template>\n  </div>\n\n</ion-content>\n'/*ion-inline-end:"D:\Development\Current_Projects\2019_5_16_Ionic\personal-coach-app\src\pages\me\mi-reads\mi-reads.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* ModalController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["b" /* AlertController */],
            __WEBPACK_IMPORTED_MODULE_6__ionic_native_social_sharing__["a" /* SocialSharing */],
            __WEBPACK_IMPORTED_MODULE_2_angularfire2_database__["a" /* AngularFireDatabase */],
            __WEBPACK_IMPORTED_MODULE_3__shared_index__["a" /* AuthProvider */],
            __WEBPACK_IMPORTED_MODULE_3__shared_index__["f" /* IonicProvider */],
            __WEBPACK_IMPORTED_MODULE_3__shared_index__["d" /* ErrorHandlerProvider */]])
    ], MiReads);
    return MiReads;
}());

//# sourceMappingURL=mi-reads.js.map

/***/ }),

/***/ 587:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MiSchedule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(10);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__components_index__ = __webpack_require__(29);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_moment__ = __webpack_require__(2);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_moment___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_moment__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_angularfire2_database__ = __webpack_require__(17);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var MiSchedule = /** @class */ (function () {
    function MiSchedule(navParams, modalCtrl, data$) {
        this.navParams = navParams;
        this.modalCtrl = modalCtrl;
        this.data$ = data$;
    }
    MiSchedule.prototype.ngOnInit = function () {
        this.cal_options = {
            display: 'gradient',
            showEvents: true,
            showEventDots: true,
            navType: 'left-right',
            isPage: true,
            showMenu: true
        };
        this.user = this.navParams.data;
    };
    MiSchedule.prototype.getCurrentUser = function () {
        var _this = this;
        this.data$.object('/users/' + this.mi.uid).valueChanges()
            .subscribe(function (user) {
            _this.user = user;
        });
    };
    MiSchedule.prototype.onEventsChange = function (day) {
        this.selected_day = day;
    };
    MiSchedule.prototype.createNotification = function (day) {
        var params = {
            user: this.user,
            notification: {
                extras: {},
                with: this.user.uid,
                start: __WEBPACK_IMPORTED_MODULE_3_moment___default()(day).toISOString(),
                end: __WEBPACK_IMPORTED_MODULE_3_moment___default()(day).toISOString(),
                recurrence: 'none',
                color: '#' + Math.floor(Math.random() * 16777215).toString(16),
                start_time: __WEBPACK_IMPORTED_MODULE_3_moment___default()().startOf('date').toISOString(),
                end_time: __WEBPACK_IMPORTED_MODULE_3_moment___default()().endOf('date').toISOString(),
            }
        };
        var modal = this.modalCtrl.create(__WEBPACK_IMPORTED_MODULE_2__components_index__["e" /* NotificationForm */], params);
        modal.onDidDismiss(function () { });
        modal.present();
    };
    MiSchedule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'mi-schedule',template:/*ion-inline-start:"D:\Development\Current_Projects\2019_5_16_Ionic\personal-coach-app\src\pages\me\mi-schedule\mi-schedule.html"*/'<ion-header>\n\n  <ion-navbar>\n    <button ion-button menuToggle>\n      <ion-icon name="menu-outline"></ion-icon>\n    </button>\n    <ion-title>Mi Schedule</ion-title>\n  </ion-navbar>\n\n</ion-header>\n<ion-content>\n  <calendar [options]="cal_options" [user]="user" (events)="onEventsChange($event)"></calendar>\n</ion-content>\n\n<div class="floating">\n  <button ion-button block gradient-left box-shadow (click)="createNotification(selected_day?.day)">Create a Notification</button>\n</div>'/*ion-inline-end:"D:\Development\Current_Projects\2019_5_16_Ionic\personal-coach-app\src\pages\me\mi-schedule\mi-schedule.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* ModalController */],
            __WEBPACK_IMPORTED_MODULE_4_angularfire2_database__["a" /* AngularFireDatabase */]])
    ], MiSchedule);
    return MiSchedule;
}());

//# sourceMappingURL=mi-schedule.js.map

/***/ }),

/***/ 588:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MiGoals; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(10);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_angularfire2_database__ = __webpack_require__(17);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__shared_index__ = __webpack_require__(15);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__index__ = __webpack_require__(31);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var MiGoals = /** @class */ (function () {
    function MiGoals(navCtrl, data$, auth$, ionic$, error$) {
        this.navCtrl = navCtrl;
        this.data$ = data$;
        this.auth$ = auth$;
        this.ionic$ = ionic$;
        this.error$ = error$;
    }
    MiGoals.prototype.ngOnInit = function () {
        this.mi = this.auth$.currentUser;
        this.mi.lbm = +this.mi.weight - (+this.mi.weight * +this.mi.bfp / 100); //calculate lean body mass
    };
    MiGoals.prototype.profile = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_4__index__["p" /* MiProfileForm */]);
    };
    MiGoals.prototype.health = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_4__index__["n" /* MiHealth */]);
    };
    MiGoals = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'mi-goals',template:/*ion-inline-start:"D:\Development\Current_Projects\2019_5_16_Ionic\personal-coach-app\src\pages\me\mi-goals\mi-goals.html"*/'<ion-header>\n\n  <ion-navbar>\n    <button ion-button menuToggle>\n      <ion-icon name="menu-outline"></ion-icon>\n    </button>\n    <ion-title>Goals</ion-title>\n  </ion-navbar>\n\n</ion-header>\n\n<ion-content padding>\n  <div class="container">\n    <ion-card>\n      <ion-card-header>\n        <ion-icon class="title" name="person"></ion-icon>Personal Detail\n        <ion-fab top right class="personal">\n          <button ion-fab mini (click)="profile()"><ion-icon name="md-create"></ion-icon></button>\n        </ion-fab>\n      </ion-card-header>\n\n      <ion-card-content>\n        <div class="info">\n          <p class="label">Fullname</p>\n          <p>{{mi?.fullname? mi.fullname : \'Unknown\'}}</p>\n          <p class="label">Address</p>\n          <p>{{mi?.address ? mi.address: \'Unknown\'}}</p>\n          <ion-row>\n            <ion-col no-padding>\n              <p class="label">Gender</p>\n              <p>{{mi?.gender ? mi.gender: \'Unknown\'}}</p>\n            </ion-col>\n            <ion-col no-padding>\n              <p class="label">Date of Birth</p>\n              <p>{{mi?.dob ? mi.dob: \'Unknown\'}}</p>\n            </ion-col>\n          </ion-row>\n        </div>\n      </ion-card-content>\n    </ion-card>\n\n    <ion-card>\n      <ion-card-header>\n        <ion-icon class="title" name="heart"></ion-icon>Health Detail\n        <ion-fab top right>\n          <button ion-fab mini (click)="health()"><ion-icon name="md-create"></ion-icon></button>\n        </ion-fab>\n      </ion-card-header>\n\n      <ion-card-content>\n        <div class="info">\n          <p class="label">Height</p>\n          <p>{{mi?.height? mi.height : \'Unknown\'}}</p>\n          <p class="label">Weight</p>\n          <p>{{mi?.weight ? mi.weight: \'Unknown\'}}</p>\n          <ion-row>\n            <ion-col no-padding>\n              <p class="label">BMI</p>\n              <p>{{mi?.bmi ? mi.bmi: \'Unknown\'}}</p>\n            </ion-col>\n            <ion-col no-padding>\n              <p class="label">Body Fat %</p>\n              <p>{{mi?.bfp ? mi.bfp: \'Unknown\'}}</p>\n            </ion-col>\n          </ion-row>\n          <p class="label">Lean Body Mass</p>\n          <p>{{mi?.lbm ? mi.lbm : \'Unknown\'}}</p>\n        </div>\n      </ion-card-content>\n    </ion-card>\n  </div>\n\n</ion-content>\n'/*ion-inline-end:"D:\Development\Current_Projects\2019_5_16_Ionic\personal-coach-app\src\pages\me\mi-goals\mi-goals.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_2_angularfire2_database__["a" /* AngularFireDatabase */],
            __WEBPACK_IMPORTED_MODULE_3__shared_index__["a" /* AuthProvider */],
            __WEBPACK_IMPORTED_MODULE_3__shared_index__["f" /* IonicProvider */],
            __WEBPACK_IMPORTED_MODULE_3__shared_index__["d" /* ErrorHandlerProvider */]])
    ], MiGoals);
    return MiGoals;
}());

//# sourceMappingURL=mi-goals.js.map

/***/ }),

/***/ 674:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser_dynamic__ = __webpack_require__(675);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__app_module__ = __webpack_require__(679);


Object(__WEBPACK_IMPORTED_MODULE_0__angular_platform_browser_dynamic__["a" /* platformBrowserDynamic */])().bootstrapModule(__WEBPACK_IMPORTED_MODULE_1__app_module__["a" /* AppModule */]);
//# sourceMappingURL=main.js.map

/***/ }),

/***/ 679:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser__ = __webpack_require__(45);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_http__ = __webpack_require__(250);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_forms__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_ionic_angular__ = __webpack_require__(10);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_angularfire2__ = __webpack_require__(60);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6_angularfire2_database__ = __webpack_require__(17);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7_angularfire2_auth__ = __webpack_require__(383);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8_angular2_moment__ = __webpack_require__(783);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8_angular2_moment___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_8_angular2_moment__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__base_url__ = __webpack_require__(523);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__app_component__ = __webpack_require__(786);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__components_index__ = __webpack_require__(29);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12__shared_index__ = __webpack_require__(15);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_13__pages_index__ = __webpack_require__(31);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_14__shared_shared_module__ = __webpack_require__(1214);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_15__shared_providers_storage_provider__ = __webpack_require__(213);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_16__ionic_native_status_bar__ = __webpack_require__(524);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_17__ionic_native_splash_screen__ = __webpack_require__(528);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_18__ionic_native_camera__ = __webpack_require__(138);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_19__ionic_native_keyboard__ = __webpack_require__(220);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_20__ionic_native_geolocation__ = __webpack_require__(94);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_21__ionic_native_call_number__ = __webpack_require__(98);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_22__ionic_native_network__ = __webpack_require__(541);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_23__ionic_native_in_app_browser__ = __webpack_require__(228);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_24__ionic_native_social_sharing__ = __webpack_require__(74);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_25__ionic_native_photo_library__ = __webpack_require__(1215);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};





// Import the AF2 Module





















var AppModule = /** @class */ (function () {
    function AppModule() {
    }
    AppModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["NgModule"])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_10__app_component__["a" /* MyApp */],
                __WEBPACK_IMPORTED_MODULE_11__components_index__["g" /* Search */],
                __WEBPACK_IMPORTED_MODULE_11__components_index__["c" /* Login */],
                __WEBPACK_IMPORTED_MODULE_11__components_index__["h" /* Signup */],
                __WEBPACK_IMPORTED_MODULE_11__components_index__["a" /* Calendar */],
                __WEBPACK_IMPORTED_MODULE_11__components_index__["b" /* Chat */],
                __WEBPACK_IMPORTED_MODULE_11__components_index__["d" /* Map */],
                __WEBPACK_IMPORTED_MODULE_11__components_index__["f" /* Notifications */],
                __WEBPACK_IMPORTED_MODULE_11__components_index__["e" /* NotificationForm */],
                __WEBPACK_IMPORTED_MODULE_13__pages_index__["u" /* Walkthrough */],
                __WEBPACK_IMPORTED_MODULE_13__pages_index__["i" /* Gyms */],
                __WEBPACK_IMPORTED_MODULE_13__pages_index__["t" /* Trainers */],
                __WEBPACK_IMPORTED_MODULE_13__pages_index__["l" /* Me */],
                __WEBPACK_IMPORTED_MODULE_13__pages_index__["j" /* Intro */],
                __WEBPACK_IMPORTED_MODULE_13__pages_index__["a" /* AddBlog */],
                __WEBPACK_IMPORTED_MODULE_13__pages_index__["c" /* Blog */],
                __WEBPACK_IMPORTED_MODULE_13__pages_index__["d" /* BlogComments */],
                __WEBPACK_IMPORTED_MODULE_13__pages_index__["e" /* Blogs */],
                __WEBPACK_IMPORTED_MODULE_13__pages_index__["f" /* FitnessBlog */],
                __WEBPACK_IMPORTED_MODULE_13__pages_index__["g" /* FoodBlog */],
                __WEBPACK_IMPORTED_MODULE_13__pages_index__["k" /* LifestyleBlog */],
                __WEBPACK_IMPORTED_MODULE_13__pages_index__["p" /* MiProfileForm */],
                __WEBPACK_IMPORTED_MODULE_13__pages_index__["o" /* MiProfile */],
                __WEBPACK_IMPORTED_MODULE_13__pages_index__["r" /* MiSchedule */],
                __WEBPACK_IMPORTED_MODULE_13__pages_index__["q" /* MiReads */],
                __WEBPACK_IMPORTED_MODULE_13__pages_index__["m" /* MiGoals */],
                __WEBPACK_IMPORTED_MODULE_13__pages_index__["n" /* MiHealth */],
                __WEBPACK_IMPORTED_MODULE_13__pages_index__["h" /* Gym */],
                __WEBPACK_IMPORTED_MODULE_13__pages_index__["s" /* Trainer */],
                __WEBPACK_IMPORTED_MODULE_13__pages_index__["b" /* Appointment */]
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser__["a" /* BrowserModule */],
                __WEBPACK_IMPORTED_MODULE_2__angular_http__["a" /* HttpModule */],
                __WEBPACK_IMPORTED_MODULE_3__angular_forms__["ReactiveFormsModule"],
                __WEBPACK_IMPORTED_MODULE_8_angular2_moment__["MomentModule"],
                __WEBPACK_IMPORTED_MODULE_14__shared_shared_module__["a" /* SharedModule */],
                __WEBPACK_IMPORTED_MODULE_4_ionic_angular__["g" /* IonicModule */].forRoot(__WEBPACK_IMPORTED_MODULE_10__app_component__["a" /* MyApp */], __WEBPACK_IMPORTED_MODULE_9__base_url__["a" /* APP_CONFIG */], {
                    links: []
                }),
                __WEBPACK_IMPORTED_MODULE_5_angularfire2__["a" /* AngularFireModule */].initializeApp(__WEBPACK_IMPORTED_MODULE_9__base_url__["b" /* FIREBASE_CONFIG */]),
                __WEBPACK_IMPORTED_MODULE_6_angularfire2_database__["b" /* AngularFireDatabaseModule */],
                __WEBPACK_IMPORTED_MODULE_7_angularfire2_auth__["b" /* AngularFireAuthModule */]
            ],
            bootstrap: [__WEBPACK_IMPORTED_MODULE_4_ionic_angular__["e" /* IonicApp */]],
            entryComponents: [
                __WEBPACK_IMPORTED_MODULE_10__app_component__["a" /* MyApp */],
                __WEBPACK_IMPORTED_MODULE_11__components_index__["g" /* Search */],
                __WEBPACK_IMPORTED_MODULE_11__components_index__["c" /* Login */],
                __WEBPACK_IMPORTED_MODULE_11__components_index__["h" /* Signup */],
                __WEBPACK_IMPORTED_MODULE_11__components_index__["a" /* Calendar */],
                __WEBPACK_IMPORTED_MODULE_11__components_index__["b" /* Chat */],
                __WEBPACK_IMPORTED_MODULE_11__components_index__["d" /* Map */],
                __WEBPACK_IMPORTED_MODULE_11__components_index__["f" /* Notifications */],
                __WEBPACK_IMPORTED_MODULE_11__components_index__["e" /* NotificationForm */],
                __WEBPACK_IMPORTED_MODULE_13__pages_index__["u" /* Walkthrough */],
                __WEBPACK_IMPORTED_MODULE_13__pages_index__["i" /* Gyms */],
                __WEBPACK_IMPORTED_MODULE_13__pages_index__["t" /* Trainers */],
                __WEBPACK_IMPORTED_MODULE_13__pages_index__["l" /* Me */],
                __WEBPACK_IMPORTED_MODULE_13__pages_index__["j" /* Intro */],
                __WEBPACK_IMPORTED_MODULE_13__pages_index__["a" /* AddBlog */],
                __WEBPACK_IMPORTED_MODULE_13__pages_index__["c" /* Blog */],
                __WEBPACK_IMPORTED_MODULE_13__pages_index__["d" /* BlogComments */],
                __WEBPACK_IMPORTED_MODULE_13__pages_index__["e" /* Blogs */],
                __WEBPACK_IMPORTED_MODULE_13__pages_index__["f" /* FitnessBlog */],
                __WEBPACK_IMPORTED_MODULE_13__pages_index__["g" /* FoodBlog */],
                __WEBPACK_IMPORTED_MODULE_13__pages_index__["k" /* LifestyleBlog */],
                __WEBPACK_IMPORTED_MODULE_13__pages_index__["o" /* MiProfile */],
                __WEBPACK_IMPORTED_MODULE_13__pages_index__["p" /* MiProfileForm */],
                __WEBPACK_IMPORTED_MODULE_13__pages_index__["r" /* MiSchedule */],
                __WEBPACK_IMPORTED_MODULE_13__pages_index__["q" /* MiReads */],
                __WEBPACK_IMPORTED_MODULE_13__pages_index__["m" /* MiGoals */],
                __WEBPACK_IMPORTED_MODULE_13__pages_index__["n" /* MiHealth */],
                __WEBPACK_IMPORTED_MODULE_13__pages_index__["h" /* Gym */],
                __WEBPACK_IMPORTED_MODULE_13__pages_index__["s" /* Trainer */],
                __WEBPACK_IMPORTED_MODULE_13__pages_index__["b" /* Appointment */]
            ],
            providers: [
                __WEBPACK_IMPORTED_MODULE_16__ionic_native_status_bar__["a" /* StatusBar */],
                __WEBPACK_IMPORTED_MODULE_17__ionic_native_splash_screen__["a" /* SplashScreen */],
                __WEBPACK_IMPORTED_MODULE_18__ionic_native_camera__["a" /* Camera */],
                __WEBPACK_IMPORTED_MODULE_19__ionic_native_keyboard__["a" /* Keyboard */],
                __WEBPACK_IMPORTED_MODULE_20__ionic_native_geolocation__["a" /* Geolocation */],
                __WEBPACK_IMPORTED_MODULE_12__shared_index__["b" /* Connectivity */],
                __WEBPACK_IMPORTED_MODULE_21__ionic_native_call_number__["a" /* CallNumber */],
                __WEBPACK_IMPORTED_MODULE_22__ionic_native_network__["a" /* Network */],
                __WEBPACK_IMPORTED_MODULE_23__ionic_native_in_app_browser__["a" /* InAppBrowser */],
                __WEBPACK_IMPORTED_MODULE_24__ionic_native_social_sharing__["a" /* SocialSharing */],
                __WEBPACK_IMPORTED_MODULE_25__ionic_native_photo_library__["a" /* PhotoLibrary */],
                { provide: __WEBPACK_IMPORTED_MODULE_1__angular_core__["ErrorHandler"], useClass: __WEBPACK_IMPORTED_MODULE_4_ionic_angular__["f" /* IonicErrorHandler */] },
                __WEBPACK_IMPORTED_MODULE_12__shared_index__["a" /* AuthProvider */],
                __WEBPACK_IMPORTED_MODULE_12__shared_index__["e" /* GoogleMaps */],
                __WEBPACK_IMPORTED_MODULE_15__shared_providers_storage_provider__["a" /* StorageProvider */],
                __WEBPACK_IMPORTED_MODULE_12__shared_index__["f" /* IonicProvider */],
                __WEBPACK_IMPORTED_MODULE_12__shared_index__["d" /* ErrorHandlerProvider */]
            ]
        })
    ], AppModule);
    return AppModule;
}());

//# sourceMappingURL=app.module.js.map

/***/ }),

/***/ 784:
/***/ (function(module, exports, __webpack_require__) {

var map = {
	"./af": 389,
	"./af.js": 389,
	"./ar": 390,
	"./ar-dz": 391,
	"./ar-dz.js": 391,
	"./ar-kw": 392,
	"./ar-kw.js": 392,
	"./ar-ly": 393,
	"./ar-ly.js": 393,
	"./ar-ma": 394,
	"./ar-ma.js": 394,
	"./ar-sa": 395,
	"./ar-sa.js": 395,
	"./ar-tn": 396,
	"./ar-tn.js": 396,
	"./ar.js": 390,
	"./az": 397,
	"./az.js": 397,
	"./be": 398,
	"./be.js": 398,
	"./bg": 399,
	"./bg.js": 399,
	"./bm": 400,
	"./bm.js": 400,
	"./bn": 401,
	"./bn.js": 401,
	"./bo": 402,
	"./bo.js": 402,
	"./br": 403,
	"./br.js": 403,
	"./bs": 404,
	"./bs.js": 404,
	"./ca": 405,
	"./ca.js": 405,
	"./cs": 406,
	"./cs.js": 406,
	"./cv": 407,
	"./cv.js": 407,
	"./cy": 408,
	"./cy.js": 408,
	"./da": 409,
	"./da.js": 409,
	"./de": 410,
	"./de-at": 411,
	"./de-at.js": 411,
	"./de-ch": 412,
	"./de-ch.js": 412,
	"./de.js": 410,
	"./dv": 413,
	"./dv.js": 413,
	"./el": 414,
	"./el.js": 414,
	"./en-au": 415,
	"./en-au.js": 415,
	"./en-ca": 416,
	"./en-ca.js": 416,
	"./en-gb": 417,
	"./en-gb.js": 417,
	"./en-ie": 418,
	"./en-ie.js": 418,
	"./en-il": 419,
	"./en-il.js": 419,
	"./en-nz": 420,
	"./en-nz.js": 420,
	"./eo": 421,
	"./eo.js": 421,
	"./es": 422,
	"./es-do": 423,
	"./es-do.js": 423,
	"./es-us": 424,
	"./es-us.js": 424,
	"./es.js": 422,
	"./et": 425,
	"./et.js": 425,
	"./eu": 426,
	"./eu.js": 426,
	"./fa": 427,
	"./fa.js": 427,
	"./fi": 428,
	"./fi.js": 428,
	"./fo": 429,
	"./fo.js": 429,
	"./fr": 430,
	"./fr-ca": 431,
	"./fr-ca.js": 431,
	"./fr-ch": 432,
	"./fr-ch.js": 432,
	"./fr.js": 430,
	"./fy": 433,
	"./fy.js": 433,
	"./gd": 434,
	"./gd.js": 434,
	"./gl": 435,
	"./gl.js": 435,
	"./gom-latn": 436,
	"./gom-latn.js": 436,
	"./gu": 437,
	"./gu.js": 437,
	"./he": 438,
	"./he.js": 438,
	"./hi": 439,
	"./hi.js": 439,
	"./hr": 440,
	"./hr.js": 440,
	"./hu": 441,
	"./hu.js": 441,
	"./hy-am": 442,
	"./hy-am.js": 442,
	"./id": 443,
	"./id.js": 443,
	"./is": 444,
	"./is.js": 444,
	"./it": 445,
	"./it.js": 445,
	"./ja": 446,
	"./ja.js": 446,
	"./jv": 447,
	"./jv.js": 447,
	"./ka": 448,
	"./ka.js": 448,
	"./kk": 449,
	"./kk.js": 449,
	"./km": 450,
	"./km.js": 450,
	"./kn": 451,
	"./kn.js": 451,
	"./ko": 452,
	"./ko.js": 452,
	"./ky": 453,
	"./ky.js": 453,
	"./lb": 454,
	"./lb.js": 454,
	"./lo": 455,
	"./lo.js": 455,
	"./lt": 456,
	"./lt.js": 456,
	"./lv": 457,
	"./lv.js": 457,
	"./me": 458,
	"./me.js": 458,
	"./mi": 459,
	"./mi.js": 459,
	"./mk": 460,
	"./mk.js": 460,
	"./ml": 461,
	"./ml.js": 461,
	"./mr": 462,
	"./mr.js": 462,
	"./ms": 463,
	"./ms-my": 464,
	"./ms-my.js": 464,
	"./ms.js": 463,
	"./mt": 465,
	"./mt.js": 465,
	"./my": 466,
	"./my.js": 466,
	"./nb": 467,
	"./nb.js": 467,
	"./ne": 468,
	"./ne.js": 468,
	"./nl": 469,
	"./nl-be": 470,
	"./nl-be.js": 470,
	"./nl.js": 469,
	"./nn": 471,
	"./nn.js": 471,
	"./pa-in": 472,
	"./pa-in.js": 472,
	"./pl": 473,
	"./pl.js": 473,
	"./pt": 474,
	"./pt-br": 475,
	"./pt-br.js": 475,
	"./pt.js": 474,
	"./ro": 476,
	"./ro.js": 476,
	"./ru": 477,
	"./ru.js": 477,
	"./sd": 478,
	"./sd.js": 478,
	"./se": 479,
	"./se.js": 479,
	"./si": 480,
	"./si.js": 480,
	"./sk": 481,
	"./sk.js": 481,
	"./sl": 482,
	"./sl.js": 482,
	"./sq": 483,
	"./sq.js": 483,
	"./sr": 484,
	"./sr-cyrl": 485,
	"./sr-cyrl.js": 485,
	"./sr.js": 484,
	"./ss": 486,
	"./ss.js": 486,
	"./sv": 487,
	"./sv.js": 487,
	"./sw": 488,
	"./sw.js": 488,
	"./ta": 489,
	"./ta.js": 489,
	"./te": 490,
	"./te.js": 490,
	"./tet": 491,
	"./tet.js": 491,
	"./tg": 492,
	"./tg.js": 492,
	"./th": 493,
	"./th.js": 493,
	"./tl-ph": 494,
	"./tl-ph.js": 494,
	"./tlh": 495,
	"./tlh.js": 495,
	"./tr": 496,
	"./tr.js": 496,
	"./tzl": 497,
	"./tzl.js": 497,
	"./tzm": 498,
	"./tzm-latn": 499,
	"./tzm-latn.js": 499,
	"./tzm.js": 498,
	"./ug-cn": 500,
	"./ug-cn.js": 500,
	"./uk": 501,
	"./uk.js": 501,
	"./ur": 502,
	"./ur.js": 502,
	"./uz": 503,
	"./uz-latn": 504,
	"./uz-latn.js": 504,
	"./uz.js": 503,
	"./vi": 505,
	"./vi.js": 505,
	"./x-pseudo": 506,
	"./x-pseudo.js": 506,
	"./yo": 507,
	"./yo.js": 507,
	"./zh-cn": 508,
	"./zh-cn.js": 508,
	"./zh-hk": 509,
	"./zh-hk.js": 509,
	"./zh-tw": 510,
	"./zh-tw.js": 510
};
function webpackContext(req) {
	return __webpack_require__(webpackContextResolve(req));
};
function webpackContextResolve(req) {
	var id = map[req];
	if(!(id + 1)) // check for number or string
		throw new Error("Cannot find module '" + req + "'.");
	return id;
};
webpackContext.keys = function webpackContextKeys() {
	return Object.keys(map);
};
webpackContext.resolve = webpackContextResolve;
module.exports = webpackContext;
webpackContext.id = 784;

/***/ }),

/***/ 786:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MyApp; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(10);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_native_status_bar__ = __webpack_require__(524);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_native_splash_screen__ = __webpack_require__(528);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__shared_index__ = __webpack_require__(15);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__pages_index__ = __webpack_require__(31);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__components_index__ = __webpack_require__(29);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







var MyApp = /** @class */ (function () {
    function MyApp(platform, statusBar, splashScreen, auth$, events) {
        this.platform = platform;
        this.statusBar = statusBar;
        this.splashScreen = splashScreen;
        this.auth$ = auth$;
        this.events = events;
        this.rootPage = __WEBPACK_IMPORTED_MODULE_5__pages_index__["u" /* Walkthrough */];
    }
    MyApp.prototype.ngOnInit = function () {
        var _this = this;
        this.initializeApp();
        this.events.subscribe('loggedIn', function (user) {
            _this.mi = user;
        });
        // used for an example of ngFor and navigation
        this.pages = [
            { title: 'Intro', component: __WEBPACK_IMPORTED_MODULE_5__pages_index__["j" /* Intro */] },
            { title: 'Blogs', component: __WEBPACK_IMPORTED_MODULE_5__pages_index__["e" /* Blogs */] },
            { title: 'Schedule', component: __WEBPACK_IMPORTED_MODULE_5__pages_index__["r" /* MiSchedule */] },
            { title: 'Notifications', component: __WEBPACK_IMPORTED_MODULE_6__components_index__["f" /* Notifications */] },
            { title: 'Trainers', component: __WEBPACK_IMPORTED_MODULE_5__pages_index__["t" /* Trainers */] },
            { title: 'Gyms', component: __WEBPACK_IMPORTED_MODULE_5__pages_index__["i" /* Gyms */] },
            { title: 'Logout', component: __WEBPACK_IMPORTED_MODULE_5__pages_index__["u" /* Walkthrough */] },
        ];
    };
    MyApp.prototype.initializeApp = function () {
        var _this = this;
        this.platform.ready().then(function () {
            if (_this.platform.is('cordova')) {
                _this.statusBar.styleDefault();
                _this.splashScreen.hide();
            }
        });
    };
    MyApp.prototype.openMe = function () {
        this.nav.setRoot(__WEBPACK_IMPORTED_MODULE_5__pages_index__["l" /* Me */]);
    };
    MyApp.prototype.openPage = function (page) {
        // Reset the content nav to have just this page
        // we wouldn't want the back button to show in this scenario
        if (page.title === 'Logout') {
            this.auth$.logout();
        }
        this.nav.setRoot(page.component);
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])(__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* Nav */]),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* Nav */])
    ], MyApp.prototype, "nav", void 0);
    MyApp = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({template:/*ion-inline-start:"D:\Development\Current_Projects\2019_5_16_Ionic\personal-coach-app\src\app\app.html"*/'<ion-menu [content]="content">\n  <ion-content>\n    <ion-item (click)="openMe()" menuClose>\n      <ion-avatar>\n        <img [src]="mi?.picture | image: \'url:profile\'">\n      </ion-avatar>\n      <h2>{{mi?.fullname}}</h2>\n      <p class="small">{{mi?.country}}</p>\n    </ion-item>\n    <ion-item menuClose *ngFor="let p of pages" (click)="openPage(p)">\n      {{p.title}}\n    </ion-item>\n  </ion-content>\n\n</ion-menu>\n\n<!-- Disable swipe-to-go-back because it\'s poor UX to combine STGB with side menus -->\n<ion-nav [root]="rootPage" #content swipeBackEnabled="false"></ion-nav>\n'/*ion-inline-end:"D:\Development\Current_Projects\2019_5_16_Ionic\personal-coach-app\src\app\app.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* Platform */],
            __WEBPACK_IMPORTED_MODULE_2__ionic_native_status_bar__["a" /* StatusBar */],
            __WEBPACK_IMPORTED_MODULE_3__ionic_native_splash_screen__["a" /* SplashScreen */],
            __WEBPACK_IMPORTED_MODULE_4__shared_index__["a" /* AuthProvider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["d" /* Events */]])
    ], MyApp);
    return MyApp;
}());

//# sourceMappingURL=app.component.js.map

/***/ }),

/***/ 793:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AuthProvider; });
/* unused harmony export User */
/* unused harmony export UserProvider */
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_angularfire2_auth__ = __webpack_require__(383);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_angularfire2_database__ = __webpack_require__(17);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_firebase_app__ = __webpack_require__(529);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_firebase_app___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_firebase_app__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__storage_provider__ = __webpack_require__(213);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var AuthProvider = /** @class */ (function () {
    function AuthProvider(fireauth, storage$, data$) {
        this.fireauth = fireauth;
        this.storage$ = storage$;
        this.data$ = data$;
    }
    AuthProvider.prototype.isAuthenticated = function () {
        return this.fireauth.authState;
    };
    AuthProvider.prototype.signUp = function (_a) {
        var email = _a.email, password = _a.password;
        return this.fireauth.auth
            .createUserWithEmailAndPassword(email, password)
            .then(function (res) { return res; })
            .catch(function (error) { return error; });
    };
    AuthProvider.prototype.logIn = function (_a) {
        var email = _a.email, password = _a.password;
        return this.fireauth.auth
            .signInWithEmailAndPassword(email, password)
            .then(function (res) { return res; })
            .catch(function (error) { return error; });
    };
    AuthProvider.prototype.logInWithFacebook = function () {
        return this.fireauth.auth
            .signInWithPopup(new __WEBPACK_IMPORTED_MODULE_3_firebase_app__["auth"].FacebookAuthProvider())
            .then(function (res) { return res; })
            .catch(function (error) { return error; });
    };
    AuthProvider.prototype.logInWithGoogle = function () {
        return this.fireauth.auth
            .signInWithPopup(new __WEBPACK_IMPORTED_MODULE_3_firebase_app__["auth"].GoogleAuthProvider())
            .then(function (res) { return res; })
            .catch(function (error) { return error; });
    };
    AuthProvider.prototype.getUser = function () {
        var _this = this;
        return this.data$.object('/users/' + this.fireauth.auth.currentUser.uid).valueChanges().map(function (user) {
            _this.currentUser = user;
            return user;
        });
    };
    /* Check if image has been changed,
     save image and user profile,
     return user profile
    */
    AuthProvider.prototype.updateUser = function (user) {
        var _this = this;
        if (user.photoURL) {
            return this.storage$
                .uploadFile(user.uid + '-image', user.photoURL)
                .then(function (file) {
                user.photoURL = file;
                return _this.fireauth.auth.currentUser.updateProfile(user).then(function () { return user; }, function (error) { return error; });
            });
        }
        else {
            return this.fireauth.auth.currentUser.updateProfile(user).then(function () { return user; }, function (error) { return error; });
        }
    };
    AuthProvider.prototype.sendPasswordReset = function (email) {
        this.fireauth.auth
            .sendPasswordResetEmail(email)
            .then(function () {
            // Email sent.
        }, function (error) {
            // An error happened.
        });
    };
    AuthProvider.prototype.logout = function () {
        return this.fireauth.auth.signOut();
    };
    AuthProvider = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_angularfire2_auth__["a" /* AngularFireAuth */],
            __WEBPACK_IMPORTED_MODULE_4__storage_provider__["a" /* StorageProvider */],
            __WEBPACK_IMPORTED_MODULE_2_angularfire2_database__["a" /* AngularFireDatabase */]])
    ], AuthProvider);
    return AuthProvider;
}());

var User = /** @class */ (function () {
    function User() {
    }
    return User;
}());

var UserProvider = /** @class */ (function () {
    function UserProvider() {
    }
    return UserProvider;
}());

//# sourceMappingURL=auth.provider.js.map

/***/ }),

/***/ 829:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return GoogleMaps; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__ionic_provider__ = __webpack_require__(540);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_native_geolocation__ = __webpack_require__(94);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__app_base_url__ = __webpack_require__(523);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




/*
Find tutorial here
 https://www.joshmorony.com/location-select-page-with-google-maps-and-ionic/
*/
var GoogleMaps = /** @class */ (function () {
    function GoogleMaps(connectivityService, geolocation) {
        this.connectivityService = connectivityService;
        this.geolocation = geolocation;
        this.mapInitialised = false;
        this.apiKey = __WEBPACK_IMPORTED_MODULE_3__app_base_url__["c" /* GOOGLE_MAP_API_KEY */];
    }
    GoogleMaps.prototype.init = function (mapElement, pleaseConnect, coords, location_details) {
        this.mapElement = mapElement;
        this.pleaseConnect = pleaseConnect;
        this.location_details = location_details;
        return this.loadGoogleMaps(coords);
    };
    GoogleMaps.prototype.loadGoogleMaps = function (coords) {
        var _this = this;
        return new Promise(function (resolve) {
            if (typeof google == "undefined" || typeof google.maps == "undefined") {
                console.log("Google maps JavaScript needs to be loaded.");
                _this.disableMap();
                if (_this.connectivityService.isOnline()) {
                    window['mapInit'] = function () {
                        _this.initMap(coords).then(function () {
                            resolve(true);
                        });
                        _this.enableMap();
                    };
                    var script = document.createElement("script");
                    script.id = "googleMaps";
                    if (_this.apiKey) {
                        script.src = 'http://maps.google.com/maps/api/js?key=' + _this.apiKey + '&callback=mapInit&libraries=places';
                    }
                    else {
                        script.src = 'http://maps.google.com/maps/api/js?callback=mapInit';
                    }
                    document.body.appendChild(script);
                }
            }
            else {
                if (_this.connectivityService.isOnline()) {
                    _this.initMap(coords);
                    _this.enableMap();
                }
                else {
                    _this.disableMap();
                }
                resolve(true);
            }
            _this.addConnectivityListeners();
        });
    };
    GoogleMaps.prototype.initMap = function (coords) {
        var _this = this;
        this.mapInitialised = true;
        return new Promise(function (resolve) {
            var latLng;
            _this.geolocation.getCurrentPosition().then(function (position) {
                if (coords) {
                    latLng = new google.maps.LatLng(coords[0], coords[1]);
                }
                else {
                    latLng = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);
                }
                var mapOptions = {
                    center: latLng,
                    zoom: 15,
                    mapTypeId: google.maps.MapTypeId.ROADMAP
                };
                _this.map = new google.maps.Map(_this.mapElement, mapOptions);
                _this.addMarker();
                resolve(true);
            });
        });
    };
    GoogleMaps.prototype.addMarker = function () {
        var marker = new google.maps.Marker({
            map: this.map,
            animation: google.maps.Animation.DROP,
            position: this.map.getCenter()
        });
        var content =  true ? this.location_details.name : 'Current Location' + '</h5>';
        this.addInfoWindow(marker, content);
    };
    GoogleMaps.prototype.addInfoWindow = function (marker, content) {
        var _this = this;
        var infoWindow = new google.maps.InfoWindow({
            content: content
        });
        google.maps.event.addListener(marker, 'click', function () {
            infoWindow.open(_this.map, marker);
        });
    };
    GoogleMaps.prototype.disableMap = function () {
        if (this.pleaseConnect) {
            this.pleaseConnect.style.display = "block";
        }
    };
    GoogleMaps.prototype.enableMap = function () {
        if (this.pleaseConnect) {
            this.pleaseConnect.style.display = "none";
        }
    };
    GoogleMaps.prototype.addConnectivityListeners = function () {
        var _this = this;
        this.connectivityService.watchOnline().subscribe(function () {
            setTimeout(function () {
                if (typeof google == "undefined" || typeof google.maps == "undefined") {
                    _this.loadGoogleMaps();
                }
                else {
                    if (!_this.mapInitialised) {
                        _this.initMap();
                    }
                    _this.enableMap();
                }
            }, 2000);
        });
        this.connectivityService.watchOffline().subscribe(function () {
            _this.disableMap();
        });
    };
    GoogleMaps = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__ionic_provider__["a" /* Connectivity */],
            __WEBPACK_IMPORTED_MODULE_2__ionic_native_geolocation__["a" /* Geolocation */]])
    ], GoogleMaps);
    return GoogleMaps;
}());

//# sourceMappingURL=places.provider.js.map

/***/ }),

/***/ 830:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ErrorHandlerProvider; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_http__ = __webpack_require__(250);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_angular__ = __webpack_require__(10);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var ErrorHandlerProvider = /** @class */ (function () {
    function ErrorHandlerProvider(toastCtrl) {
        this.toastCtrl = toastCtrl;
    }
    ErrorHandlerProvider.prototype.handleError = function (err) {
        if (typeof err === 'string') {
            this.presentToast(err);
        }
        else if (err instanceof __WEBPACK_IMPORTED_MODULE_1__angular_http__["b" /* Response */]) {
            var res = err;
            if (res.text() && res.text() !== res.statusText) {
                this.presentToast(res.text() + '\n' + res.statusText);
            }
            else {
                this.presentToast(res.statusText);
            }
        }
        else if (err && err.message) {
            this.presentToast(err.message);
        }
        else if (err) {
            this.presentToast(err.toString());
        }
        else {
            this.presentToast('An unknown error has occurred');
        }
    };
    ErrorHandlerProvider.prototype.presentToast = function (message, duration) {
        var durationTime = 0;
        if (duration === 'long') {
            durationTime = 5000;
        }
        else if (duration === 'short') {
            durationTime = 2500;
        }
        else {
            durationTime = 3500;
        }
        var toast = this.toastCtrl.create({
            message: message,
            duration: durationTime
        });
        toast.present();
    };
    ErrorHandlerProvider = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_2_ionic_angular__["n" /* ToastController */]])
    ], ErrorHandlerProvider);
    return ErrorHandlerProvider;
}());

//# sourceMappingURL=error-handler.provider.js.map

/***/ }),

/***/ 831:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return Loading; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var Loading = /** @class */ (function () {
    function Loading() {
    }
    Loading = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'loading',template:/*ion-inline-start:"D:\Development\Current_Projects\2019_5_16_Ionic\personal-coach-app\src\shared\widgets\loading\loading.html"*/'<div class="loading">\n    <svg class="spinner" width="65px" height="65px" viewBox="0 0 66 66" xmlns="http://www.w3.org/2000/svg">\n      <circle class="path" fill="none" stroke-width="6" stroke-linecap="round" cx="33" cy="33" r="30"></circle>\n    </svg>\n  </div>'/*ion-inline-end:"D:\Development\Current_Projects\2019_5_16_Ionic\personal-coach-app\src\shared\widgets\loading\loading.html"*/,
        }),
        __metadata("design:paramtypes", [])
    ], Loading);
    return Loading;
}());

//# sourceMappingURL=loading.js.map

/***/ }),

/***/ 832:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return NoData; });
/* unused harmony export NoDataOptions */
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var NoData = /** @class */ (function () {
    function NoData() {
        this.event = new __WEBPACK_IMPORTED_MODULE_0__angular_core__["EventEmitter"]();
        var defaults = {
            image: 'box',
            text: 'No Data to display!'
        };
        this.options = Object.assign(defaults, this.options);
    }
    NoData.prototype.onClick = function () {
        this.event.next();
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])(),
        __metadata("design:type", NoDataOptions)
    ], NoData.prototype, "options", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])(),
        __metadata("design:type", Object)
    ], NoData.prototype, "button", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Output"])(),
        __metadata("design:type", Object)
    ], NoData.prototype, "event", void 0);
    NoData = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'no-data',template:/*ion-inline-start:"D:\Development\Current_Projects\2019_5_16_Ionic\personal-coach-app\src\shared\widgets\no-data\no-data.html"*/'<div class="no-data">\n  <div [ngSwitch]="options.image">\n    <div *ngSwitchCase="\'cart\'">\n      <img src="./assets/empty-cart.png" alt="">\n    </div>\n    <div *ngSwitchCase="\'box\'">\n      <img src="./assets/empty-box.png" alt="">\n    </div>\n    <div *ngSwitchDefault>\n      <img src="./assets/empty-box.png" alt="">\n    </div>\n  </div>\n\n  <p>{{options?.text}}</p>\n  <ion-row *ngIf="button">\n    <ion-col text-center>\n      <button ion-button [color]="button.color" (click)="onClick()">{{button.text}}</button>\n    </ion-col>\n  </ion-row>\n  \n\n</div>\n'/*ion-inline-end:"D:\Development\Current_Projects\2019_5_16_Ionic\personal-coach-app\src\shared\widgets\no-data\no-data.html"*/,
        }),
        __metadata("design:paramtypes", [])
    ], NoData);
    return NoData;
}());

var NoDataOptions = /** @class */ (function () {
    function NoDataOptions() {
    }
    return NoDataOptions;
}());

//# sourceMappingURL=no-data.js.map

/***/ }),

/***/ 833:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return Rating; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var Rating = /** @class */ (function () {
    function Rating() {
        this.color = 'favorite';
        this.align = 'center';
        this.updateRate = new __WEBPACK_IMPORTED_MODULE_0__angular_core__["EventEmitter"]();
        this.range = [1, 2, 3, 4, 5];
    }
    Rating.prototype.update = function (value) {
        this.rate = value;
        this.updateRate.emit(value);
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])(),
        __metadata("design:type", Number)
    ], Rating.prototype, "rate", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])(),
        __metadata("design:type", String)
    ], Rating.prototype, "color", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])(),
        __metadata("design:type", String)
    ], Rating.prototype, "align", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Output"])(),
        __metadata("design:type", Object)
    ], Rating.prototype, "updateRate", void 0);
    Rating = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'rating',template:/*ion-inline-start:"D:\Development\Current_Projects\2019_5_16_Ionic\personal-coach-app\src\shared\widgets\rating\rating.html"*/'   <span tabindex="0" style="display: flex;" [style.justify-content]="align">\n      <div *ngFor="let item of range; let i = index">\n        <ion-icon name="star" [color]="color" *ngIf="i < rate" (click)="update(i + 1)"></ion-icon>\n        <ion-icon name="star-outline" *ngIf="i >= rate || rate===undefined" (click)="update(i + 1)"></ion-icon>\n      </div>\n    </span>'/*ion-inline-end:"D:\Development\Current_Projects\2019_5_16_Ionic\personal-coach-app\src\shared\widgets\rating\rating.html"*/,
        }),
        __metadata("design:paramtypes", [])
    ], Rating);
    return Rating;
}());

//# sourceMappingURL=rating.js.map

/***/ }),

/***/ 834:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return KeyboardAttachDirective; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(10);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_native_keyboard__ = __webpack_require__(220);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



/**
 * @name KeyboardAttachDirective
 * @description
 * The `keyboardAttach` directive will cause an element to float above the
 * keyboard when the keyboard shows. Currently only supports the `ion-footer` element.
 *
 * https://gist.github.com/Manduro/bc121fd39f21558df2a952b39e907754#file-keyboard-attach-directive-ts-L20
 *
 * ### Notes
 * - This directive requires [Ionic Native](https://github.com/driftyco/ionic-native)
 * and the [Ionic Keyboard Plugin](https://github.com/driftyco/ionic-plugin-keyboard).
 * - Currently only tested to work on iOS.
 * - If there is an input in your footer, you will need to set
 *   `Keyboard.disableScroll(true)`.
 *
 * @usage
 *
 * ```html
 * <ion-content #content>
 * </ion-content>
 *
 * <ion-footer [keyboard-attach]="content">
 *   <ion-toolbar>
 *     <ion-item>
 *       <ion-input></ion-input>
 *     </ion-item>
 *   </ion-toolbar>
 * </ion-footer>
 * ```
 */
var KeyboardAttachDirective = /** @class */ (function () {
    function KeyboardAttachDirective(elementRef, platform, keyboard) {
        var _this = this;
        this.elementRef = elementRef;
        this.platform = platform;
        this.keyboard = keyboard;
        if (this.platform.is('cordova') && this.platform.is('ios')) {
            keyboard.disableScroll(true);
            this.onShowSubscription = keyboard.onKeyboardShow().subscribe(function (e) { return _this.onShow(e); });
            this.onHideSubscription = keyboard.onKeyboardHide().subscribe(function () { return _this.onHide(); });
        }
    }
    KeyboardAttachDirective.prototype.ngOnDestroy = function () {
        if (this.onShowSubscription) {
            this.onShowSubscription.unsubscribe();
        }
        if (this.onHideSubscription) {
            this.onHideSubscription.unsubscribe();
        }
    };
    KeyboardAttachDirective.prototype.onShow = function (e) {
        var keyboardHeight = e.keyboardHeight || (e.detail && e.detail.keyboardHeight);
        this.setElementPosition(keyboardHeight);
    };
    ;
    KeyboardAttachDirective.prototype.onHide = function () {
        this.setElementPosition(0);
    };
    ;
    KeyboardAttachDirective.prototype.setElementPosition = function (pixels) {
        this.elementRef.nativeElement.style.paddingBottom = pixels + 'px';
        this.content.resize();
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])('keyboard-attach'),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["c" /* Content */])
    ], KeyboardAttachDirective.prototype, "content", void 0);
    KeyboardAttachDirective = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Directive"])({
            selector: '[keyboard-attach]',
            providers: [__WEBPACK_IMPORTED_MODULE_2__ionic_native_keyboard__["a" /* Keyboard */]]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_0__angular_core__["ElementRef"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* Platform */],
            __WEBPACK_IMPORTED_MODULE_2__ionic_native_keyboard__["a" /* Keyboard */]])
    ], KeyboardAttachDirective);
    return KeyboardAttachDirective;
}());

//# sourceMappingURL=keyboard-attach.directive.js.map

/***/ }),

/***/ 835:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ElasticTextareaDirective; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var ElasticTextareaDirective = /** @class */ (function () {
    function ElasticTextareaDirective() {
    }
    ElasticTextareaDirective.prototype.onInput = function (nativeElement) {
        nativeElement.style.overflow = 'hidden';
        nativeElement.style.height = 'auto';
        nativeElement.style.height = nativeElement.scrollHeight + "px";
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["HostListener"])('input', ['$event.target']),
        __metadata("design:type", Function),
        __metadata("design:paramtypes", [Object]),
        __metadata("design:returntype", void 0)
    ], ElasticTextareaDirective.prototype, "onInput", null);
    ElasticTextareaDirective = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Directive"])({
            selector: '[elastic-textarea]'
        })
    ], ElasticTextareaDirective);
    return ElasticTextareaDirective;
}());

//# sourceMappingURL=elastic.directive.js.map

/***/ }),

/***/ 836:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SanitizePipe; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_platform_browser__ = __webpack_require__(45);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var SanitizePipe = /** @class */ (function () {
    function SanitizePipe(sanitized) {
        this.sanitized = sanitized;
    }
    SanitizePipe.prototype.transform = function (value, args) {
        if (value) {
            switch (args.toLowerCase()) {
                case 'url':
                    return this.sanitized.bypassSecurityTrustUrl(value);
                case 'resource':
                    return this.sanitized.bypassSecurityTrustResourceUrl(value);
                case 'script':
                    return this.sanitized.bypassSecurityTrustScript(value);
                case 'style':
                    return this.sanitized.bypassSecurityTrustStyle(value);
                case 'html':
                    return this.sanitized.bypassSecurityTrustHtml(value);
                default:
                    throw new Error("Unable to bypass security for invalid type: " + args.toLowerCase());
            }
        }
        else {
            return 'Nothing to Display';
        }
    };
    SanitizePipe = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Pipe"])({ name: 'sanitize' }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__angular_platform_browser__["c" /* DomSanitizer */]])
    ], SanitizePipe);
    return SanitizePipe;
}());

;
//# sourceMappingURL=sanitize.pipe.js.map

/***/ }),

/***/ 837:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SanitizeImagePipe; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_platform_browser__ = __webpack_require__(45);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var SanitizeImagePipe = /** @class */ (function () {
    function SanitizeImagePipe(sanitized) {
        this.sanitized = sanitized;
    }
    SanitizeImagePipe.prototype.transform = function (value, args) {
        var pipeArgs = args.split(':');
        for (var i = 0; i < pipeArgs.length; i++) {
            pipeArgs[i] = pipeArgs[i].trim(' ');
        }
        if (value) {
            switch (pipeArgs[0].toLowerCase()) {
                case 'url':
                    return this.sanitized.bypassSecurityTrustUrl(value);
                case 'resource':
                    return this.sanitized.bypassSecurityTrustResourceUrl(value);
                case 'script':
                    return this.sanitized.bypassSecurityTrustScript(value);
                case 'style':
                    if (value === 'url(undefined)' || value === 'url(null)' || value === 'url((unknown))') {
                        switch (pipeArgs[1].toLowerCase()) {
                            case 'profile':
                                return 'url(/assets/imgs/Placeholder.png)';
                            case 'placeholder':
                                return 'url(/assets/imgs/Placeholder.png)';
                        }
                    }
                    else {
                        return this.sanitized.bypassSecurityTrustStyle(value);
                    }
                case 'html':
                    return this.sanitized.bypassSecurityTrustHtml(value);
                default:
                    throw new Error("Unable to bypass security for invalid type: " + pipeArgs[0].toLowerCase());
            }
        }
        else {
            switch (pipeArgs[1].toLowerCase()) {
                case 'profile':
                    return 'http://www.ionicity.co.uk/wp-content/uploads/2017/01/placeholder.png';
                case 'placeholder':
                    return 'http://www.ionicity.co.uk/wp-content/uploads/2016/12/placeholder.png';
                default:
                    return 'Nothing to Display';
            }
        }
    };
    SanitizeImagePipe = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Pipe"])({ name: 'image' }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__angular_platform_browser__["c" /* DomSanitizer */]])
    ], SanitizeImagePipe);
    return SanitizeImagePipe;
}());

;
//# sourceMappingURL=sanitize-image.pipe.js.map

/***/ }),

/***/ 838:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return OrderByPipe; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};

var OrderByPipe = /** @class */ (function () {
    function OrderByPipe() {
        this.value = [];
    }
    OrderByPipe_1 = OrderByPipe;
    OrderByPipe._orderByComparator = function (a, b) {
        if (a === null || typeof a === 'undefined') {
            a = 0;
        }
        ;
        if (b === null || typeof b === 'undefined') {
            b = 0;
        }
        ;
        if ((isNaN(parseFloat(a)) || !isFinite(a)) || (isNaN(parseFloat(b)) || !isFinite(b))) {
            if (a.toLowerCase() < b.toLowerCase()) {
                return -1;
            }
            ;
            if (a.toLowerCase() > b.toLowerCase()) {
                return 1;
            }
            ;
        }
        else {
            // Parse strings as numbers to compare properly
            if (parseFloat(a) < parseFloat(b)) {
                return -1;
            }
            ;
            if (parseFloat(a) > parseFloat(b)) {
                return 1;
            }
            ;
        }
        return 0; // equal each other
    };
    OrderByPipe.prototype.transform = function (input, config) {
        if (config === void 0) { config = '+'; }
        // make a copy of the input's reference
        if (input) {
            this.value = input.slice();
            var value = this.value;
            if (!Array.isArray(value)) {
                return value;
            }
            if (!Array.isArray(config) || (Array.isArray(config) && config.length === 1)) {
                var propertyToCheck = !Array.isArray(config) ? config : config[0];
                var desc_1 = propertyToCheck.substr(0, 1) === '-';
                // Basic array
                if (!propertyToCheck || propertyToCheck === '-' || propertyToCheck === '+') {
                    return !desc_1 ? value.sort() : value.sort().reverse();
                }
                else {
                    var property_1 = propertyToCheck.substr(0, 1) === '+' || propertyToCheck.substr(0, 1) === '-'
                        ? propertyToCheck.substr(1)
                        : propertyToCheck;
                    return value.sort(function (a, b) {
                        return !desc_1
                            ? OrderByPipe_1._orderByComparator(a[property_1], b[property_1])
                            : -OrderByPipe_1._orderByComparator(a[property_1], b[property_1]);
                    });
                }
            }
            else {
                // Loop over property of the array in order and sort
                return value.sort(function (a, b) {
                    for (var i = 0; i < config.length; i++) {
                        var desc = config[i].substr(0, 1) === '-';
                        var property = config[i].substr(0, 1) === '+' || config[i].substr(0, 1) === '-'
                            ? config[i].substr(1)
                            : config[i];
                        var comparison = !desc
                            ? OrderByPipe_1._orderByComparator(a[property], b[property])
                            : -OrderByPipe_1._orderByComparator(a[property], b[property]);
                        // Don't return 0 yet in case of needing to sort by next property
                        if (comparison !== 0) {
                            return comparison;
                        }
                    }
                    return 0; // equal each other
                });
            }
        }
    };
    OrderByPipe = OrderByPipe_1 = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Pipe"])({ name: 'orderBy', pure: false })
    ], OrderByPipe);
    return OrderByPipe;
    var OrderByPipe_1;
}());

//# sourceMappingURL=order-by.pipe.js.map

/***/ }),

/***/ 839:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return Walkthrough; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(10);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__pages_index__ = __webpack_require__(31);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__components_index__ = __webpack_require__(29);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__shared_index__ = __webpack_require__(15);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var Walkthrough = /** @class */ (function () {
    function Walkthrough(navCtrl, modalCtrl, events, auth$) {
        this.navCtrl = navCtrl;
        this.modalCtrl = modalCtrl;
        this.events = events;
        this.auth$ = auth$;
        this.slides = [];
        this.currentSlideIndex = 0;
    }
    Walkthrough.prototype.ngOnInit = function () {
        this.start();
    };
    Walkthrough.prototype.ionViewDidLoad = function () {
        var _this = this;
        this.slides = [
            {
                title: 'Trainers',
                paragraph: 'Find trainers around you and suiting to your goals',
                image: './assets/walkthrough/trainer.png',
            },
            {
                title: 'Gyms',
                paragraph: 'Find the best and affordable gyms around you and suiting to your goals',
                image: './assets/walkthrough/gym.png',
            },
            {
                title: 'Nutrition',
                paragraph: 'Information about nutrition, track your calories, find out suitable suppliments',
                image: './assets/walkthrough/nutrition.png'
            },
            {
                title: 'News & Blogs',
                paragraph: 'Fitness new and blogs from curators and other Personal Coach users',
                image: './assets/walkthrough/news.png'
            },
            {
                title: 'You',
                paragraph: 'List your goals, Achieve & Be the best You!',
                image: './assets/walkthrough/you.png',
                button: {
                    text: 'Get Started',
                    show: false,
                    click: function () { _this.getStarted(); }
                }
            },
        ];
    };
    Walkthrough.prototype.slideChanged = function () {
        this.currentSlideIndex = this.slider.getActiveIndex();
    };
    Walkthrough.prototype.previous = function () {
        if (this.currentSlideIndex !== 0)
            this.slider.slidePrev();
    };
    Walkthrough.prototype.next = function () {
        if (this.currentSlideIndex !== this.slides.length - 1)
            this.slider.slideNext();
    };
    Walkthrough.prototype.getStarted = function () {
        var _this = this;
        var modal = this.modalCtrl.create(__WEBPACK_IMPORTED_MODULE_3__components_index__["c" /* Login */]);
        modal.onDidDismiss(function () {
            _this.start();
        });
        modal.present();
    };
    Walkthrough.prototype.isAuthenticated = function () {
        return this.auth$.isAuthenticated();
    };
    Walkthrough.prototype.start = function () {
        var _this = this;
        this.auth$.isAuthenticated().subscribe(function (authResp) {
            if (authResp) {
                _this.events.publish('loggedIn', authResp);
                _this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_2__pages_index__["j" /* Intro */]);
            }
        });
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])(__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* Slides */]),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* Slides */])
    ], Walkthrough.prototype, "slider", void 0);
    Walkthrough = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-walkthrough',template:/*ion-inline-start:"D:\Development\Current_Projects\2019_5_16_Ionic\personal-coach-app\src\pages\walkthrough\walkthrough.html"*/'<ion-content>\n  <ion-slides #slider pager (ionSlideDidChange)="slideChanged()">\n    <ion-slide *ngFor="let slide of slides">\n      <div class="wrap">\n        <img [src]="slide.image">\n        <h1 [innerHTML]="slide.title"></h1>\n        <p [innerHTML]="slide.paragraph"></p>\n        <i class="fa fa-lock"></i>\n        <button ion-button gradient-left box-shadow *ngIf="slide.button" (click)="slide.button.click()">{{slide.button.text}}</button>\n      </div>\n    </ion-slide>\n\n  </ion-slides>\n  <div class="buttons">\n    <button (click)="previous()" ion-button box-shadow [attr.gradient-left]="currentSlideIndex > 0 ? true: null" color="default" [attr.greydient-left]="currentSlideIndex == 0 ? true : null"\n      [outline]="currentSlideIndex == 0 ? true : null" pull-left>\n      <ion-icon name="arrow-back"></ion-icon>\n    </button>\n    <button (click)="next()" ion-button box-shadow [attr.gradient-right]="currentSlideIndex < slides.length - 1 ? true: null" color="default" [attr.greydient-left]="currentSlideIndex == slides.length - 1 ? true : null"\n      [outline]="currentSlideIndex == slides.length - 1 ? true : null" pull-right>\n      <ion-icon name="arrow-forward"></ion-icon>\n    </button>\n  </div>\n</ion-content>\n'/*ion-inline-end:"D:\Development\Current_Projects\2019_5_16_Ionic\personal-coach-app\src\pages\walkthrough\walkthrough.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* ModalController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["d" /* Events */],
            __WEBPACK_IMPORTED_MODULE_4__shared_index__["a" /* AuthProvider */]])
    ], Walkthrough);
    return Walkthrough;
}());

//# sourceMappingURL=walkthrough.js.map

/***/ }),

/***/ 840:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return Signup; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(10);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_forms__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__shared_index__ = __webpack_require__(15);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_ng2_validation__ = __webpack_require__(542);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_ng2_validation___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_4_ng2_validation__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_angularfire2_database__ = __webpack_require__(17);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__index__ = __webpack_require__(29);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







var Signup = /** @class */ (function () {
    function Signup(viewCtrl, modalCtrl, formBuilder, data$, auth$, ionic$, error$) {
        this.viewCtrl = viewCtrl;
        this.modalCtrl = modalCtrl;
        this.formBuilder = formBuilder;
        this.data$ = data$;
        this.auth$ = auth$;
        this.ionic$ = ionic$;
        this.error$ = error$;
    }
    Signup.prototype.ngOnInit = function () {
        this.signup_form = this.formBuilder.group({
            displayName: ['', __WEBPACK_IMPORTED_MODULE_2__angular_forms__["Validators"].required],
            email: ['dummy@ionicity.com', [__WEBPACK_IMPORTED_MODULE_2__angular_forms__["Validators"].required, __WEBPACK_IMPORTED_MODULE_4_ng2_validation__["CustomValidators"].email]],
            password: ['password', [__WEBPACK_IMPORTED_MODULE_2__angular_forms__["Validators"].required]]
        });
    };
    Signup.prototype.signUp = function (_a) {
        var _this = this;
        var value = _a.value, valid = _a.valid;
        if (valid) {
            // sign up user
            this.auth$.signUp(value).then(function (res) {
                // update user name
                _this.auth$.updateUser(value)
                    .then(function (user) {
                    // create a user entry in database that will hold other information about user
                    return _this.data$.object('/users/' + res.uid).set({ uid: res.uid, fullname: value.displayName }).then(function (user) {
                        _this.viewCtrl.dismiss();
                        _this.ionic$.presentToast('You have successfully signed up to the app');
                    }, function (error) { return _this.error$.handleError(error); });
                }, function (error) { return _this.error$.handleError(error); });
            });
        }
    };
    Signup.prototype.login = function () {
        this.viewCtrl.dismiss();
        var modal = this.modalCtrl.create(__WEBPACK_IMPORTED_MODULE_6__index__["c" /* Login */]);
        modal.present();
    };
    Signup.prototype.googleSignIn = function () {
        var _this = this;
        this.auth$.logInWithGoogle().then(function (res) {
            // create a user entry in database that will hold other information about user
            _this.data$.object('/users/' + res.user.uid).set({
                uid: res.user.uid,
                picture: res.user.photoURL,
                fullname: res.user.displayName
            }).then(function (user) {
                _this.viewCtrl.dismiss();
                _this.ionic$.presentToast('You have successfully signed up to the app');
            }, function (error) { return _this.error$.handleError(error); });
        }, function (error) { return _this.error$.handleError(error); });
    };
    Signup = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'signup',template:/*ion-inline-start:"D:\Development\Current_Projects\2019_5_16_Ionic\personal-coach-app\src\components\signup\signup.html"*/'<ion-content>\n  <div class="container custom-form">\n    <p class="formidable">sign up  <button ion-button icon-only clear pull-right (click)="viewCtrl.dismiss()"><ion-icon name="close"></ion-icon></button></p>\n    <form [formGroup]="signup_form">\n      <ion-item>\n        <ion-label fixed>\n          <ion-icon name="person-outline"></ion-icon>\n        </ion-label>\n        <ion-input type="text" formControlName="displayName" placeholder="Name"></ion-input>\n      </ion-item>\n      <ion-item>\n        <ion-label fixed>\n          <ion-icon name="mail-outline"></ion-icon>\n        </ion-label>\n        <ion-input type="email" formControlName="email" placeholder="Email"></ion-input>\n      </ion-item>\n      <ion-item>\n        <ion-label fixed>\n          <ion-icon name="lock-outline"></ion-icon>\n        </ion-label>\n        <ion-input type="password" formControlName="password" placeholder="Password"></ion-input>\n      </ion-item>\n      <ion-row class="remember-me">\n        <ion-col text-left no-padding>\n          <p (click)="rememberMe = !rememberMe">\n            <ion-icon *ngIf="rememberMe" name="checkmark-circle-outline"></ion-icon>\n            <ion-icon *ngIf="!rememberMe" name="radio-button-off-outline"></ion-icon>\n            Remember me?</p>\n        </ion-col>\n      </ion-row>\n    </form>\n    <button ion-button block gradient-left (click)="signUp(signup_form)" >SIGN UP</button>\n    <button ion-button gradient-left box-shadow block (click)="googleSignIn()">Sign Up with Google</button>\n  </div>\n</ion-content>\n<ion-footer>\n  <p text-center>Already have an account? <span class="daring" (click)="login()">Login</span></p>\n</ion-footer>'/*ion-inline-end:"D:\Development\Current_Projects\2019_5_16_Ionic\personal-coach-app\src\components\signup\signup.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["o" /* ViewController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* ModalController */],
            __WEBPACK_IMPORTED_MODULE_2__angular_forms__["FormBuilder"],
            __WEBPACK_IMPORTED_MODULE_5_angularfire2_database__["a" /* AngularFireDatabase */],
            __WEBPACK_IMPORTED_MODULE_3__shared_index__["a" /* AuthProvider */],
            __WEBPACK_IMPORTED_MODULE_3__shared_index__["f" /* IonicProvider */],
            __WEBPACK_IMPORTED_MODULE_3__shared_index__["d" /* ErrorHandlerProvider */]])
    ], Signup);
    return Signup;
}());

//# sourceMappingURL=signup.js.map

/***/ }),

/***/ 930:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return Login; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(10);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_forms__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__shared_index__ = __webpack_require__(15);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_ng2_validation__ = __webpack_require__(542);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_ng2_validation___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_4_ng2_validation__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_angularfire2_database__ = __webpack_require__(17);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__index__ = __webpack_require__(29);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







var Login = /** @class */ (function () {
    function Login(viewCtrl, modalCtrl, formBuilder, auth$, data$, ionic$, error$) {
        this.viewCtrl = viewCtrl;
        this.modalCtrl = modalCtrl;
        this.formBuilder = formBuilder;
        this.auth$ = auth$;
        this.data$ = data$;
        this.ionic$ = ionic$;
        this.error$ = error$;
    }
    Login.prototype.ngOnInit = function () {
        this.login_form = this.formBuilder.group({
            email: ['dummy@ionicity.com', [__WEBPACK_IMPORTED_MODULE_2__angular_forms__["Validators"].required, __WEBPACK_IMPORTED_MODULE_4_ng2_validation__["CustomValidators"].email]],
            password: ['password', [__WEBPACK_IMPORTED_MODULE_2__angular_forms__["Validators"].required]]
        });
    };
    Login.prototype.signIn = function (_a) {
        var _this = this;
        var value = _a.value, valid = _a.valid;
        if (valid) {
            this.auth$.logIn(value).then(function (res) { return _this.viewCtrl.dismiss(); }, function (error) { return _this.error$.handleError(error); });
        }
    };
    Login.prototype.googleSignIn = function () {
        var _this = this;
        this.auth$.logInWithGoogle().then(function (res) {
            // if user already exists
            if (_this.data$.object('/users/' + res.user.uid)) {
                _this.viewCtrl.dismiss();
            }
            else {
                _this.data$.object('/users/' + res.user.uid).set({
                    uid: res.user.uid,
                    picture: res.user.photoURL,
                    fullname: res.user.displayName
                }).then(function (user) {
                    _this.viewCtrl.dismiss();
                    _this.ionic$.presentToast('You have successfully signed up to the app');
                }, function (error) { return _this.error$.handleError(error); });
            }
        }, function (error) { return _this.error$.handleError(error); });
    };
    Login.prototype.signup = function () {
        this.viewCtrl.dismiss();
        var modal = this.modalCtrl.create(__WEBPACK_IMPORTED_MODULE_6__index__["h" /* Signup */]);
        modal.present();
    };
    Login = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'login',template:/*ion-inline-start:"D:\Development\Current_Projects\2019_5_16_Ionic\personal-coach-app\src\components\login\login.html"*/'<ion-content>\n  <div class="container custom-form bordered">\n    <p class="formidable">sign in <button ion-button icon-only clear pull-right (click)="viewCtrl.dismiss()"><ion-icon name="close"></ion-icon></button></p>\n    <form [formGroup]="login_form">\n      <ion-item>\n        <ion-label fixed>\n          <ion-icon name="mail-outline"></ion-icon>\n        </ion-label>\n        <ion-input type="email" formControlName="email" placeholder="Email"></ion-input>\n      </ion-item>\n      <ion-item>\n        <ion-label fixed>\n          <ion-icon name="lock-outline"></ion-icon>\n        </ion-label>\n        <ion-input type="password" formControlName="password" placeholder="Password"></ion-input>\n      </ion-item>\n      <ion-row class="remember-me">\n        <ion-col text-left no-padding>\n          <p (click)="rememberMe = !rememberMe">\n            <ion-icon *ngIf="rememberMe" name="checkmark-circle-outline"></ion-icon>\n            <ion-icon *ngIf="!rememberMe" name="radio-button-off-outline"></ion-icon>\n            Remember me?</p>\n        </ion-col>\n        <ion-col col-2 (click)="forgot()">\n          <ion-icon name="help-circle" color="stable"></ion-icon>\n        </ion-col>\n      </ion-row>\n    </form>\n    <button ion-button block gradient-left (click)="signIn(login_form)" [disabled]="!login_form.valid">SIGN IN</button>\n    <button ion-button gradient-left box-shadow block (click)="googleSignIn()">Sign in with Google</button>\n  </div>\n</ion-content>\n<ion-footer>\n  <p text-center>Don\'t have an account? <span class="daring" (click)="signup()">sign up</span></p>\n</ion-footer>\n'/*ion-inline-end:"D:\Development\Current_Projects\2019_5_16_Ionic\personal-coach-app\src\components\login\login.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["o" /* ViewController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* ModalController */],
            __WEBPACK_IMPORTED_MODULE_2__angular_forms__["FormBuilder"],
            __WEBPACK_IMPORTED_MODULE_3__shared_index__["a" /* AuthProvider */],
            __WEBPACK_IMPORTED_MODULE_5_angularfire2_database__["a" /* AngularFireDatabase */],
            __WEBPACK_IMPORTED_MODULE_3__shared_index__["f" /* IonicProvider */],
            __WEBPACK_IMPORTED_MODULE_3__shared_index__["d" /* ErrorHandlerProvider */]])
    ], Login);
    return Login;
}());

//# sourceMappingURL=login.js.map

/***/ }),

/***/ 931:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return Calendar; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(10);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__shared_index__ = __webpack_require__(15);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__models_index__ = __webpack_require__(932);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__index__ = __webpack_require__(29);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_moment__ = __webpack_require__(2);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_moment___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_5_moment__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6_angularfire2_database__ = __webpack_require__(17);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







var Calendar = /** @class */ (function () {
    function Calendar(navCtrl, navParams, viewCtrl, data$, auth$, error$) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.viewCtrl = viewCtrl;
        this.data$ = data$;
        this.auth$ = auth$;
        this.error$ = error$;
        this.events = new __WEBPACK_IMPORTED_MODULE_0__angular_core__["EventEmitter"]();
        this.notifications = [];
        this.weeks = [];
        this.dayEvents = [];
        this.mi = this.auth$.currentUser;
    }
    Calendar.prototype.ngOnInit = function () {
        this.defaultOptions = {
            showEvents: true,
            showEventDots: true,
            display: 'gradient',
            navType: 'left',
            isPage: true,
            showMenu: true
        };
        this.options = Object.assign(this.defaultOptions, this.options);
        this.weekDayNames = __WEBPACK_IMPORTED_MODULE_5_moment__["weekdays"]();
        this.user = this.navParams.data;
        if (!this.selected) {
            this.selected = this._removeTime(this.selected || __WEBPACK_IMPORTED_MODULE_5_moment__());
            this.month = this.selected.clone();
            this.start = this.selected.clone();
            this.start.date(1);
            this._removeTime(this.start.day(0));
        }
        this.findNotifications();
    };
    Calendar.prototype.findNotifications = function () {
        var _this = this;
        this.data$.list('/notifications/' + this.mi.uid).valueChanges()
            .subscribe(function (events) {
            _this.notifications = events;
            _this._buildMonth(_this.start, _this.month, _this.notifications);
            var today = { day: {}, events: [] };
            _this.weeks.map(function (week) {
                week.days.map(function (day) {
                    if (day.date.isSame(__WEBPACK_IMPORTED_MODULE_5_moment__(new Date()).startOf('day'), 'day')) {
                        today = day;
                    }
                });
            });
            if (today.events) {
                _this.events.emit(today);
            }
            else {
                _this.events.emit({ day: new Date(), events: [] });
            }
        }, function (error) { return _this.error$.handleError(error); });
    };
    Calendar.prototype.select = function (day) {
        this.selected = day.date;
        this.dayEvents = day.events;
        this.events.emit(day);
    };
    ;
    Calendar.prototype.next = function () {
        var next = this.month.clone();
        next.date(1);
        this._removeTime(next.month(next.month() + 1)).day(0);
        this.month = this.month.month(this.month.month() + 1);
        this._buildMonth(next, this.month, this.notifications);
    };
    ;
    Calendar.prototype.previous = function () {
        var previous = this.month.clone();
        this._removeTime(previous.month(previous.month() - 1).date(1));
        this.month.month(this.month.month() - 1);
        this._buildMonth(previous, this.month, this.notifications);
    };
    ;
    Calendar.prototype._removeTime = function (date) {
        return date.day(0).hour(0).minute(0).second(0).millisecond(0);
    };
    Calendar.prototype._buildMonth = function (start, month, calendar_events) {
        this.weeks = [];
        var done = false, date = start.clone(), monthIndex = date.month(), count = 0;
        while (!done) {
            this.weeks.push({ days: this._buildWeek(date.clone(), month, calendar_events) });
            date.add(1, "w");
            done = count++ > 2 && monthIndex !== date.month();
            monthIndex = date.month();
        }
    };
    Calendar.prototype._buildWeek = function (date, month, calendar_events) {
        var days = [];
        for (var i = 0; i < 7; i++) {
            days.push({
                name: date.format("dd").substring(0, 1),
                number: date.date(),
                isCurrentMonth: date.month() === month.month(),
                isToday: date.isSame(__WEBPACK_IMPORTED_MODULE_5_moment__(new Date()).startOf('day'), 'day'),
                date: date
            });
            date = date.clone();
            date.add(1, "d");
        }
        days.forEach(function (element) {
            calendar_events.forEach(function (eventItem) {
                var _eventItem = Object.assign({}, eventItem);
                var start = __WEBPACK_IMPORTED_MODULE_5_moment__(eventItem.start).startOf('day');
                var end = __WEBPACK_IMPORTED_MODULE_5_moment__(eventItem.start).endOf('day');
                if (__WEBPACK_IMPORTED_MODULE_5_moment__(element.date).isBetween(start, end, null, '[]')) {
                    !element.events ? element.events = [] : null;
                    element.events.push(_eventItem);
                }
            });
        });
        return days;
    };
    Calendar.prototype.open = function (event) {
        var params = {
            user: null,
            notification: event
        };
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_4__index__["e" /* NotificationForm */], params);
    };
    Calendar.prototype.openNotifications = function (selected) {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_4__index__["f" /* Notifications */], selected);
    };
    Calendar.prototype.close = function () {
        if (this.options.isPage) {
            this.navCtrl.pop();
        }
        else {
            this.viewCtrl.dismiss();
        }
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])(),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_3__models_index__["a" /* CalendarOptions */])
    ], Calendar.prototype, "options", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])(),
        __metadata("design:type", Object)
    ], Calendar.prototype, "user", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Output"])(),
        __metadata("design:type", Object)
    ], Calendar.prototype, "events", void 0);
    Calendar = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'calendar',template:/*ion-inline-start:"D:\Development\Current_Projects\2019_5_16_Ionic\personal-coach-app\src\components\calendar\calendar.html"*/'<div class="container">\n  <div class="calendar" [ngClass]="options.display === \'light\'? \'light\': \'dark\'" >\n    <!--<ion-toolbar>\n      <button ion-button menuToggle *ngIf="options.showMenu">\n      <ion-icon name="menu-outline"></ion-icon>\n    </button>\n      <ion-buttons end>\n        <button *ngIf="options.navType === \'left\' && !options.isPage" ion-button icon-only (click)="close()">\n            <ion-icon name="close"></ion-icon>\n          </button>\n        <button ion-button *ngIf="options.navType === \'left-right\'" (click)="next()">\n            <ion-icon name="arrow-forward"></ion-icon>\n          </button>\n      </ion-buttons>\n      <ion-title>{{month.format("MMMM")}} <span class="title-accent">{{month.format("YYYY")}}</span></ion-title>\n      <ion-buttons start>\n        <button ion-button (click)="previous()">\n          <ion-icon name="arrow-back"></ion-icon>\n          </button>\n        <button ion-button *ngIf="options.navType === \'left\'" (click)="next()">\n          <ion-icon name="arrow-forward"></ion-icon>\n          </button>\n      </ion-buttons>\n    </ion-toolbar>-->\n\n    <ion-row class="header">\n      <ion-col>\n        <button ion-button (click)="previous()">\n          <ion-icon name="arrow-back"></ion-icon>\n        </button>\n        <button ion-button *ngIf="options.navType === \'left\'" (click)="next()">\n          <ion-icon name="arrow-forward"></ion-icon>\n          </button>\n      </ion-col>\n      <ion-col><h2 class="title">{{month.format("MMMM")}} <span class="title-accent">{{month.format("YYYY")}}</span></h2></ion-col>\n      <ion-col>\n        <button *ngIf="options.navType === \'left\' && !options.isPage" ion-button icon-only (click)="close()">\n          <ion-icon name="close"></ion-icon>\n        </button>\n        <button ion-button *ngIf="options.navType === \'left-right\'" (click)="next()">\n          <ion-icon name="arrow-forward"></ion-icon>\n        </button>\n      </ion-col>\n    </ion-row>\n\n    <ion-row class="weeks">\n      <ion-col *ngFor="let item of weekDayNames">{{item | slice: 0: 3}}</ion-col>\n    </ion-row>\n    <div *ngIf="weeks">\n      <ion-row class="weekdays" *ngFor="let week of weeks">\n        <ion-col class="day" (click)="select(day)" *ngFor="let day of week.days">\n          <span [ngClass]="{\'today\': day.isToday, \'different-month\': !day.isCurrentMonth, \'selected\': day.date.isSame(selected) }">{{day.number}}</span>\n          <div *ngIf="options.showEventDots" class="calendar-events" [ngClass]="{\'different-month\': !day.isCurrentMonth}">\n            <span *ngFor="let events of day.events| slice: 0: 5" [ngClass]="{ \'has-events\': day.events}">\n              <span class="event-ticks" [ngStyle]="{\'backgroundColor\': events.color}"></span>\n            </span>\n          </div>\n        </ion-col>\n      </ion-row>\n    </div>\n  </div>\n\n  <!--Notifications Preview-->\n  <div class="list" *ngIf="options.showEvents">\n    <loading *ngIf="isLoading"></loading>\n\n    <div *ngIf="dayEvents?.length > 0; then dataTemplate else noDataTemplate"></div>\n\n    <ng-template #noDataTemplate>\n      <no-data></no-data>\n    </ng-template>\n\n    <ng-template #dataTemplate>\n      <h5>{{dayEvents?.length > 3 ? \'Fully booked on \' : \'Make an Appt on \' }}{{selected | amDateFormat:\'LL\'}}</h5>\n      <div class="scroll">\n        <ion-card *ngFor="let event of dayEvents; let i= index">\n          <ion-item [style.border-left-color]="event.color" text-wrap (click)="open(event)">\n            <h2>{{event.title}}</h2>\n            <p>{{event.start | date: \'shortTime\'}} - {{event.end | date: \'shortTime\'}}</p>\n            <ion-icon item-right name="arrow-forward"></ion-icon>\n          </ion-item>\n        </ion-card>\n        <p (click)="openNotifications(selected)">See More</p>\n      </div>\n    </ng-template>\n  </div>\n</div>\n'/*ion-inline-end:"D:\Development\Current_Projects\2019_5_16_Ionic\personal-coach-app\src\components\calendar\calendar.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["o" /* ViewController */],
            __WEBPACK_IMPORTED_MODULE_6_angularfire2_database__["a" /* AngularFireDatabase */],
            __WEBPACK_IMPORTED_MODULE_2__shared_index__["a" /* AuthProvider */],
            __WEBPACK_IMPORTED_MODULE_2__shared_index__["d" /* ErrorHandlerProvider */]])
    ], Calendar);
    return Calendar;
}());

//# sourceMappingURL=calendar.js.map

/***/ }),

/***/ 932:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__calendar_model__ = __webpack_require__(933);
/* harmony namespace reexport (by used) */ __webpack_require__.d(__webpack_exports__, "a", function() { return __WEBPACK_IMPORTED_MODULE_0__calendar_model__["a"]; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__trainer_model__ = __webpack_require__(934);
/* unused harmony namespace reexport */
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__gym_model__ = __webpack_require__(935);
/* unused harmony namespace reexport */
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__blog_model__ = __webpack_require__(936);
/* unused harmony namespace reexport */
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__user_model__ = __webpack_require__(937);
/* unused harmony namespace reexport */





//# sourceMappingURL=index.js.map

/***/ }),

/***/ 933:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return CalendarOptions; });
/* unused harmony export CalendarEvent */
var CalendarOptions = /** @class */ (function () {
    function CalendarOptions() {
    }
    return CalendarOptions;
}());

var CalendarEvent = /** @class */ (function () {
    function CalendarEvent() {
    }
    return CalendarEvent;
}());

//# sourceMappingURL=calendar.model.js.map

/***/ }),

/***/ 934:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* unused harmony export TrainerModel */
/* unused harmony export TrainerReviewerModel */
/* unused harmony export TrainerAchievementModel */
var TrainerModel = /** @class */ (function () {
    function TrainerModel() {
    }
    return TrainerModel;
}());

var TrainerReviewerModel = /** @class */ (function () {
    function TrainerReviewerModel() {
    }
    return TrainerReviewerModel;
}());

var TrainerAchievementModel = /** @class */ (function () {
    function TrainerAchievementModel() {
    }
    return TrainerAchievementModel;
}());

//# sourceMappingURL=trainer.model.js.map

/***/ }),

/***/ 935:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* unused harmony export GymModel */
/* unused harmony export GymDetailModel */
/* unused harmony export GymReviewsModel */
var GymModel = /** @class */ (function () {
    function GymModel() {
    }
    return GymModel;
}());

var GymDetailModel = /** @class */ (function () {
    function GymDetailModel() {
    }
    return GymDetailModel;
}());

var GymReviewsModel = /** @class */ (function () {
    function GymReviewsModel() {
    }
    return GymReviewsModel;
}());

//# sourceMappingURL=gym.model.js.map

/***/ }),

/***/ 936:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* unused harmony export BlogModel */
/* unused harmony export BlogLike */
/* unused harmony export BlogComment */
var BlogModel = /** @class */ (function () {
    function BlogModel() {
    }
    return BlogModel;
}());

;
var BlogLike = /** @class */ (function () {
    function BlogLike() {
    }
    return BlogLike;
}());

;
var BlogComment = /** @class */ (function () {
    function BlogComment() {
    }
    return BlogComment;
}());

;
//# sourceMappingURL=blog.model.js.map

/***/ }),

/***/ 937:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* unused harmony export UserModel */
var UserModel = /** @class */ (function () {
    function UserModel() {
    }
    return UserModel;
}());

//# sourceMappingURL=user.model.js.map

/***/ }),

/***/ 938:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return Chat; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(10);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_forms__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_angularfire2_database__ = __webpack_require__(17);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__shared_index__ = __webpack_require__(15);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var Chat = /** @class */ (function () {
    function Chat(navParams, data$, auth$, error$) {
        this.navParams = navParams;
        this.data$ = data$;
        this.auth$ = auth$;
        this.error$ = error$;
        this.message = new __WEBPACK_IMPORTED_MODULE_2__angular_forms__["FormControl"]();
    }
    Chat.prototype.ionViewDidLoad = function () {
        this.mi = this.auth$.currentUser;
        this.recipient = this.navParams.data;
        this._chat = this.data$.list('/chat');
        this.findChat();
    };
    Chat.prototype.ionViewDidEnter = function () {
        this.content.scrollToBottom(0);
    };
    Chat.prototype.findChat = function () {
        var _this = this;
        /**
         * get snapshots data from chats (snapshots include the key of the data and the data itself)
         * if no chat data available create a new one and use the key to create a messages channel
         * if chat is available, loop through avaiable chat
         *    while looping chat, check if chat matches the given sender and recipient id
         *    (note. sender and reciepient are dependent on who is logged in therefore check if both sender and reciepient id match for a given user using
         *       if ((snapshot.val().sender === this.mi.uid && snapshot.val().recipient === this.recipient.uid) ||
                      (snapshot.val().recipient === this.mi.uid && snapshot.val().sender === this.recipient.uid))
              )
         *    if chat exists, get the chat key and use it to get the messages channel  (this._messages = this.data$.list('/messages/' + this.key);)
         */
        this._chat.valueChanges()
            .subscribe(function (snapshots) {
            if (snapshots.length === 0) {
                _this.key = _this._chat.push({
                    sender: _this.mi.uid,
                    recipient: _this.recipient.uid
                }).key;
                _this._messages = _this.data$.list('/messages/' + _this.key);
            }
            else {
                if (!_this.key) {
                    snapshots.forEach(function (snapshot) {
                        if ((snapshot.sender === _this.mi.uid && snapshot.recipient === _this.recipient.uid) ||
                            (snapshot.recipient === _this.mi.uid && snapshot.sender === _this.recipient.uid)) {
                            _this.key = snapshot.key;
                        }
                    });
                }
                if (_this.key) {
                    _this.data$.list('/messages/' + _this.key).valueChanges().subscribe(function (msg) {
                        _this._messages = msg;
                    });
                }
                else {
                    _this.key = _this._chat.push({
                        sender: _this.mi.uid,
                        recipient: _this.recipient.uid
                    }).key;
                }
            }
        });
    };
    Chat.prototype.send = function (message) {
        var _this = this;
        this.data$.list('/messages/' + this.key).push({
            sender: this.mi.uid,
            text: message,
            read_at: new Date(),
            sent_at: new Date()
        }).then(function () {
            _this.autoReply();
            _this.message.reset();
            _this.content.scrollToBottom(0);
        }, function (error) { return _this.error$.handleError(error); });
    };
    Chat.prototype.autoReply = function () {
        var _this = this;
        setTimeout(function () {
            _this.data$.list('/messages/' + _this.key).push({
                sender: _this.recipient.uid,
                text: msgs[Math.floor(Math.random() * msgs.length)].message,
                read_at: new Date(),
                sent_at: new Date()
            }).then(function () { }, function (error) { return _this.error$.handleError(error); });
        }, Math.floor(Math.random() * 6000) + 1000);
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])(__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["c" /* Content */]),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["c" /* Content */])
    ], Chat.prototype, "content", void 0);
    Chat = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'chat',template:/*ion-inline-start:"D:\Development\Current_Projects\2019_5_16_Ionic\personal-coach-app\src\components\chat\chat.html"*/'<ion-header>\n\n  <ion-navbar>\n    <button ion-button menuToggle>\n      <ion-icon name="menu-outline"></ion-icon>\n    </button>\n    <ion-title><img [src]="recipient?.picture | image: \'url: profile\'" alt="">{{recipient?.fullname}}</ion-title>\n    <ion-buttons end>\n      <button ion-button>\n          <ion-icon name="search-outline"></ion-icon>\n        </button>\n      <button ion-button>\n          <ion-icon name="settings-outline"></ion-icon>\n        </button>\n    </ion-buttons>\n  </ion-navbar>\n\n</ion-header>\n\n<ion-content padding #content>\n  <div class="container">\n    <div *ngFor="let chat of _messages">\n      <div [ngClass]="chat.sender === mi.uid ? \'from-me\': \'from-them\'">\n        <p>{{chat.text}}</p>\n      </div>\n      <div class="clear"></div>\n    </div>\n  </div>\n</ion-content>\n<ion-footer [keyboard-attach]="content">\n  <ion-toolbar class="custom-form">\n    <ion-item>\n      <ion-input name="message" placeholder="Type message here..." [formControl]="message"></ion-input>\n    </ion-item>\n    <ion-buttons end>\n      <button ion-button color="primary" (click)="send(message.value)">\n         <ion-icon name="send" color="primary" item-left></ion-icon> Send\n      </button>\n    </ion-buttons>\n  </ion-toolbar>\n</ion-footer>\n'/*ion-inline-end:"D:\Development\Current_Projects\2019_5_16_Ionic\personal-coach-app\src\components\chat\chat.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_3_angularfire2_database__["a" /* AngularFireDatabase */],
            __WEBPACK_IMPORTED_MODULE_4__shared_index__["a" /* AuthProvider */],
            __WEBPACK_IMPORTED_MODULE_4__shared_index__["d" /* ErrorHandlerProvider */]])
    ], Chat);
    return Chat;
}());

var msgs = [{
        "message": "Integer ac leo."
    }, {
        "message": "In eleifend quam a odio."
    }, {
        "message": "Donec ut mauris eget massa tempor convallis."
    }, {
        "message": "Maecenas tincidunt lacus at velit."
    }, {
        "message": "Phasellus in felis."
    }, {
        "message": "Donec odio justo, sollicitudin ut, suscipit a, feugiat et, eros."
    }, {
        "message": "Nulla tellus."
    }, {
        "message": "In eleifend quam a odio."
    }, {
        "message": "Nulla justo."
    }, {
        "message": "Pellentesque at nulla."
    }, {
        "message": "Nam tristique tortor eu pede."
    }, {
        "message": "Donec dapibus."
    }, {
        "message": "Donec ut mauris eget massa tempor convallis."
    }, {
        "message": "Fusce posuere felis sed lacus."
    }, {
        "message": "In blandit ultrices enim."
    }, {
        "message": "Morbi porttitor lorem id ligula."
    }, {
        "message": "Mauris sit amet eros."
    }, {
        "message": "Nullam porttitor lacus at turpis."
    }, {
        "message": "Nunc nisl."
    }, {
        "message": "Nulla ac enim."
    }, {
        "message": "Cras non velit nec nisi vulputate nonummy."
    }, {
        "message": "Curabitur at ipsum ac tellus semper interdum."
    }, {
        "message": "Nunc purus."
    }, {
        "message": "In hac habitasse platea dictumst."
    }, {
        "message": "Curabitur at ipsum ac tellus semper interdum."
    }, {
        "message": "Duis ac nibh."
    }, {
        "message": "Duis at velit eu est congue elementum."
    }, {
        "message": "Pellentesque at nulla."
    }, {
        "message": "Praesent blandit."
    }, {
        "message": "Lorem ipsum dolor sit amet, consectetuer adipiscing elit."
    }, {
        "message": "Quisque porta volutpat erat."
    }, {
        "message": "Pellentesque viverra pede ac diam."
    }, {
        "message": "Etiam vel augue."
    }, {
        "message": "Etiam pretium iaculis justo."
    }, {
        "message": "Nullam varius."
    }, {
        "message": "Morbi porttitor lorem id ligula."
    }, {
        "message": "Suspendisse accumsan tortor quis turpis."
    }, {
        "message": "Mauris sit amet eros."
    }, {
        "message": "Vivamus in felis eu sapien cursus vestibulum."
    }, {
        "message": "Integer non velit."
    }, {
        "message": "Maecenas tincidunt lacus at velit."
    }, {
        "message": "Vestibulum quam sapien, varius ut, blandit non, interdum in, ante."
    }, {
        "message": "Morbi odio odio, elementum eu, interdum eu, tincidunt in, leo."
    }, {
        "message": "Vivamus metus arcu, adipiscing molestie, hendrerit at, vulputate vitae, nisl."
    }, {
        "message": "Quisque id justo sit amet sapien dignissim vestibulum."
    }, {
        "message": "Mauris lacinia sapien quis libero."
    }, {
        "message": "Vivamus vel nulla eget eros elementum pellentesque."
    }, {
        "message": "Phasellus in felis."
    }, {
        "message": "Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus."
    }, {
        "message": "Integer ac neque."
    }, {
        "message": "Integer a nibh."
    }, {
        "message": "Aliquam non mauris."
    }, {
        "message": "Vivamus vestibulum sagittis sapien."
    }, {
        "message": "Morbi vel lectus in quam fringilla rhoncus."
    }, {
        "message": "Integer ac neque."
    }, {
        "message": "Morbi non quam nec dui luctus rutrum."
    }, {
        "message": "Sed accumsan felis."
    }, {
        "message": "Vestibulum sed magna at nunc commodo placerat."
    }, {
        "message": "Maecenas leo odio, condimentum id, luctus nec, molestie sed, justo."
    }, {
        "message": "Donec ut dolor."
    }, {
        "message": "Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus."
    }, {
        "message": "In blandit ultrices enim."
    }, {
        "message": "Donec semper sapien a libero."
    }, {
        "message": "Nam dui."
    }, {
        "message": "In sagittis dui vel nisl."
    }, {
        "message": "Nam tristique tortor eu pede."
    }, {
        "message": "Nulla ut erat id mauris vulputate elementum."
    }, {
        "message": "Proin risus."
    }, {
        "message": "Proin eu mi."
    }, {
        "message": "Quisque erat eros, viverra eget, congue eget, semper rutrum, nulla."
    }, {
        "message": "Morbi vestibulum, velit id pretium iaculis, diam erat fermentum justo, nec condimentum neque sapien placerat ante."
    }, {
        "message": "Vivamus in felis eu sapien cursus vestibulum."
    }, {
        "message": "Nulla justo."
    }, {
        "message": "Aenean auctor gravida sem."
    }, {
        "message": "Nunc nisl."
    }, {
        "message": "Sed accumsan felis."
    }, {
        "message": "Quisque ut erat."
    }, {
        "message": "Morbi vel lectus in quam fringilla rhoncus."
    }, {
        "message": "In est risus, auctor sed, tristique in, tempus sit amet, sem."
    }, {
        "message": "Nulla mollis molestie lorem."
    }, {
        "message": "In hac habitasse platea dictumst."
    }, {
        "message": "Quisque porta volutpat erat."
    }, {
        "message": "Cras pellentesque volutpat dui."
    }, {
        "message": "Aenean fermentum."
    }, {
        "message": "Sed sagittis."
    }, {
        "message": "Ut at dolor quis odio consequat varius."
    }, {
        "message": "Nullam varius."
    }, {
        "message": "Nullam molestie nibh in lectus."
    }, {
        "message": "In congue."
    }, {
        "message": "Nunc purus."
    }, {
        "message": "Integer ac neque."
    }, {
        "message": "Fusce lacus purus, aliquet at, feugiat non, pretium quis, lectus."
    }, {
        "message": "Vivamus metus arcu, adipiscing molestie, hendrerit at, vulputate vitae, nisl."
    }, {
        "message": "Duis bibendum."
    }, {
        "message": "Quisque porta volutpat erat."
    }, {
        "message": "Maecenas rhoncus aliquam lacus."
    }, {
        "message": "Nam ultrices, libero non mattis pulvinar, nulla pede ullamcorper augue, a suscipit nulla elit ac nulla."
    }, {
        "message": "Maecenas ut massa quis augue luctus tincidunt."
    }, {
        "message": "Vestibulum quam sapien, varius ut, blandit non, interdum in, ante."
    }, {
        "message": "Quisque arcu libero, rutrum ac, lobortis vel, dapibus at, diam."
    }];
//# sourceMappingURL=chat.js.map

/***/ }),

/***/ 939:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return Map; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_ionic_angular__ = __webpack_require__(10);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_native_geolocation__ = __webpack_require__(94);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__shared_index__ = __webpack_require__(15);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__pages_index__ = __webpack_require__(31);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var Map = /** @class */ (function () {
    function Map(navCtrl, zone, maps, platform, geolocation, viewCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.zone = zone;
        this.maps = maps;
        this.platform = platform;
        this.geolocation = geolocation;
        this.viewCtrl = viewCtrl;
        this.navParams = navParams;
        this.query = '';
        this.places = [];
        this.markers = [];
        this.searchDisabled = true;
        this.saveDisabled = true;
    }
    Map.prototype.ionViewDidLoad = function () {
        var _this = this;
        var coords = this.navParams.get('coords');
        var location_details = this.navParams.get('location');
        this.maps.init(this.mapElement.nativeElement, this.pleaseConnect.nativeElement, coords, location_details).then(function (result) {
            _this.autocompleteService = new google.maps.places.AutocompleteService();
            _this.placesService = new google.maps.places.PlacesService(_this.maps.map);
            _this.searchDisabled = false;
        });
    };
    Map.prototype.selectPlace = function (place) {
        var _this = this;
        this.places = [];
        var location = {
            lat: null,
            lng: null,
            name: place.name
        };
        this.placesService.getDetails({ placeId: place.place_id }, function (details) {
            _this.zone.run(function () {
                location.name = details.name;
                location.lat = details.geometry.location.lat();
                location.lng = details.geometry.location.lng();
                _this.saveDisabled = false;
                _this.maps.map.setCenter({ lat: location.lat, lng: location.lng });
                _this.location = location;
                _this.createMapMarker(details);
            });
        });
    };
    Map.prototype.createMapMarker = function (place) {
        var placeLoc = place.geometry.location;
        var marker = new google.maps.Marker({
            map: this.maps.map,
            position: placeLoc
        });
        this.place = place;
        this.markers.push(marker);
        var content = '<h3 class="info-window">' + place.name + '</h3>' +
            '<h5 class="info-window">' + place.formatted_address + '</h5>';
        // '<span (click)="more(place)">See More</span>';
        this.addInfoWindow(marker, content);
    };
    Map.prototype.more = function (place) {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_4__pages_index__["h" /* Gym */], place);
    };
    Map.prototype.addInfoWindow = function (marker, content) {
        var _this = this;
        // let infoWindow = new google.maps.InfoWindow({
        //   content: content
        // });
        google.maps.event.addListener(marker, 'click', function () {
            // infoWindow.open(this.maps, marker);
            _this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_4__pages_index__["h" /* Gym */], _this.place);
            console.log(_this.place);
        });
    };
    Map.prototype.searchPlace = function () {
        var _this = this;
        this.saveDisabled = true;
        if (this.query.length > 0 && !this.searchDisabled) {
            var config = {
                types: ['establishment'],
                input: this.query
            };
            this.autocompleteService.getPlacePredictions(config, function (predictions, status) {
                if (status == google.maps.places.PlacesServiceStatus.OK && predictions) {
                    _this.places = [];
                    predictions.forEach(function (prediction) {
                        _this.places.push(prediction);
                    });
                }
            });
        }
        else {
            this.places = [];
        }
    };
    Map.prototype.save = function () {
        this.viewCtrl.dismiss(this.location);
    };
    Map.prototype.close = function () {
        this.viewCtrl.dismiss();
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["ViewChild"])('map'),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_1__angular_core__["ElementRef"])
    ], Map.prototype, "mapElement", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["ViewChild"])('pleaseConnect'),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_1__angular_core__["ElementRef"])
    ], Map.prototype, "pleaseConnect", void 0);
    Map = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["Component"])({
            selector: 'map',template:/*ion-inline-start:"D:\Development\Current_Projects\2019_5_16_Ionic\personal-coach-app\src\components\map\map.html"*/'<ion-header>\n  <ion-toolbar>\n    <ion-buttons start>\n      <button ion-button (click)="close()" icon-only>\n        <ion-icon name="close"></ion-icon>\n      </button>\n    </ion-buttons>\n    <ion-title>Find Gym</ion-title>\n    <ion-buttons end>\n      <button ion-button (click)="save()" icon-only>\n      <ion-icon name="send"></ion-icon></button>\n    </ion-buttons>\n  </ion-toolbar>\n\n  <ion-toolbar>\n    <ion-searchbar [(ngModel)]="query" (ionInput)="searchPlace()"></ion-searchbar>\n  </ion-toolbar>\n\n  <ion-list>\n    <ion-item *ngFor="let place of places" (touchstart)="selectPlace(place)">{{place.description}}</ion-item>\n  </ion-list>\n\n</ion-header>\n\n\n<ion-content>\n  <div #pleaseConnect id="please-connect">\n    <p>Please connect to the Internet...</p>\n  </div>\n\n  <div #map id="map">\n    <ion-spinner></ion-spinner>\n  </div>\n\n</ion-content>'/*ion-inline-end:"D:\Development\Current_Projects\2019_5_16_Ionic\personal-coach-app\src\components\map\map.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_0_ionic_angular__["j" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_1__angular_core__["NgZone"],
            __WEBPACK_IMPORTED_MODULE_3__shared_index__["e" /* GoogleMaps */],
            __WEBPACK_IMPORTED_MODULE_0_ionic_angular__["l" /* Platform */],
            __WEBPACK_IMPORTED_MODULE_2__ionic_native_geolocation__["a" /* Geolocation */],
            __WEBPACK_IMPORTED_MODULE_0_ionic_angular__["o" /* ViewController */],
            __WEBPACK_IMPORTED_MODULE_0_ionic_angular__["k" /* NavParams */]])
    ], Map);
    return Map;
}());

//# sourceMappingURL=map.js.map

/***/ }),

/***/ 940:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return Search; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(10);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_forms__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_lodash__ = __webpack_require__(73);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_lodash___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_lodash__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var Search = /** @class */ (function () {
    function Search(navParams, viewCtrl, fb) {
        this.navParams = navParams;
        this.viewCtrl = viewCtrl;
        this.fb = fb;
        this.searchTitles = [];
    }
    Search.prototype.ngOnInit = function () {
        this.searchTitles = this.navParams.get('searchable');
        this.search_form = this.fb.group({
            search: this.fb.array([])
        });
        var objArray = [];
        this.searchTitles.map(function (title, index) {
            var obj = {};
            obj[title] = [];
            objArray.push(obj);
        });
        for (var index = 0; index < this.searchTitles.length; index++) {
            var control = this.search_form.controls['search'];
            control.push(this.fb.group(objArray[index]));
        }
    };
    /*
    if search_form is empty, return nothing, else return results from the search_form
    */
    Search.prototype.search = function (_a) {
        var _this = this;
        var value = _a.value, valid = _a.valid;
        var search = [];
        value.search.map(function (element, index) {
            if (element[_this.searchTitles[index]] !== null)
                search.push(element);
        });
        if (search.length !== 0) {
            var result = __WEBPACK_IMPORTED_MODULE_3_lodash__["reduce"](search, function (memo, current) {
                return __WEBPACK_IMPORTED_MODULE_3_lodash__["extend"](memo, current);
            }, {});
            this.viewCtrl.dismiss(result);
        }
        else {
            this.viewCtrl.dismiss();
        }
    };
    Search.prototype.reset = function () {
        this.viewCtrl.dismiss();
    };
    Search = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'search',template:/*ion-inline-start:"D:\Development\Current_Projects\2019_5_16_Ionic\personal-coach-app\src\components\search\search.html"*/'<ion-content >\n  <div class="container custom-form" *ngIf="searchTitles">\n    <p class="formidable">Search <span>by</span> <button ion-button icon-only clear pull-right (click)="viewCtrl.dismiss()"><ion-icon name="close"></ion-icon></button></p>\n    <form [formGroup]="search_form">\n      <div formArrayName="search">\n      <ion-item *ngFor="let item of search_form.controls.search.controls; let i = index"  [formGroupName]="i">\n        <ion-label fixed>{{searchTitles[i]}}</ion-label>\n        <ion-input type="text" [formControlName]="searchTitles[i]"></ion-input>\n      </ion-item>\n      </div>\n    </form>\n    </div>\n</ion-content>\n\n<div class="floating">\n  <button ion-button block gradient-left (click)="search(search_form)">Search</button>\n  <button ion-button block danger-gradient-left (click)="reset(search_form)">Reset</button>\n</div>'/*ion-inline-end:"D:\Development\Current_Projects\2019_5_16_Ionic\personal-coach-app\src\components\search\search.html"*/
        })
        /*
          Dynamically generate a form based on specified fields to search from.
          send form data as an array of string like ['title', 'content', 'rating'] as a navigation parameter with key 'searchable'.
          example from trainers page, oepn search modal and send the following as its nav parameters {searchable: ['fullname', 'gym', 'rating']}
          the search_form will create an array of fields based on the titles specified in the searchable array.
        
          when search button is clicked, the resulting form object will be sent back to the trainers page and each value will be used to filter the array of trianers and return results that match
         */
        ,
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["o" /* ViewController */],
            __WEBPACK_IMPORTED_MODULE_2__angular_forms__["FormBuilder"]])
    ], Search);
    return Search;
}());

//# sourceMappingURL=search.js.map

/***/ }),

/***/ 941:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return Notifications; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(10);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_angularfire2_database__ = __webpack_require__(17);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__shared_index__ = __webpack_require__(15);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__components_index__ = __webpack_require__(29);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_moment__ = __webpack_require__(2);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_moment___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_5_moment__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






var Notifications = /** @class */ (function () {
    function Notifications(navParams, modalCtrl, data$, auth$) {
        this.navParams = navParams;
        this.modalCtrl = modalCtrl;
        this.data$ = data$;
        this.auth$ = auth$;
    }
    Notifications.prototype.ngOnInit = function () {
        this.selected_date = this.navParams.data;
        this.mi = this.auth$.currentUser;
        this.getData();
    };
    Notifications.prototype.getData = function () {
        var _this = this;
        this.isLoading = true;
        if (!this.selected_date) {
            this.selected_date = new Date().toString();
        }
        this.data$.list('/notifications/' + this.mi.uid, function (ref) { return ref.orderByChild('start').equalTo(__WEBPACK_IMPORTED_MODULE_5_moment___default()(_this.selected_date).startOf('day').toString()); }).valueChanges()
            .subscribe(function (events) {
            _this.isLoading = false;
            _this.notifications = _this.notificationsBackUp = events;
        });
    };
    Notifications.prototype.search = function () {
        var _this = this;
        var searchItems = ['title', 'content'];
        var modal = this.modalCtrl.create(__WEBPACK_IMPORTED_MODULE_4__components_index__["g" /* Search */], { searchable: searchItems });
        modal.onDidDismiss(function (search) {
            if (search) {
                _this.notifications = _this.notificationsBackUp;
                _this.notifications = _this.notifications.filter(function (notification) {
                    return search.title ? notification.title.toLowerCase().indexOf(search.title.toLowerCase()) > -1 : null;
                });
            }
        });
        modal.present();
    };
    Notifications = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'notifications',template:/*ion-inline-start:"D:\Development\Current_Projects\2019_5_16_Ionic\personal-coach-app\src\components\notifications\notifications.html"*/'<ion-header>\n\n  <ion-navbar>\n    <button ion-button menuToggle>\n      <ion-icon name="menu-outline"></ion-icon>\n    </button>\n    <ion-title>Notifications</ion-title>\n    <ion-buttons end>\n      <button ion-button (click)="search()">\n          <ion-icon name="search-outline"></ion-icon>\n      </button>\n    </ion-buttons>\n  </ion-navbar>\n\n</ion-header>\n\n<ion-content padding>\n  <loading *ngIf="isLoading"></loading>\n\n  <div class="container" *ngIf="!isLoading">\n\n    <div *ngIf="notifications?.length > 0; then dataTemplate else noDataTemplate"></div>\n\n    <ng-template #noDataTemplate>\n      <no-data [options]="{text:\'You have no Notifications, create one from schedule page\'}"></no-data>\n    </ng-template>\n\n    <ng-template #dataTemplate>\n      <ion-card *ngFor="let item of notifications">\n        <ion-row>\n          <ion-col col-3 [style.border-left-color]="item.color">\n            {{item.start | date: \'dd\'}}\n            <p>{{item.start | date: \'MMM\'}}</p>\n          </ion-col>\n          <ion-col>\n            <h2>{{item.title}}</h2>\n            <p>{{item.start_time | date:\'shortTime\'}} - {{item.end_time| date:\'shortTime\'}}</p>\n          </ion-col>\n        </ion-row>\n      </ion-card>\n    </ng-template>\n  </div>\n</ion-content>\n'/*ion-inline-end:"D:\Development\Current_Projects\2019_5_16_Ionic\personal-coach-app\src\components\notifications\notifications.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* ModalController */],
            __WEBPACK_IMPORTED_MODULE_2_angularfire2_database__["a" /* AngularFireDatabase */],
            __WEBPACK_IMPORTED_MODULE_3__shared_index__["a" /* AuthProvider */]])
    ], Notifications);
    return Notifications;
}());

//# sourceMappingURL=notifications.js.map

/***/ }),

/***/ 942:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return NotificationForm; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(10);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_forms__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__shared_index__ = __webpack_require__(15);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_angularfire2_database__ = __webpack_require__(17);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_moment__ = __webpack_require__(2);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_moment___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_5_moment__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






var NotificationForm = /** @class */ (function () {
    function NotificationForm(fb, navParams, viewCtrl, data$, auth$, ionic$, error$) {
        this.fb = fb;
        this.navParams = navParams;
        this.viewCtrl = viewCtrl;
        this.data$ = data$;
        this.auth$ = auth$;
        this.ionic$ = ionic$;
        this.error$ = error$;
        this.recurrences = [
            {
                value: 'none',
                text: 'None'
            }, {
                value: 'daily',
                text: 'Daily'
            }, {
                value: 'weekly',
                text: 'Weekly'
            }, {
                value: 'fortnightly',
                text: 'Fortnightly'
            }, {
                value: 'monthly',
                text: 'Monthly'
            }, {
                value: 'yearly',
                text: 'Yearly'
            },
        ];
    }
    NotificationForm.prototype.ngOnInit = function () {
        this.mi = this.auth$.currentUser;
        this.notification = this.navParams.get('notification');
        this.with_user = this.navParams.get('user');
        this.key = this.navParams.get('key');
        this.notification_form = this.fb.group({
            title: [this.notification ? this.notification.title : ''],
            start: [this.notification ? new Date(this.notification.start).toISOString() : new Date()],
            start_time: [this.notification ? this.notification.start_time : ''],
            end: [this.notification ? new Date(this.notification.end).toISOString() : new Date()],
            end_time: [this.notification ? this.notification.end_time : ''],
            recurrence: [this.notification ? this.notification.recurrence : 'none'],
            color: [this.notification ? this.notification.color : '#' + Math.floor(Math.random() * 16777215).toString(16)],
            extras: [this.notification ? this.notification.extras : ''],
            created_by: [this.mi.uid],
            with: [this.notification.with],
            user_detail: this.fb.group({
                fullname: this.mi.fullname,
                picture: this.mi.picture
            })
        });
    };
    NotificationForm.prototype.save = function (_a) {
        var _this = this;
        var value = _a.value, valid = _a.valid;
        if (valid) {
            value.start = __WEBPACK_IMPORTED_MODULE_5_moment___default()(value.start).startOf('day').toString();
            if (!this.key) {
                this.data$.list('/notifications/' + this.mi.uid).push(value).then(function (appt) { return _this.ionic$.presentToast('Notification Created'); }, function (error) { return _this.error$.handleError(error); });
            }
            else {
                this.data$.list('/notifications/' + this.mi.uid).update(this.key, value)
                    .then(function (appt) { return _this.ionic$.presentToast('Notification Updated'); }, function (error) { return _this.error$.handleError(error); });
                ;
            }
            this.viewCtrl.dismiss();
        }
    };
    NotificationForm = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'notification-form',template:/*ion-inline-start:"D:\Development\Current_Projects\2019_5_16_Ionic\personal-coach-app\src\components\notification-form\notification-form.html"*/'<ion-header>\n  <ion-navbar>\n     <ion-buttons start>\n    </ion-buttons>\n    <ion-title>Notifications</ion-title>\n    <ion-buttons end>\n      <button ion-button icon-only (click)="viewCtrl.dismiss()">\n          <ion-icon name="close"></ion-icon>\n      </button>\n    </ion-buttons>\n  </ion-navbar>\n</ion-header>\n\n<ion-content padding>\n  <div class="container">\n    <form [formGroup]="notification_form">\n\n      <ion-card *ngIf="with_user?.fullname">\n        <ion-item>\n          <ion-avatar item-left>\n            <img [src]="with_user.picture| image:\'url:profile\'">\n          </ion-avatar>\n          <h2>{{with_user.fullname}}</h2>\n          <p>{{with_user.gym}}</p>\n        </ion-item>\n      </ion-card>\n      <ion-card>\n        <ion-item>\n          <ion-icon item-left name="information-circle-outline"></ion-icon>\n          <ion-textarea elastic-textarea formControlName="title" placeholder="Title"></ion-textarea>\n        </ion-item>\n      </ion-card>\n      <ion-card>\n        <ion-item>\n          <ion-icon item-left name="calendar-outline"></ion-icon>\n          <ion-datetime formControlName="start" placeholder="Date" pickerFormat="DD MMMM YYYY"></ion-datetime>\n        </ion-item>\n      </ion-card>\n\n      <ion-card>\n        <ion-row>\n          <ion-item col-7>\n            <ion-icon item-left name="clock-outline"></ion-icon>\n            <ion-datetime formControlName="start_time" placeholder="Start Time" pickerFormat="HH mm"></ion-datetime>\n          </ion-item>\n          <ion-item col-5>\n            <ion-datetime formControlName="end_time" placeholder="End Time" pickerFormat="HH mm"></ion-datetime>\n          </ion-item>\n        </ion-row>\n      </ion-card>\n\n      <ion-card>\n        <ion-item>\n          <ion-icon item-left name="shuffle-outline"></ion-icon>\n          <ion-label>Recurring?</ion-label>\n          <ion-select formControlName="recurrence">\n            <ion-option [value]="recurrence.value" *ngFor="let recurrence of recurrences">{{recurrence.text}}</ion-option>\n          </ion-select>\n        </ion-item>\n      </ion-card>\n\n    </form>\n  </div>\n</ion-content>\n\n<div class="floating">\n  <button ion-button block gradient-left box-shadow (click)="save(notification_form)">Save Appointment</button>\n</div>\n'/*ion-inline-end:"D:\Development\Current_Projects\2019_5_16_Ionic\personal-coach-app\src\components\notification-form\notification-form.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_2__angular_forms__["FormBuilder"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["o" /* ViewController */],
            __WEBPACK_IMPORTED_MODULE_4_angularfire2_database__["a" /* AngularFireDatabase */],
            __WEBPACK_IMPORTED_MODULE_3__shared_index__["a" /* AuthProvider */],
            __WEBPACK_IMPORTED_MODULE_3__shared_index__["f" /* IonicProvider */],
            __WEBPACK_IMPORTED_MODULE_3__shared_index__["d" /* ErrorHandlerProvider */]])
    ], NotificationForm);
    return NotificationForm;
}());

//# sourceMappingURL=notification-form.js.map

/***/ }),

/***/ 943:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return Trainers; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(10);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__index__ = __webpack_require__(31);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__components_index__ = __webpack_require__(29);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_angularfire2_database__ = __webpack_require__(17);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__shared_index__ = __webpack_require__(15);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__ionic_native_geolocation__ = __webpack_require__(94);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__ionic_native_call_number__ = __webpack_require__(98);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};








var Trainers = /** @class */ (function () {
    function Trainers(callNumber, geolocation, navCtrl, modalCtrl, data$, auth$, ionic$, error$) {
        this.callNumber = callNumber;
        this.geolocation = geolocation;
        this.navCtrl = navCtrl;
        this.modalCtrl = modalCtrl;
        this.data$ = data$;
        this.auth$ = auth$;
        this.ionic$ = ionic$;
        this.error$ = error$;
    }
    Trainers.prototype.ionViewDidLoad = function () {
        var _this = this;
        /*
        Get Current Location
        Get trainers
        user your current Location and trainers Location Coordinates to find distance away
         */
        this.isLoading = true;
        this.mi = this.auth$.currentUser;
        this.geolocation.getCurrentPosition().then(function (resp) {
            _this._trainers = _this.data$.list('/trainers');
            _this._trainers.valueChanges().subscribe(function (trainers) {
                _this.trainers = _this.trainersBackup = trainers;
                _this.trainers.forEach(function (trainer) {
                    var my_cords = [resp.coords.latitude, resp.coords.longitude];
                    var trainer_cords = [trainer.location.lat, trainer.location.lng];
                    trainer.distance_away = _this.ionic$.calculateDistance(my_cords, trainer_cords);
                });
                _this.isLoading = false;
            });
        }).catch(function (error) { return _this.error$.handleError(error); });
    };
    /*
      send an object with key 'searchable' of an array of string to parameters of modal control used to open search page.
      the array values are used to create form fields in the search modal. these field's values are then filtered to
      get resulting trainers based on search criteria
    */
    Trainers.prototype.search = function () {
        var _this = this;
        var modal = this.modalCtrl.create(__WEBPACK_IMPORTED_MODULE_3__components_index__["g" /* Search */], { searchable: ['fullname', 'city', 'country', 'gym', 'rating'] });
        modal.onDidDismiss(function (search) {
            _this.trainers = _this.trainersBackup;
            if (search) {
                _this.trainers = _this.trainers.filter(function (trainer) {
                    return search.fullname ? trainer.fullname.toLowerCase().indexOf(search.fullname.toLowerCase()) > -1 : null
                        || search.city ? trainer.city.toLowerCase().indexOf(search.city.toLowerCase()) > -1 : null
                        || search.country ? trainer.country.toLowerCase().indexOf(search.country.toLowerCase()) > -1 : null
                        || search.gym ? trainer.gym.toLowerCase().indexOf(search.gym.toLowerCase()) > -1 : null
                        || search.rating ? trainer.rating === +search.rating : null;
                });
            }
        });
        modal.present();
    };
    Trainers.prototype.open = function (trainer) {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_2__index__["s" /* Trainer */], trainer);
    };
    Trainers.prototype.call = function (trainer, fab) {
        fab.close();
        this.callNumber.callNumber(trainer.phone, true);
    };
    Trainers.prototype.chat = function (trainer, fab) {
        fab.close();
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_3__components_index__["b" /* Chat */], trainer);
    };
    Trainers.prototype.calendar = function (trainer, fab) {
        fab.close();
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_2__index__["b" /* Appointment */], trainer);
    };
    Trainers = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-trainers',template:/*ion-inline-start:"D:\Development\Current_Projects\2019_5_16_Ionic\personal-coach-app\src\pages\trainers\trainers.html"*/'<ion-header>\n\n  <ion-navbar>\n    <button ion-button menuToggle>\n      <ion-icon name="menu-outline"></ion-icon>\n    </button>\n    <ion-title>trainers</ion-title>\n    <ion-buttons end>\n      <button ion-button (click)="search()">\n          <ion-icon name="search-outline"></ion-icon>\n      </button>\n    </ion-buttons>\n  </ion-navbar>\n</ion-header>\n\n<ion-content padding>\n  <loading *ngIf="isLoading"></loading>\n\n  <div class="container" *ngIf="!isLoading">\n\n    <div *ngIf="trainers?.length > 0; then dataTemplate else noDataTemplate"></div>\n\n    <ng-template #noDataTemplate>\n      <no-data></no-data>\n    </ng-template>\n\n    <ng-template #dataTemplate>\n      <!--Filter by closest trainer-->\n      <div class="trainer-card" *ngFor="let trainer of trainers | orderBy: \'+distance_away\'">\n        <img [src]="trainer.picture | image: \'url: profile\'" (click)="open(trainer)">\n        <div class="card">\n          <div class="desc" (click)="open(trainer)">\n            <h2>{{trainer.fullname}}</h2>\n            <p>{{trainer.gym}}, {{trainer.country}}</p>\n            <p>\n              <ion-icon name="pin"></ion-icon> {{trainer.distance_away | number:\'2.1-2\'}} km away</p>\n          </div>\n          <div class="more">\n            <p>\n              <ion-icon name="star-outline"></ion-icon> {{trainer.rating}}\n            </p>\n            <ion-fab class="trainer" middle right edge #fab>\n              <button ion-fab mini><ion-icon name="md-more"></ion-icon></button>\n              <ion-fab-list side="left">\n                <button ion-fab (click)="call(trainer, fab)"><ion-icon name="call-outline" color="primary"></ion-icon></button>\n                <button ion-fab (click)="chat(trainer, fab)"><ion-icon name="mail-outline" color="primary"></ion-icon></button>\n                <button ion-fab (click)="calendar(trainer, fab)"><ion-icon name="calendar-outline" color="primary"></ion-icon></button>\n              </ion-fab-list>\n            </ion-fab>\n          </div>\n        </div>\n      </div>\n    </ng-template>\n  </div>\n\n</ion-content>\n'/*ion-inline-end:"D:\Development\Current_Projects\2019_5_16_Ionic\personal-coach-app\src\pages\trainers\trainers.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_7__ionic_native_call_number__["a" /* CallNumber */],
            __WEBPACK_IMPORTED_MODULE_6__ionic_native_geolocation__["a" /* Geolocation */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* ModalController */],
            __WEBPACK_IMPORTED_MODULE_4_angularfire2_database__["a" /* AngularFireDatabase */],
            __WEBPACK_IMPORTED_MODULE_5__shared_index__["a" /* AuthProvider */],
            __WEBPACK_IMPORTED_MODULE_5__shared_index__["f" /* IonicProvider */],
            __WEBPACK_IMPORTED_MODULE_5__shared_index__["d" /* ErrorHandlerProvider */]])
    ], Trainers);
    return Trainers;
}());

//# sourceMappingURL=trainers.js.map

/***/ }),

/***/ 944:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return Trainer; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(10);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__pages_index__ = __webpack_require__(31);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__components_index__ = __webpack_require__(29);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__ionic_native_call_number__ = __webpack_require__(98);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var Trainer = /** @class */ (function () {
    function Trainer(navCtrl, navParams, callNumber) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.callNumber = callNumber;
    }
    Trainer.prototype.ngOnInit = function () {
        this.trainer = this.navParams.data;
    };
    Trainer.prototype.call = function (trainer) {
        this.callNumber.callNumber(trainer.phone, true);
    };
    Trainer.prototype.chat = function (trainer) {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_3__components_index__["b" /* Chat */], trainer);
    };
    Trainer.prototype.bookAppt = function (trainer) {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_2__pages_index__["b" /* Appointment */], trainer);
    };
    Trainer = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'trainer',template:/*ion-inline-start:"D:\Development\Current_Projects\2019_5_16_Ionic\personal-coach-app\src\pages\trainers\trainer\trainer.html"*/'<ion-header>\n\n  <ion-navbar>\n    <button ion-button menuToggle>\n      <ion-icon name="menu-outline"></ion-icon>\n    </button>\n    <ion-title>Trainer</ion-title>\n    <!--<ion-buttons end>\n        <button ion-button>\n          <ion-icon name="search-outline"></ion-icon>\n        </button>\n        <button ion-button>\n          <ion-icon name="settings-outline"></ion-icon>\n        </button>\n    </ion-buttons>-->\n  </ion-navbar>\n\n</ion-header>\n\n<ion-content padding>\n  <div class="container">\n    <ion-row class="user-image">\n      <ion-col col-3 text-left>\n        <button (click)="call(trainer)" ion-button box-shadow gradient-left>\n          <ion-icon name="call"></ion-icon>\n        </button>\n      </ion-col>\n      <ion-col text-center>\n        <div class="image">\n          <img [src]="trainer.picture | image: \'url: profile\'">\n        </div>\n      </ion-col>\n      <ion-col col-3 text-right>\n        <button (click)="chat(trainer)" ion-button box-shadow gradient-right>\n          <ion-icon name="md-text"></ion-icon>\n        </button>\n      </ion-col>\n    </ion-row>\n    <div class="user-profile">\n      <h2>{{trainer.fullname}}</h2>\n      <p>{{trainer.gym}}, {{trainer.country}}</p>\n      <rating [rate]="trainer.rating" [color]="\'gold\'" [align]="\'center\'" ></rating>\n      <p class="reviews"> {{trainer.reviewers}} reviewers</p>\n\n      <button ion-button gradient-left box-shadow (click)="bookAppt(trainer)">Book Appointment</button>\n    </div>\n\n    <ion-card box-shadow>\n      <ion-item>\n        <ion-icon item-left name="person-outline" color="primary"></ion-icon>\n        <h2>Personal Info</h2>\n        <ion-icon item-right name="arrow-forward" color="default"></ion-icon>\n      </ion-item>\n    </ion-card>\n    <ion-card box-shadow>\n      <ion-item>\n        <ion-icon item-left name="podium-outline" color="primary"></ion-icon>\n        <h2>Achievements and History</h2>\n        <ion-icon item-right name="arrow-forward" color="default"></ion-icon>\n      </ion-item>\n    </ion-card>\n    <ion-card box-shadow>\n      <ion-item>\n        <ion-icon item-left name="star-outline" color="primary"></ion-icon>\n        <h2>Reviews</h2>\n        <ion-icon item-right name="arrow-forward" color="default"></ion-icon>\n      </ion-item>\n    </ion-card>\n  </div>\n\n</ion-content>\n'/*ion-inline-end:"D:\Development\Current_Projects\2019_5_16_Ionic\personal-coach-app\src\pages\trainers\trainer\trainer.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_4__ionic_native_call_number__["a" /* CallNumber */]])
    ], Trainer);
    return Trainer;
}());

//# sourceMappingURL=trainer.js.map

/***/ }),

/***/ 945:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return Appointment; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(10);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__components_index__ = __webpack_require__(29);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_moment__ = __webpack_require__(2);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_moment___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_moment__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var Appointment = /** @class */ (function () {
    function Appointment(navParams, modalCtrl) {
        this.navParams = navParams;
        this.modalCtrl = modalCtrl;
    }
    Appointment.prototype.ngOnInit = function () {
        this.cal_options = {
            display: 'light',
            showEvents: false,
            showEventDots: true,
            navType: 'left-right',
            isPage: false,
            showMenu: false
        };
        this.trainer = this.navParams.data;
    };
    Appointment.prototype.onEventsChange = function (day) {
        this.selected_day = day;
    };
    Appointment.prototype.makeAppt = function (day) {
        var params = {
            user: this.trainer,
            notification: {
                extras: {},
                with: this.trainer.uid,
                start: __WEBPACK_IMPORTED_MODULE_3_moment___default()(day).toISOString(),
                end: __WEBPACK_IMPORTED_MODULE_3_moment___default()(day).toISOString(),
                recurrence: 'none',
                color: '#' + Math.floor(Math.random() * 16777215).toString(16),
                start_time: __WEBPACK_IMPORTED_MODULE_3_moment___default()().startOf('date').toISOString(),
                end_time: __WEBPACK_IMPORTED_MODULE_3_moment___default()().endOf('date').toISOString(),
            }
        };
        var modal = this.modalCtrl.create(__WEBPACK_IMPORTED_MODULE_2__components_index__["e" /* NotificationForm */], params);
        modal.onDidDismiss(function () { });
        modal.present();
    };
    Appointment = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'appointment',template:/*ion-inline-start:"D:\Development\Current_Projects\2019_5_16_Ionic\personal-coach-app\src\pages\trainers\trainer\appointment\appointment.html"*/'<ion-header>\n\n  <ion-navbar>\n    <button ion-button menuToggle>\n      <ion-icon name="menu-outline"></ion-icon>\n    </button>\n    <ion-title>{{trainer.fullname.split(\' \')[0] + "\'s Appointments"}}</ion-title>\n\n  </ion-navbar>\n\n</ion-header>\n\n<ion-content>\n  <div class="appt-container">\n    <div class="user-image">\n      <img [src]="trainer.picture">\n    </div>\n    <ion-card class="user-profile">\n      <h2>{{trainer.fullname}}</h2>\n      <p>{{trainer.gym}}, {{trainer.country}}</p>\n      <p><strong>{{trainer.distance_away | number:\'2.1-2\'}} km</strong> away from you</p>\n      <calendar [options]="cal_options" [user]="trainer" (events)="onEventsChange($event)"></calendar>\n\n    </ion-card>\n  </div>\n</ion-content>\n\n<div class="floating">\n  <button ion-button block \n  [attr.gradient-left]="selected_day?.events && selected_day?.events?.length < 3 ? true: null" \n  [attr.danger-gradient-left]="selected_day?.events && selected_day?.events?.length > 3 ? true : null"\n  [attr.greydient-left]="!selected_day?.events ? true : null"\n    box-shadow (click)="makeAppt(selected_day?.day)">{{selected_day?.events?.length > 3 ? \'Fully booked\': \'Make an Appointment\'}}</button>\n</div>\n'/*ion-inline-end:"D:\Development\Current_Projects\2019_5_16_Ionic\personal-coach-app\src\pages\trainers\trainer\appointment\appointment.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* ModalController */]])
    ], Appointment);
    return Appointment;
}());

//# sourceMappingURL=appointment.js.map

/***/ }),

/***/ 946:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return Intro; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(10);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__shared_index__ = __webpack_require__(15);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__index__ = __webpack_require__(31);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__components_index__ = __webpack_require__(29);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var Intro = /** @class */ (function () {
    function Intro(navCtrl, auth$, events) {
        this.navCtrl = navCtrl;
        this.auth$ = auth$;
        this.events = events;
    }
    Intro.prototype.ngOnInit = function () {
        var _this = this;
        this.auth$.getUser().subscribe(function (user) {
            _this.mi = user;
            _this.events.publish('loggedIn', _this.mi);
        });
        this.menus = [
            {
                icon: './assets/walkthrough/trainer.png',
                head: 'Trainer',
                sub: 'Find a trainer',
                component: __WEBPACK_IMPORTED_MODULE_3__index__["t" /* Trainers */]
            }, {
                icon: './assets/walkthrough/gym.png',
                head: 'Gym',
                sub: 'Find a gym',
                component: __WEBPACK_IMPORTED_MODULE_3__index__["i" /* Gyms */]
            }, {
                icon: './assets/walkthrough/news.png',
                head: 'Blogs',
                sub: 'Information Center',
                component: __WEBPACK_IMPORTED_MODULE_3__index__["e" /* Blogs */]
            }, {
                icon: './assets/walkthrough/you.png',
                head: 'You',
                sub: 'All about you!',
                component: __WEBPACK_IMPORTED_MODULE_3__index__["l" /* Me */]
            },
        ];
        this.rows = Array.from(Array(Math.ceil(this.menus.length / 2)).keys());
    };
    Intro.prototype.open = function (menu) {
        this.navCtrl.setRoot(menu.component);
    };
    Intro.prototype.openMe = function () {
        this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_3__index__["l" /* Me */]);
    };
    Intro.prototype.openNotifications = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_4__components_index__["f" /* Notifications */]);
    };
    Intro = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'intro',template:/*ion-inline-start:"D:\Development\Current_Projects\2019_5_16_Ionic\personal-coach-app\src\pages\intro\intro.html"*/'<ion-header>\n\n  <ion-navbar>\n    <button ion-button menuToggle>\n      <ion-icon name="menu-outline"></ion-icon>\n    </button>\n    <ion-title><img src="./assets/coach-lite.png"></ion-title>\n    <ion-buttons end>\n      <button ion-button>\n          <ion-icon name="mail-outline"></ion-icon>\n      </button>\n      <button ion-button (click)="openNotifications()">\n          <ion-icon name="notifications-outline"></ion-icon>\n      </button>\n      <button ion-button (click)="openMe()">\n          <ion-icon name="person-outline"></ion-icon>\n      </button>\n    </ion-buttons>\n  </ion-navbar>\n\n</ion-header>\n\n<ion-content padding>\n  <div class="container">\n    <p class="formidable">Hello {{mi?.fullname ? mi?.fullname.split(\' \')[0] : \'there\'}},</p>\n    <p class="formidable-light">how can we help you stay fit?</p>\n\n    <ion-grid class="menu">\n      <ion-row *ngFor="let i of rows;">\n        <ion-col *ngFor="let menu of menus | slice:(i*2):(i+1)*2">\n          <ion-card (click)="open(menu)">\n            <img [src]="menu.icon" alt="">\n            <div class="desc">\n              <h2 [innerHTML]="menu.head"></h2>\n              <p [innerHTML]="menu.sub"></p>\n            </div>\n          </ion-card>\n        </ion-col>\n      </ion-row>\n    </ion-grid>\n  </div>\n\n</ion-content>\n'/*ion-inline-end:"D:\Development\Current_Projects\2019_5_16_Ionic\personal-coach-app\src\pages\intro\intro.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_2__shared_index__["a" /* AuthProvider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["d" /* Events */]])
    ], Intro);
    return Intro;
}());

//# sourceMappingURL=intro.js.map

/***/ }),

/***/ 947:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return Gyms; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(10);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_native_geolocation__ = __webpack_require__(94);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_angularfire2_database__ = __webpack_require__(17);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__shared_index__ = __webpack_require__(15);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__index__ = __webpack_require__(31);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__components_index__ = __webpack_require__(29);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__ionic_native_call_number__ = __webpack_require__(98);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__ionic_native_in_app_browser__ = __webpack_require__(228);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};









var Gyms = /** @class */ (function () {
    function Gyms(navCtrl, modalCtrl, callNumber, geolocation, iab, data$, auth$, ionic$, error$) {
        this.navCtrl = navCtrl;
        this.modalCtrl = modalCtrl;
        this.callNumber = callNumber;
        this.geolocation = geolocation;
        this.iab = iab;
        this.data$ = data$;
        this.auth$ = auth$;
        this.ionic$ = ionic$;
        this.error$ = error$;
    }
    Gyms.prototype.ionViewDidLoad = function () {
        var _this = this;
        /*
        Get Current Location
        Get trainers
        user your current Location and trainers Location Coordinates to find distance away
         */
        this.mi = this.auth$.currentUser;
        this.geolocation.getCurrentPosition().then(function (resp) {
            _this._gyms = _this.data$.list('/gyms');
            _this.isLoading = true;
            _this._gyms.valueChanges().subscribe(function (gyms) {
                _this.gyms = _this.gymsBackup = gyms;
                _this.gyms.forEach(function (gym) {
                    var my_cords = [resp.coords.latitude, resp.coords.longitude];
                    var gym_cords = [gym.geometry.location.lat, gym.geometry.location.lng];
                    gym.distance_away = _this.ionic$.calculateDistance(my_cords, gym_cords);
                });
                _this.isLoading = false;
            });
        }).catch(function (error) { return _this.error$.handleError(error); });
    };
    Gyms.prototype.search = function () {
        var _this = this;
        var modal = this.modalCtrl.create(__WEBPACK_IMPORTED_MODULE_6__components_index__["g" /* Search */], { searchable: ['name', 'vicinity', 'rating'] });
        modal.onDidDismiss(function (search) {
            _this.gyms = _this.gymsBackup;
            if (search) {
                _this.gyms = _this.gyms.filter(function (gym) {
                    return search.name ? gym.name.toLowerCase().indexOf(search.fullname.toLowerCase()) > -1 : null
                        || search.vicinity ? gym.vicinity.toLowerCase().indexOf(search.city.toLowerCase()) > -1 : null
                        || search.rating ? gym.rating === +search.rating : null;
                });
            }
        });
        modal.present();
    };
    Gyms.prototype.open = function (gym) {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_5__index__["h" /* Gym */], gym);
    };
    Gyms.prototype.call = function (gym, fab) {
        fab.close();
        this.callNumber.callNumber(gym.detail.formatted_phone_number, true);
    };
    Gyms.prototype.map = function (gym, fab) {
        fab.close();
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_6__components_index__["d" /* Map */], { coords: [gym.geometry.location.lat, gym.geometry.location.lng], location: gym });
    };
    Gyms.prototype.webpage = function (gym, fab) {
        fab.close();
        this.iab.create(gym.detail.website);
    };
    Gyms = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-gyms',template:/*ion-inline-start:"D:\Development\Current_Projects\2019_5_16_Ionic\personal-coach-app\src\pages\gyms\gyms.html"*/'<ion-header>\n\n  <ion-navbar>\n    <button ion-button menuToggle>\n      <ion-icon name="menu-outline"></ion-icon>\n    </button>\n    <ion-title>gyms</ion-title>\n    <ion-buttons end>\n      <button ion-button (click)="search()">\n          <ion-icon name="search-outline"></ion-icon>\n      </button>\n    </ion-buttons>\n  </ion-navbar>\n</ion-header>\n\n\n<ion-content>\n  <loading *ngIf="isLoading"></loading>\n\n  <div class="container" *ngIf="!isLoading">\n\n    <div *ngIf="gyms?.length > 0; then dataTemplate else noDataTemplate"></div>\n\n    <ng-template #noDataTemplate>\n      <no-data></no-data>\n    </ng-template>\n\n    <ng-template #dataTemplate>\n      <!--Filter by closest trainer-->\n      <div class="gym-card" *ngFor="let gym of gyms | orderBy: \'+distance_away\'">\n        <div class="image">\n          <ion-fab class="gym" top left #fab>\n            <button ion-fab mini><ion-icon name="md-more"></ion-icon></button>\n            <ion-fab-list side="right">\n              <button ion-fab (click)="call(gym, fab)"><ion-icon name="call-outline" color="primary"></ion-icon></button>\n              <button ion-fab (click)="map(gym, fab)"><ion-icon name="pin-outline" color="primary"></ion-icon></button>\n              <button ion-fab (click)="webpage(gym, fab)"><ion-icon name="globe-outline" color="primary"></ion-icon></button>\n            </ion-fab-list>\n          </ion-fab>\n          <img src="./assets/walkthrough/gym.png" (click)="open(gym)">\n          <p (click)="open(gym)">\n            <ion-icon name="pin"></ion-icon>{{gym.distance_away | number:\'1.1-2\'}} km away</p>\n        </div>\n        <div class="desc" (click)="open(gym)">\n          <div class="info">\n            <h2>{{gym.name}}</h2>\n            <p>{{gym.vicinity | slice: 0: 40}}...</p>\n            <span [ngClass]="gym?.opening_hours?.open_now ? \'opened\': \'closed\'">{{gym?.opening_hours?.open_now ? \'Open Now\': \'Closed\'}}</span>\n          </div>\n          <rating [rate]="gym.rating" [color]="\'gold\'" [align]="\'flex-end\'"></rating>\n        </div>\n      </div>\n    </ng-template>\n  </div>\n</ion-content>\n'/*ion-inline-end:"D:\Development\Current_Projects\2019_5_16_Ionic\personal-coach-app\src\pages\gyms\gyms.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* ModalController */],
            __WEBPACK_IMPORTED_MODULE_7__ionic_native_call_number__["a" /* CallNumber */],
            __WEBPACK_IMPORTED_MODULE_2__ionic_native_geolocation__["a" /* Geolocation */],
            __WEBPACK_IMPORTED_MODULE_8__ionic_native_in_app_browser__["a" /* InAppBrowser */],
            __WEBPACK_IMPORTED_MODULE_3_angularfire2_database__["a" /* AngularFireDatabase */],
            __WEBPACK_IMPORTED_MODULE_4__shared_index__["a" /* AuthProvider */],
            __WEBPACK_IMPORTED_MODULE_4__shared_index__["f" /* IonicProvider */],
            __WEBPACK_IMPORTED_MODULE_4__shared_index__["d" /* ErrorHandlerProvider */]])
    ], Gyms);
    return Gyms;
}());

//# sourceMappingURL=gyms.js.map

/***/ }),

/***/ 948:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return Gym; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(10);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__components_index__ = __webpack_require__(29);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_native_call_number__ = __webpack_require__(98);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_angularfire2_database__ = __webpack_require__(17);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__ionic_native_in_app_browser__ = __webpack_require__(228);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






var Gym = /** @class */ (function () {
    function Gym(navCtrl, navParams, callNumber, iab, data$) {
        var _this = this;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.callNumber = callNumber;
        this.iab = iab;
        this.data$ = data$;
        this.contacts = [
            {
                icon: 'call-outline',
                click: function (e) { _this.call(_this.gym); }
            },
            {
                icon: 'globe-outline',
                click: function (e) { _this.webpage(); }
            },
            {
                icon: 'logo-twitter',
                click: function (e) { }
            },
            {
                icon: 'logo-google',
                click: function (e) { }
            }
        ];
    }
    Gym.prototype.ngOnInit = function () {
        this.gym = this.navParams.data;
        this.getMembers();
    };
    Gym.prototype.getMembers = function () {
        var _this = this;
        this.data$.list('/trainers').valueChanges().subscribe(function (trainers) {
            _this.members = trainers;
        });
    };
    Gym.prototype.webpage = function () {
        this.iab.create(this.gym.detail.website);
    };
    Gym.prototype.call = function (gym) {
        this.callNumber.callNumber(gym.detail.formatted_phone_number, true);
    };
    Gym.prototype.map = function (gym) {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_2__components_index__["d" /* Map */], { coords: [gym.geometry.location.lat, gym.geometry.location.lng], location: gym });
    };
    Gym = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'gym',template:/*ion-inline-start:"D:\Development\Current_Projects\2019_5_16_Ionic\personal-coach-app\src\pages\gyms\gym\gym.html"*/'<ion-content>\n  <div class="container">\n\n    <div class="top" gradient-left box-shadow>\n      <!--Toolbar-->\n      <ion-toolbar>\n        <button ion-button icon-only (click)="navCtrl.pop()">\n          <ion-icon name="arrow-back"></ion-icon>\n        </button>\n        <ion-title>{{gym.name}}</ion-title>\n      </ion-toolbar>\n      <!--Image-->\n      <div class="image">\n        <img src="./assets/walkthrough/gym.png" (click)="open(gym)">\n\n        <!--<h2>{{gym.name}}</h2>-->\n        <p>{{gym?.detail?.formatted_address}}</p>\n        <button ion-button greydient-left box-shadow (click)="map(gym)">Visit the Gym</button>\n      </div>\n    </div>\n    <div class="bottom">\n      <ion-card>\n        <h4>About Gym</h4>\n        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer nec odio. Praesent libero. Sed cursus ante dapibus\n          diam. Sed nisi. Nulla quis sem at nibh elementum imperdiet. Duis sagittis ipsum. Praesent mauris. Fusce nec tellus\n          sed augue semper porta. Mauris massa. Vestibulum lacinia arcu eget nulla. </p>\n        <ion-row class="contact">\n          <ion-col *ngFor="let item of contacts">\n            <ion-icon [name]="item.icon"></ion-icon>\n          </ion-col>\n        </ion-row>\n      </ion-card>\n\n      <ion-card class="members">\n        <h4>Member</h4>\n        <ion-slides loop slidesPerView="4" spaceBetween="10">\n          <ion-slide *ngFor="let member of members | slice : 0: 15">\n            <img [src]="member.picture">\n          </ion-slide>\n        </ion-slides>\n      </ion-card>\n\n      <ion-card class="reviews">\n        <h4>Reviews  <rating [rate]="gym.rating" [color]="\'gold\'" [align]="\'flex-end\'"></rating></h4>\n        <ion-slides loop>\n          <ion-slide *ngFor="let review of gym?.detail?.reviews">\n            <ion-item>\n              <ion-avatar item-left>\n                <img [src]="review.profile_photo_url">\n              </ion-avatar>\n              <h2>{{review.author_name}}</h2>\n              <p>{{review.text}}</p>\n              <p small>{{review.relative_time_description}}</p>\n            </ion-item>\n          </ion-slide>\n        </ion-slides>\n      </ion-card>\n    </div>\n  </div>\n\n</ion-content>\n'/*ion-inline-end:"D:\Development\Current_Projects\2019_5_16_Ionic\personal-coach-app\src\pages\gyms\gym\gym.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_3__ionic_native_call_number__["a" /* CallNumber */],
            __WEBPACK_IMPORTED_MODULE_5__ionic_native_in_app_browser__["a" /* InAppBrowser */],
            __WEBPACK_IMPORTED_MODULE_4_angularfire2_database__["a" /* AngularFireDatabase */]])
    ], Gym);
    return Gym;
}());

//# sourceMappingURL=gym.js.map

/***/ }),

/***/ 949:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return Me; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__mi_profile_mi_profile__ = __webpack_require__(585);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__mi_reads_mi_reads__ = __webpack_require__(586);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__mi_schedule_mi_schedule__ = __webpack_require__(587);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__mi_goals_mi_goals__ = __webpack_require__(588);
/* harmony namespace reexport (by used) */ __webpack_require__.d(__webpack_exports__, "d", function() { return __WEBPACK_IMPORTED_MODULE_1__mi_profile_mi_profile__["a"]; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__mi_profile_form_mi_profile_form__ = __webpack_require__(950);
/* harmony namespace reexport (by used) */ __webpack_require__.d(__webpack_exports__, "e", function() { return __WEBPACK_IMPORTED_MODULE_5__mi_profile_form_mi_profile_form__["a"]; });
/* harmony namespace reexport (by used) */ __webpack_require__.d(__webpack_exports__, "f", function() { return __WEBPACK_IMPORTED_MODULE_2__mi_reads_mi_reads__["a"]; });
/* harmony namespace reexport (by used) */ __webpack_require__.d(__webpack_exports__, "b", function() { return __WEBPACK_IMPORTED_MODULE_4__mi_goals_mi_goals__["a"]; });
/* harmony namespace reexport (by used) */ __webpack_require__.d(__webpack_exports__, "g", function() { return __WEBPACK_IMPORTED_MODULE_3__mi_schedule_mi_schedule__["a"]; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__mi_health_mi_health__ = __webpack_require__(951);
/* harmony namespace reexport (by used) */ __webpack_require__.d(__webpack_exports__, "c", function() { return __WEBPACK_IMPORTED_MODULE_6__mi_health_mi_health__["a"]; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};





var Me = /** @class */ (function () {
    function Me() {
        this.miProfile = __WEBPACK_IMPORTED_MODULE_1__mi_profile_mi_profile__["a" /* MiProfile */];
        this.miReads = __WEBPACK_IMPORTED_MODULE_2__mi_reads_mi_reads__["a" /* MiReads */];
        this.miSchedule = __WEBPACK_IMPORTED_MODULE_3__mi_schedule_mi_schedule__["a" /* MiSchedule */];
        this.miGoals = __WEBPACK_IMPORTED_MODULE_4__mi_goals_mi_goals__["a" /* MiGoals */];
    }
    Me = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-me',template:/*ion-inline-start:"D:\Development\Current_Projects\2019_5_16_Ionic\personal-coach-app\src\pages\me\me.html"*/'\n<ion-tabs>\n  <ion-tab [root]="miGoals" tabIcon="podium-outline"></ion-tab>\n  <ion-tab [root]="miProfile" tabIcon="contact-outline"></ion-tab>\n  <ion-tab [root]="miReads" tabIcon="book-outline"></ion-tab>\n  <ion-tab [root]="miSchedule" tabIcon="time-outline"></ion-tab>\n</ion-tabs>'/*ion-inline-end:"D:\Development\Current_Projects\2019_5_16_Ionic\personal-coach-app\src\pages\me\me.html"*/,
        })
    ], Me);
    return Me;
}());







//# sourceMappingURL=me.js.map

/***/ }),

/***/ 950:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MiProfileForm; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(10);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_forms__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__shared_index__ = __webpack_require__(15);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__ionic_native_camera__ = __webpack_require__(138);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_angularfire2_database__ = __webpack_require__(17);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






var MiProfileForm = /** @class */ (function () {
    function MiProfileForm(fb, navCtrl, actionSheetCtrl, camera, data$, auth$, storage$, ionic$, error$) {
        this.fb = fb;
        this.navCtrl = navCtrl;
        this.actionSheetCtrl = actionSheetCtrl;
        this.camera = camera;
        this.data$ = data$;
        this.auth$ = auth$;
        this.storage$ = storage$;
        this.ionic$ = ionic$;
        this.error$ = error$;
        this.genders = [
            { text: 'Male', value: 'M' },
            { text: 'Female', value: 'F' },
            { text: 'Unspecified', value: 'U' }
        ];
        this.cameraOptions = {
            quality: 100,
            destinationType: this.camera.DestinationType.DATA_URL,
            sourceType: this.camera.PictureSourceType.CAMERA
        };
    }
    MiProfileForm.prototype.ngOnInit = function () {
        var _this = this;
        this.mi = this.auth$.currentUser;
        this._user = this.data$.object('/users/' + this.mi.uid);
        this.profile_form = this.fb.group({
            fullname: [''],
            picture: [''],
            address: [''],
            city: [''],
            country: [''],
            gender: [],
            dob: [''],
            about: [''],
            uid: []
        });
        this.isLoading = true;
        this._user.valueChanges().subscribe(function (user) {
            _this.user = user;
            if (_this.user)
                _this.profile_form.patchValue(_this.user);
            _this.isLoading = false;
        });
    };
    MiProfileForm.prototype.addImage = function (ev) {
        var _this = this;
        var actionSheet = this.actionSheetCtrl.create({
            title: 'Save Image from',
            buttons: [
                {
                    text: 'Camera',
                    handler: function () {
                        _this.cameraOptions.sourceType = _this.camera.PictureSourceType.CAMERA;
                        _this.getPicture(_this.cameraOptions);
                    }
                },
                {
                    text: 'Photo Library',
                    handler: function () {
                        _this.cameraOptions.sourceType = _this.camera.PictureSourceType.PHOTOLIBRARY;
                        _this.getPicture(_this.cameraOptions);
                    }
                },
                {
                    text: 'Cancel',
                    role: 'cancel',
                    handler: function () {
                    }
                }
            ]
        });
        actionSheet.present();
    };
    MiProfileForm.prototype.getPicture = function (cameraOptions) {
        var _this = this;
        this.camera.getPicture(cameraOptions).then(function (imageData) {
            _this.user.picture = 'data:image/jpeg;base64,' + imageData;
            _this.profile_form.patchValue({ picture: 'data:image/jpeg;base64,' + imageData });
        }, function (error) {
            _this.error$.handleError(error);
        });
    };
    MiProfileForm.prototype.save = function (_a) {
        var _this = this;
        var value = _a.value, valid = _a.valid;
        if (valid) {
            value.uid = this.mi.uid;
            if (value.picture) {
                this.storage$
                    .uploadFile(this.mi.uid.replace(' ', '_'), value.picture)
                    .then(function (file) {
                    value.picture = file;
                    _this._user.update(value).then(function (user) {
                        _this.ionic$.presentToast('User Saved');
                        _this.navCtrl.pop();
                    }, function (error) { return _this.error$.handleError(error); });
                });
            }
            else {
                this._user.update(value).then(function (user) {
                    _this.ionic$.presentToast('User Saved');
                    _this.navCtrl.pop();
                }, function (error) { return _this.error$.handleError(error); });
            }
        }
    };
    MiProfileForm = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'mi-profile-form',template:/*ion-inline-start:"D:\Development\Current_Projects\2019_5_16_Ionic\personal-coach-app\src\pages\me\mi-profile-form\mi-profile-form.html"*/'<ion-header>\n\n  <ion-navbar>\n    <button ion-button menuToggle>\n      <ion-icon name="menu-outline"></ion-icon>\n    </button>\n    <ion-title>Edit Profile</ion-title>\n  </ion-navbar>\n\n</ion-header>\n\n<ion-content padding>\n  <loading *ngIf="isLoading"></loading>\n  <div class="container custom-floating-form" *ngIf="!isLoading">\n    <form [formGroup]="profile_form">\n      <div class="user-image">\n        <img [src]="user?.picture | image: \'url:profile\'" (click)="addImage()" greydient-left>\n      </div>\n      <div class="scroll">\n        <ion-item>\n          <ion-label floating>Full name</ion-label>\n          <ion-input type="text" formControlName="fullname"></ion-input>\n        </ion-item>\n\n        <ion-item>\n          <ion-label floating>Gender</ion-label>\n          <ion-select formControlName="gender">\n            <ion-option [value]="item.value" *ngFor="let item of genders">{{item.text}}</ion-option>\n          </ion-select>\n        </ion-item>\n\n        <ion-item>\n          <ion-label floating>Date of Birth</ion-label>\n          <ion-datetime type="text" displayFormat="YYYY-MM-DD" pickerFormat="YYYY-MM-DD" formControlName="dob"></ion-datetime>\n        </ion-item>\n\n        <ion-item>\n          <ion-label floating>Address</ion-label>\n          <ion-textarea elastic-textarea formControlName="address"></ion-textarea>\n        </ion-item>\n\n        <ion-item>\n          <ion-label floating>City</ion-label>\n          <ion-input type="text" formControlName="city"></ion-input>\n        </ion-item>\n\n        <ion-item>\n          <ion-label floating>Country</ion-label>\n          <ion-input type="text" formControlName="country"></ion-input>\n        </ion-item>\n\n        <ion-item>\n          <ion-label floating>Abut Me</ion-label>\n          <ion-textarea elastic-textarea formControlName="about"></ion-textarea>\n        </ion-item>\n\n      </div>\n    </form>\n  </div>\n</ion-content>\n\n<div class="floating">\n  <button ion-button block gradient-left box-shadow (click)="save(profile_form)" [disabled]="!profile_form.valid">Update Profile</button>\n</div>\n'/*ion-inline-end:"D:\Development\Current_Projects\2019_5_16_Ionic\personal-coach-app\src\pages\me\mi-profile-form\mi-profile-form.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_2__angular_forms__["FormBuilder"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* ActionSheetController */],
            __WEBPACK_IMPORTED_MODULE_4__ionic_native_camera__["a" /* Camera */],
            __WEBPACK_IMPORTED_MODULE_5_angularfire2_database__["a" /* AngularFireDatabase */],
            __WEBPACK_IMPORTED_MODULE_3__shared_index__["a" /* AuthProvider */],
            __WEBPACK_IMPORTED_MODULE_3__shared_index__["n" /* StorageProvider */],
            __WEBPACK_IMPORTED_MODULE_3__shared_index__["f" /* IonicProvider */],
            __WEBPACK_IMPORTED_MODULE_3__shared_index__["d" /* ErrorHandlerProvider */]])
    ], MiProfileForm);
    return MiProfileForm;
}());

//# sourceMappingURL=mi-profile-form.js.map

/***/ }),

/***/ 951:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MiHealth; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(10);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_forms__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__shared_index__ = __webpack_require__(15);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_angularfire2_database__ = __webpack_require__(17);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var MiHealth = /** @class */ (function () {
    function MiHealth(fb, navCtrl, data$, auth$, storage$, ionic$, error$) {
        this.fb = fb;
        this.navCtrl = navCtrl;
        this.data$ = data$;
        this.auth$ = auth$;
        this.storage$ = storage$;
        this.ionic$ = ionic$;
        this.error$ = error$;
    }
    MiHealth.prototype.ngOnInit = function () {
        var _this = this;
        this.mi = this.auth$.currentUser;
        this._user = this.data$.object('/users/' + this.mi.uid);
        this.profile_form = this.fb.group({
            weight: [''],
            height: [''],
            bmi: [''],
            bfp: ['']
        });
        this.isLoading = true;
        this._user.valueChanges().subscribe(function (user) {
            _this.user = user;
            if (_this.user)
                _this.profile_form.patchValue(_this.user);
            _this.isLoading = false;
        });
    };
    MiHealth.prototype.save = function (_a) {
        var _this = this;
        var value = _a.value, valid = _a.valid;
        if (valid) {
            this._user.update(value).then(function (user) {
                _this.ionic$.presentToast('User Saved');
                _this.navCtrl.pop();
            }, function (error) { return _this.error$.handleError(error); });
        }
    };
    MiHealth = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'mi-health',template:/*ion-inline-start:"D:\Development\Current_Projects\2019_5_16_Ionic\personal-coach-app\src\pages\me\mi-health\mi-health.html"*/'<ion-header>\n\n  <ion-navbar>\n    <button ion-button menuToggle>\n      <ion-icon name="menu-outline"></ion-icon>\n    </button>\n    <ion-title>Health Profile</ion-title>\n  </ion-navbar>\n\n</ion-header>\n\n<ion-content padding>\n  <loading *ngIf="isLoading"></loading>\n  <div class="container custom-floating-form" *ngIf="!isLoading">\n    <form [formGroup]="profile_form">\n      <div class="scroll">\n        <ion-item>\n          <ion-label floating>Weight (kg)</ion-label>\n          <ion-input type="text" formControlName="weight"></ion-input>\n        </ion-item>\n        <ion-item>\n          <ion-label floating>Height (cm)</ion-label>\n          <ion-input type="text" formControlName="height"></ion-input>\n        </ion-item>\n\n        <ion-item>\n          <ion-label floating>Body Mass Index</ion-label>\n          <ion-input type="text" formControlName="bmi"></ion-input>\n        </ion-item>\n\n        <ion-item>\n          <ion-label floating>Body Fat Percentage</ion-label>\n          <ion-input type="text" formControlName="bfp"></ion-input>\n        </ion-item>\n\n      </div>\n    </form>\n  </div>\n</ion-content>\n\n<div class="floating">\n  <button ion-button block gradient-left box-shadow (click)="save(profile_form)" [disabled]="!profile_form.valid">Update Profile</button>\n</div>\n'/*ion-inline-end:"D:\Development\Current_Projects\2019_5_16_Ionic\personal-coach-app\src\pages\me\mi-health\mi-health.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_2__angular_forms__["FormBuilder"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_4_angularfire2_database__["a" /* AngularFireDatabase */],
            __WEBPACK_IMPORTED_MODULE_3__shared_index__["a" /* AuthProvider */],
            __WEBPACK_IMPORTED_MODULE_3__shared_index__["n" /* StorageProvider */],
            __WEBPACK_IMPORTED_MODULE_3__shared_index__["f" /* IonicProvider */],
            __WEBPACK_IMPORTED_MODULE_3__shared_index__["d" /* ErrorHandlerProvider */]])
    ], MiHealth);
    return MiHealth;
}());

//# sourceMappingURL=mi-health.js.map

/***/ }),

/***/ 952:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return Blogs; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(10);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_angularfire2_database__ = __webpack_require__(17);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__shared_index__ = __webpack_require__(15);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__index__ = __webpack_require__(31);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__components_index__ = __webpack_require__(29);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






var Blogs = /** @class */ (function () {
    function Blogs(modalCtrl, data$, auth$) {
        this.modalCtrl = modalCtrl;
        this.data$ = data$;
        this.auth$ = auth$;
        this.blog_type = 'food';
    }
    Blogs.prototype.ngOnInit = function () {
        var _this = this;
        this.segments = [
            {
                title: 'food',
                click: function () {
                    _this.getData('food');
                }
            },
            {
                title: 'fitness',
                click: function () {
                    _this.getData('fitness');
                }
            },
            {
                title: 'lifestyle',
                click: function () {
                    _this.getData('lifestyle');
                }
            },
        ];
        this.mi = this.auth$.currentUser;
        this.getData(this.blog_type);
    };
    Blogs.prototype.getData = function (type) {
        var _this = this;
        this.blog_type = type;
        this.isLoading = true;
        this.data$.list('/blogs/', function (ref) { return ref.orderByChild('type').equalTo(_this.blog_type); }).valueChanges()
            .subscribe(function (blogs) {
            _this.isLoading = false;
            _this.blogs = _this.blogsBackUp = blogs;
        });
    };
    Blogs.prototype.add = function () {
        var modal = this.modalCtrl.create(__WEBPACK_IMPORTED_MODULE_4__index__["a" /* AddBlog */]);
        modal.present();
    };
    Blogs.prototype.search = function () {
        var _this = this;
        var modal = this.modalCtrl.create(__WEBPACK_IMPORTED_MODULE_5__components_index__["g" /* Search */], { searchable: ['title', 'type', 'content'] });
        modal.onDidDismiss(function (search) {
            _this.blogs = _this.blogsBackUp;
            if (search) {
                _this.blogs = _this.blogs.filter(function (blog) {
                    return search.title ? blog.title.toLowerCase().indexOf(search.title.toLowerCase()) > -1 : null
                        || search.type ? blog.type.toLowerCase().indexOf(search.type.toLowerCase()) > -1 : null
                        || search.content ? blog.content.toLowerCase().indexOf(search.content.toLowerCase()) > -1 : null;
                });
            }
        });
        modal.present();
    };
    Blogs = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-blogs',template:/*ion-inline-start:"D:\Development\Current_Projects\2019_5_16_Ionic\personal-coach-app\src\pages\blogs\blogs.html"*/'<ion-content>\n  <div class="container">\n    <!--toolbar and navigation-->\n    <div class="top" gradient-left box-shadow>\n      <ion-toolbar>\n        <button ion-button menuToggle>\n          <ion-icon name="menu-outline"></ion-icon>\n        </button>\n        <ion-title>blogs</ion-title>\n        <ion-buttons end>\n          <button ion-button (click)="add()">\n        <ion-icon name="add-outline"></ion-icon>\n      </button>\n          <button ion-button (click)="search()">\n        <ion-icon name="search-outline"></ion-icon>\n      </button>\n        </ion-buttons>\n      </ion-toolbar>\n      <ion-toolbar>\n        <div class="segments">\n          <div class="segment" [ngClass]="{\'active\': segment.title === blog_type}" *ngFor="let segment of segments" (click)="segment.click()">{{segment.title}}</div>\n        </div>\n      </ion-toolbar>\n    </div>\n\n    <div class="blog-content">\n      <loading *ngIf="isLoading"></loading>\n\n      <div *ngIf="!isLoading">\n        <div *ngIf="blogs?.length > 0; then dataTemplate else noDataTemplate"></div>\n\n        <ng-template #noDataTemplate>\n          <no-data></no-data>\n        </ng-template>\n\n        <ng-template #dataTemplate>\n          <div [ngSwitch]="blog_type">\n            <food-blog *ngSwitchCase="\'food\'" [data]="blogs"></food-blog>\n            <fitness-blog *ngSwitchCase="\'fitness\'" [data]="blogs"></fitness-blog>\n            <lifestyle-blog *ngSwitchCase="\'lifestyle\'" [data]="blogs"></lifestyle-blog>\n            <food-blog *ngSwitchDefault [data]="blogs"></food-blog>\n          </div>\n        </ng-template>\n      </div>\n    </div>\n  </div>\n\n</ion-content>\n'/*ion-inline-end:"D:\Development\Current_Projects\2019_5_16_Ionic\personal-coach-app\src\pages\blogs\blogs.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* ModalController */],
            __WEBPACK_IMPORTED_MODULE_2_angularfire2_database__["a" /* AngularFireDatabase */],
            __WEBPACK_IMPORTED_MODULE_3__shared_index__["a" /* AuthProvider */]])
    ], Blogs);
    return Blogs;
}());

//# sourceMappingURL=blogs.js.map

/***/ }),

/***/ 953:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return FitnessBlog; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(10);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__index__ = __webpack_require__(31);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_Rx__ = __webpack_require__(229);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_Rx___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_rxjs_Rx__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_angularfire2_database__ = __webpack_require__(17);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__shared_index__ = __webpack_require__(15);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__ionic_native_social_sharing__ = __webpack_require__(74);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7_lodash__ = __webpack_require__(73);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7_lodash___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_7_lodash__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};








var FitnessBlog = /** @class */ (function () {
    function FitnessBlog(navCtrl, navParams, alertCtrl, modalCtrl, socialshare, data$, auth$, ionic$, error$) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.alertCtrl = alertCtrl;
        this.modalCtrl = modalCtrl;
        this.socialshare = socialshare;
        this.data$ = data$;
        this.auth$ = auth$;
        this.ionic$ = ionic$;
        this.error$ = error$;
        this.blogs = [];
    }
    FitnessBlog.prototype.ngOnInit = function () {
        this.mi = this.auth$.currentUser;
    };
    FitnessBlog.prototype.open = function (blog) {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_2__index__["c" /* Blog */], blog);
    };
    FitnessBlog.prototype.likedByUser = function (blog) {
        blog.likes_count = __WEBPACK_IMPORTED_MODULE_7_lodash__["size"](blog.likes);
        blog.comments_count = __WEBPACK_IMPORTED_MODULE_7_lodash__["size"](blog.comments);
        return blog.likes && this.mi ? blog.likes[this.mi.uid] : undefined;
    };
    FitnessBlog.prototype.bookmarked = function (blog) {
        return blog.bookmarks ? blog.bookmarks[this.mi.uid] : undefined;
    };
    FitnessBlog.prototype.like = function (blog) {
        var _this = this;
        var likedbyUser = blog.likes && this.mi ? blog.likes[this.mi.uid] : undefined;
        if (!likedbyUser) {
            this.data$.object('/blogs/' + blog.$key + '/likes/' + this.mi.uid)
                .set({
                name: this.mi.fullname ? this.mi.fullname : '',
                picture: this.mi.picture ? this.mi.picture : '',
                date: new Date()
            })
                .then(function () { return _this.ionic$.presentToast('You Liked this post'); }, function (error) { return _this.error$.handleError(error); });
        }
        else {
            this.data$.object('/blogs/' + blog.$key + '/likes/' + this.mi.uid)
                .remove().then(function () { return _this.ionic$.presentToast('You Unliked this post'); }, function (error) { return _this.error$.handleError(error); });
        }
    };
    FitnessBlog.prototype.comment = function (blog) {
        var modal = this.modalCtrl.create(__WEBPACK_IMPORTED_MODULE_2__index__["d" /* BlogComments */], blog);
        modal.present();
    };
    FitnessBlog.prototype.share = function (blog) {
        this.socialshare.share();
    };
    FitnessBlog.prototype.delete = function (blog) {
        var _this = this;
        var alert = this.alertCtrl.create({
            title: 'Confirm Delete',
            message: 'Are you sure you want to delete this post?',
            buttons: [
                {
                    text: 'Cancel',
                    role: 'cancel',
                    handler: function () {
                    }
                },
                {
                    text: 'Delete',
                    cssClass: 'primary-button',
                    handler: function () {
                        _this.data$.object('/blogs/' + blog.$key).remove()
                            .then(function () { return _this.ionic$.presentToast('You have deleted this post'); }, function (error) { return _this.error$.handleError(error); });
                    }
                }
            ]
        });
        alert.present();
    };
    FitnessBlog.prototype.bookmark = function (blog) {
        var _this = this;
        var bookmarked = blog.bookmarks && this.mi ? blog.bookmarks[this.mi.uid] : undefined;
        if (!bookmarked) {
            this.data$.object('/blogs/' + blog.$key + '/bookmarks/' + this.mi.uid)
                .set(this.mi.uid)
                .then(function () { return _this.ionic$.presentToast('You have saved this post'); }, function (error) { return _this.error$.handleError(error); });
        }
        else {
            this.data$.list('/blogs/' + blog.$key + '/bookmarks/' + this.mi.uid)
                .remove()
                .then(function () { return _this.ionic$.presentToast('You have saved this post'); }, function (error) { return _this.error$.handleError(error); });
        }
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])(),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_3_rxjs_Rx__["Observable"])
    ], FitnessBlog.prototype, "data", void 0);
    FitnessBlog = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'fitness-blog',template:/*ion-inline-start:"D:\Development\Current_Projects\2019_5_16_Ionic\personal-coach-app\src\pages\blogs\fitness-blog\fitness-blog.html"*/'<ng-container *ngIf="mi">\n  <ion-card class="blog-card" *ngFor="let blog of data | orderBy: \'-created_at\'">\n\n    <ion-row>\n      <ion-col col-4 *ngIf="blog?.images?.length > 0" (click)="open(blog)" [style.background-image]="\'url(\' + blog?.images[0] + \')\' | image: \'style: placeholder\'"></ion-col>\n      <ion-col>\n        <p (click)="open(blog)">{{blog.content| slice: 0: 140}}</p>\n\n        <ion-row class="actions">\n          <ion-col col-3 class="date">{{blog.created_at | amTimeAgo}}</ion-col>\n          <ion-col text-right>\n            <span (click)="like(blog)">\n              <ion-icon [name]="likedByUser(blog) ? \'heart\' : \'heart-outline\'"></ion-icon>{{blog?.likes_count > 0 ?\n              blog?.likes_count : \'\'}}\n            </span>\n            <span (click)="comment(blog)">\n              <ion-icon name="quote-outline"></ion-icon>{{blog?.comments_count > 0 ? blog?.comments_count : \'\'}}\n            </span>\n            <span (click)="bookmark(blog)">\n              <ion-icon [name]="bookmarked(blog) ? \'bookmark\' : \'bookmark-outline\'"></ion-icon>\n            </span>\n            <span (click)="share(blog)">\n              <ion-icon name="md-share"></ion-icon>\n            </span>\n            <span (click)="delete(blog)" *ngIf="blog?.user?.uid === mi?.uid">\n              <ion-icon name="trash" color="danger"></ion-icon>\n            </span>\n          </ion-col>\n        </ion-row>\n      </ion-col>\n    </ion-row>\n  </ion-card>\n</ng-container>'/*ion-inline-end:"D:\Development\Current_Projects\2019_5_16_Ionic\personal-coach-app\src\pages\blogs\fitness-blog\fitness-blog.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["b" /* AlertController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* ModalController */],
            __WEBPACK_IMPORTED_MODULE_6__ionic_native_social_sharing__["a" /* SocialSharing */],
            __WEBPACK_IMPORTED_MODULE_4_angularfire2_database__["a" /* AngularFireDatabase */],
            __WEBPACK_IMPORTED_MODULE_5__shared_index__["a" /* AuthProvider */],
            __WEBPACK_IMPORTED_MODULE_5__shared_index__["f" /* IonicProvider */],
            __WEBPACK_IMPORTED_MODULE_5__shared_index__["d" /* ErrorHandlerProvider */]])
    ], FitnessBlog);
    return FitnessBlog;
}());

//# sourceMappingURL=fitness-blog.js.map

/***/ })

},[674]);
//# sourceMappingURL=main.js.map